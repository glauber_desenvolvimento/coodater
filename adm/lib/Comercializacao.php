<?php
class Comercializacao {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;

    public function __construct($connect) {
        $this->Connect = $connect;
    }
    
    public function getDados() {
        return $this->Dados;
    }
    
    public function getId($id){
        $this->Connect->Output("SELECT id_beneficiario FROM beneficiario WHERE id_beneficiario = {$id}", $result, $rows, true);
        $this->Dados = $result;
        return $result["id_beneficiario"];
    }
    
    public function setEmpty() {
        
        $this->Dados["forma_comercializacao"] = '';
        $this->Dados["renda_comercializacao"] = '';
        $this->Dados["qtd_comercializacao"] = '';
        
        $this->Connect->Output(
                "SELECT id_atividade AS id_produto, atividade AS produto FROM atividade
                WHERE atividade <> 'Outros'
                UNION 
                SELECT id_tipo_semovente AS id_produto, tipo_semovente AS produto FROM tipo_semovente
                WHERE tipo_semovente <> 'Outros'", 
                $result, $rows);
        $this->Dados["produtos"] = '';
        for($i=0; $i<$rows; $i++){
            $this->Dados["produtos"] .= 
                "<option value='{$result[$i]["id_produto"]}#{$result[$i]["produto"]}'>
                    {$result[$i]["produto"]}
                </option>";
        }

        
        $this->Connect->Output("SELECT * FROM medida WHERE id_medida IN(12, 11, 8)", $result, $rows);
        $this->Dados["und_medidas"] = '';
        for($i=0; $i<$rows; $i++){

            $this->Dados["und_medidas"] .= 
                "<option value='{$result[$i]["id_medida"]}'>{$result[$i]["medida"]}</option>";
        }
        
    }
    
    public function Criar($comercializacao) {
        $c = explode("#", $comercializacao["produto_comercializacao"]);
        $c[0];//ID
        $c[1];//PRODUTO
        $this->Connect->Output("SELECT * FROM atividade WHERE atividade ='{$c[1]}'", $atividade, $t_atividade, true);
        $this->Connect->Output("SELECT * FROM tipo_semovente WHERE tipo_semovente ='{$c[1]}'", $semovente, $t_semovente, true);
        unset($comercializacao["produto_comercializacao"]);
        if($t_atividade == 1){
            $comercializacao["id_atividade"] = $c[0];
            $comercializacao["id_tipo_semovente"] = NULL;
        }elseif($t_semovente == 1){
            $comercializacao["id_tipo_semovente"] = $c[0];
            $comercializacao["id_atividade"] = NULL;
        }
        unset($c);
        
        $comercializacao["renda_comercializacao"] = str_replace(",", ".", $comercializacao["renda_comercializacao"]);
        
        $this->Dados = $comercializacao;
        $this->Query = new Insert("comercializacao", $this->Dados);
        $this->Connect->InputBy($this->Query);
        
    }
    
    public function Ver($beneficiario) {
        
        $this->Query = new Select("comercializacao");
        $this->Query->setTables(array("medida", "tipo_semovente", "atividade"), "LEFT");
        $this->Query->setWhere(array("id_beneficiario" => $beneficiario));
        $this->Query->setOrder("id_comercializacao DESC", true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $lista = '';
        if($rows > 0){
            for($i=0; $i<$rows; $i++){
                if($i%2 == 1){
                    $zebra = "class='bg-light-blue'";
                }else{
                    $zebra = '';
                }
                $result[$i]["produto"] = (!empty($result[$i]["atividade"])) ? $result[$i]["atividade"] : $result[$i]["tipo_semovente"];
                $result[$i]["qtd_comercializacao"] = $result[$i]["qtd_comercializacao"] . ' ' . $result[$i]["medida"];
                $result[$i]["renda_comercializacao"] = number_format($result[$i]["renda_comercializacao"], 2, ",", ".");
                
                $lista .= "<tr {$zebra}>
                                <td>{$result[$i]["id_comercializacao"]}</td>
                                <td>{$result[$i]["produto"]}</td>
                                <td>{$result[$i]["qtd_comercializacao"]}</td>
                                <td>{$result[$i]["renda_comercializacao"]}</td>
                                <td>{$result[$i]["forma_comercializacao"]}</td>
                                <td>
                                    <a class='w-3' href='#SRC#comercializacao&editar={$result[$i]["id_comercializacao"]}'>
                                        <img src='" . IMG . "select.png'>
                                    </a>
                                </td>
                                <td>
                                    <a class='w-3 abre_confirma' href='javascript:;' data-href='#SRC#comercializacao&deletar={$result[$i]["id_comercializacao"]}'>
                                        <img src='" . IMG . "delete.png'>
                                    </a>
                                </td>
                          </tr>";
            }
        }else{
            $lista = "<tr><td colspan='7'><br><br>Este beneficiário ainda não possui comercio.<br><br><br></td></td>";
        }
        
        return $lista;
    }
    
    public function Editar($id) {
        $this->Query = new Select("comercializacao");
        $this->Query->setTables(array("medida", "tipo_semovente", "atividade"), "LEFT");
        $this->Query->setWhere(array("id_comercializacao" => $id), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        $this->Dados = $result;
        $this->Dados["produto"] = (!empty($this->Dados["atividade"])) ? $this->Dados["atividade"] : $this->Dados["tipo_semovente"];
        
        $this->Connect->Output(
                "SELECT id_atividade AS id_produto, atividade AS produto FROM atividade
                WHERE atividade <> 'Outros'
                UNION 
                SELECT id_tipo_semovente AS id_produto, tipo_semovente AS produto FROM tipo_semovente
                WHERE tipo_semovente <> 'Outros'", 
                $result, $rows);
        $this->Dados["produtos"] = '';
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["produto"] == $result[$i]["produto"]) ? "selected" : '';
            $this->Dados["produtos"] .= 
                "<option {$selected} value='{$result[$i]["id_produto"]}#{$result[$i]["produto"]}'>
                    {$result[$i]["produto"]}
                </option>";
        }
        
        $this->Connect->Output("SELECT * FROM medida WHERE id_medida IN(12, 11, 8)", $result, $rows);
        $this->Dados["und_medidas"] = '';
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["id_medida"] == $result[$i]["id_medida"]) ? "selected" : '';
            $this->Dados["und_medidas"] .= 
                "<option {$selected} value='{$result[$i]["id_medida"]}'>{$result[$i]["medida"]}</option>";
        }
        
        $this->Dados["renda_comercializacao"] = str_replace(".", ",", $this->Dados["renda_comercializacao"]);
        
    }
    
    public function Alterar($comercializacao, $id) {
        $c = explode("#", $comercializacao["produto_comercializacao"]);
        $c[0];//ID
        $c[1];//PRODUTO
        $this->Connect->Output("SELECT * FROM atividade WHERE atividade ='{$c[1]}'", $atividade, $t_atividade, true);
        $this->Connect->Output("SELECT * FROM tipo_semovente WHERE tipo_semovente ='{$c[1]}'", $semovente, $t_semovente, true);
        unset($comercializacao["produto_comercializacao"]);
        if($t_atividade == 1){
            $comercializacao["id_atividade"] = $c[0];
            $comercializacao["id_tipo_semovente"] = NULL;
        }elseif($t_semovente == 1){
            $comercializacao["id_tipo_semovente"] = $c[0];
            $comercializacao["id_atividade"] = NULL;
        }
        unset($c);
        
        $comercializacao["renda_comercializacao"] = str_replace(",", ".", $comercializacao["renda_comercializacao"]);
        
        $this->Query = new Update("comercializacao", $comercializacao);
        $this->Query->setWhere(array("id_comercializacao" => $id), true);
        
        $this->Connect->ExecuteBy($this->Query);
    }
    public function Deletar($id) {
        $this->Query = new Delete("comercializacao", array("id_comercializacao" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
}