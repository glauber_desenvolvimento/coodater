<?php
/**
 * <b>Classe Select</b> - Cria Objeto de consultas
 *    Classe que cria
 *    
 * @copyright 2014 Glauber Gonçalves dos Santos
 */
class Select {
    private $Values;
    private $Query = "SELECT ";
    private $Clause = '';
    private $Limit = '';


    public function __construct($From = NULL, $Columns = NULL, $Limit = NULL) {
        if(func_num_args() > 1):
            $this->Limit = (is_int($Limit)) ? " LIMIT {$Limit}" : '';
            if(is_array($Columns)){
                $this->Query .= implode(", ", $Columns) . " FROM {$From}";
            }else{
                $this->Query .= $Columns . " FROM {$From}";
            }
        elseif(func_num_args() == 1):
            $this->Query .= "* FROM {$From}";
        endif;
    }

    public function Reset($From, $Columns = NULL, $Limit = NULL) {
        
        $this->Values = NULL;
        $this->Query = "SELECT ";
        $this->Clause = '';
        $this->Limit = '';
        
        if(func_num_args() > 1):
            $this->Limit = (is_int($Limit)) ? " LIMIT {$Limit}" : '';
            if(is_array($Columns)){
                $this->Query .= implode(", ", $Columns) . " FROM {$From}";
            }else{
                $this->Query .= $Columns . " FROM {$From}";
            }
        else:
            $this->Query .= "* FROM {$From}"; 
        endif;
    }

    public function setTables($Tables, $Operator = "INNER"){
        if(is_array($Tables)):
            for($i=0; $i<sizeof($Tables); $i++):
                if(strrchr($Tables[$i], '#')):
                    $using = explode('#', $Tables[$i]);
                    $this->Query .= " {$Operator} JOIN {$using[0]} USING({$using[1]})";
                else:
                    $this->Query .= " {$Operator} JOIN {$Tables[$i]} USING(id_{$Tables[$i]})";
                endif;
            endfor;
        else:
            if(strrchr($Tables, '#')):
                $using = explode('#', $Tables);
                $this->Query .= " {$Operator} JOIN {$using[0]} USING({$using[1]})";
            else:
                $this->Query .= " {$Operator} JOIN {$Tables} USING(id_{$Tables})";
            endif;
        endif;
    }
    
    public function setOrder($Order = NULL, $end = false){
        if(is_array($Order)):
            $size = sizeof($Order);
            $this->Clause .= " ORDER BY " . implode(", ", $Order);
        elseif(!empty($Order)):
            $this->Clause .= " ORDER BY " . $Order;
        endif;
        if($end):
            $this->End();
        endif;
    }
    
    public function setGroup($Group = NULL, $end = false){
        if(is_array($Group)):
            $size = sizeof($Group);
            $this->Clause .= " GROUP BY " . implode(", ", $Group);
        elseif(!empty($Group)):
            $this->Clause .= " GROUP BY " . $Group;
        endif;
        if($end):
            $this->End();
        endif;
    }
    
    public function setOrderGroup($Order = NULL, $Group = NULL, $end = false){
        if(is_array($Group)):
            $size = sizeof($Group);
            $this->Clause .= " GROUP BY " . implode(", ", $Group);
        elseif(!empty($Group)):
            $this->Clause .= " GROUP BY " . $Group;
        endif;
        if(is_array($Order)):
            $size = sizeof($Group);
            $this->Clause .= " ORDER BY " . implode(", ", $Order);
        elseif(!empty($Order)):
            $this->Clause .= " ORDER BY " . $Order;
        endif;
        if($end):
            $this->End();
        endif;
    }
    
    public function setWhere(array $Where, $end = false, $rule = 'OR', $Operator = '='){
        if(sizeof($Where) == 1):
            $this->Clause = " WHERE " . key($Where) . " {$Operator} :" . key($Where);
        else:
            $this->Clause = " WHERE ";
            $i = 0;
            do{
                if($i++ <> (sizeof($Where)-1)):
                    $this->Clause .= key($Where) . " {$Operator} :" . key($Where) . " {$rule} ";
                else:
                    $this->Clause .= key($Where) . " {$Operator} :" . key($Where);
                endif;
            }while(next($Where));
        endif;
        $this->Values = $Where;
        if($end):
            $this->End();
        endif;
    }
    
    public function setHaving(array $Having, $end = false, $rule = 'OR', $Operator = '='){
        if(sizeof($Having) == 1):
            $this->Clause .= " HAVING " . key($Having) . " {$Operator} :" . key($Having);
        else:
            $this->Clause .= " HAVING ";
            $i = 0;
            do{
                if($i++ <> (sizeof($Having)-1)):
                    $this->Clause .= key($Having) . " {$Operator} :" . key($Having) . " {$rule} ";
                else:
                    $this->Clause .= key($Having) . " {$Operator} :" . key($Having);
                endif;
            }while(next($Having));
        endif;
        if(is_array($this->Query)):
            $this->Values = array_merge($this->Values, $Having);
        else:
            $this->Values = $Having;
        endif;
        if($end):
            $this->End();
        endif;
    }
    
    public function setIn($Key, array $In, $end = false){
        if(sizeof($In) == 1):
            $this->Clause = " WHERE {$Key} IN(:{$Key})";
            $this->Values[$Key] = $In[0];
        else:
            $this->Clause = " WHERE {$Key} IN(";
            for($i=0; $i<sizeof($In); $i++):
                $newIn[$Key . "_" . $i] = $In[$i];
                if($i <> (sizeof($In)-1)):
                    $this->Clause .= ":" . $Key . "_" . $i . ", ";
                else:
                    $this->Clause .= ":" . $Key . "_" . $i;
                endif;
            endfor;
            $this->Clause .= ")";
            $this->Values = $newIn;
        endif;
        if($end):
            $this->End();
        endif;
    }
    
    public function setLike(array $Keys, array $Likes, $end = false, $rule = "OR"){
        $sizeKeys = sizeof($Keys);
        $sizeLikes = sizeof($Likes);
        $this->Clause = " WHERE ";
        $aux_clause = array();
        for($i=0; $i<$sizeLikes; $i++):
            for($j=0; $j<$sizeKeys; $j++):
                array_push($aux_clause, "{$Keys[$j]} LIKE :{$Keys[$j]}_{$i}");
                $aux_values["{$Keys[$j]}_{$i}"] = "%{$Likes[$i]}%";
            endfor;
        endfor;
        $this->Clause .= implode(" {$rule} ", $aux_clause);
        $this->Values = $aux_values;
        if($end):
            $this->End();
        endif;
    }
    
    public function getValues() {
        return $this->Values;
    }

    public function getQuery() {
        return $this->Query;
    }

    public function setValues(array $Values) {
        $this->Values = $Values;
    }

    public function setQuery($Query) {
        $this->Query = $Query;
    }
    
    public function getClause() {
        return $this->Clause;
    }
    
    public function setClause($Clause, array $Values = NULL, $end = false) {
        $this->Clause = $Clause . $this->Clause;
        $this->Values = (!empty($Values)) ? $Values : $this->Values;
        if($end){
            $this->End();
        }
    }
    
    public function End(){
        $this->Query .= $this->Clause . $this->Limit;
        $this->Clause = NULL;
        $this->Limit = NULL;
    }
    
    public function __toString(){
        return $this->Query;
    }
}
