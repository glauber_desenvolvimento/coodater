<?php
class Relatorios {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;

    public function __construct($connect) {
        $this->Connect = $connect;
    }
    
    public function getDados() {
        return $this->Dados;
    }
    
    public function resetWords($name) {
        $format['a'] = 'ÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÕÔÙÚÛÜÝàáâãäçèéêëìíîïñòóõôùúûüý"!@#$%&*()_ +=()[]{}/?;:.,\\\'<>ºª';
        $format['b'] = 'aaaaaceeeeiiiinoooouuuuyaaaaaceeeeiiiinoooouuuuy                                  ';
        $data  = strtr(utf8_decode($name), utf8_decode($format['a']), $format['b']);
        $data  = strip_tags(trim($data));
        $data  = str_replace(' ', '_', $data);
        $data  = str_replace(array('_____', '____', '___', '__'), '_', $data);
        return strtolower(utf8_encode($data));
    }
    
    public function Ver($beneficiario) {
        
        $this->Query = new Select("relatorio");
        $this->Query->setWhere(array("id_beneficiario" => $beneficiario));
        $this->Query->setOrder("id_relatorio DESC", true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $lista = '';
        if($rows > 0){
            $files = array('jpeg' => 'FOTOGRAFIA', 'jpg' => 'FOTOGRAFIA', 'png' => 'FOTOGRAFIA', 'gif' => 'FOTOGRAFIA', 'pdf' => 'DOCUMENTO PDF', 'doc' => 'DOCUMENTO WORD', 'docx' => 'DOCUMENTO WORD', 'xlsx' => 'DOCUMENTO EXCEL');
            for($i=0; $i<$rows; $i++){
                
                if(!empty($result[$i]["arquivo_relatorio"])){
                    $ext = explode(".", $result[$i]["arquivo_relatorio"]);
                    $ext = end($ext);

                    $name = '';
                    foreach ($files as $key => $value) {
                        if($key == $ext){$name = $value;}
                    }

                    $result[$i]["arquivo_relatorio"] = "<a class='m-side' target='_blank' href='" . SRC . $result[$i]["arquivo_relatorio"] . "'>{$name}</a>";
                }else{
                    $result[$i]["arquivo_relatorio"] = "<span class='tx-5 tx-gray'>&Oslash;</span>";
                }
                
                
                if($i%2 == 1){
                    $zebra = "class='bg-light-blue'";
                }else{
                    $zebra = '';
                }
                $lista .= "<tr {$zebra}>
                                <td class='p-equal-s'>{$result[$i]["id_relatorio"]}</td>
                                <td class='p-equal-s'>{$result[$i]["relatorio"]}</td>
                                <td class='p-equal-s'>{$result[$i]["arquivo_relatorio"]}</td>
                                <td class='p-equal-s'>
                                    <a class='w-3' href='" . SRC . "relatorios&editar={$result[$i]["id_relatorio"]}'>
                                        <img src='" . IMG . "select.png'>
                                    </a>
                                </td>
                                <td class='p-equal-s'>
                                    <a class='w-3 abre_confirma' href='javascript:;' data-href='" . SRC . "relatorios&deletar={$result[$i]["id_relatorio"]}'>
                                        <img src='" . IMG . "delete.png'>
                                    </a>
                                </td>
                          </tr>";
            }
        }else{
            $lista = "<tr><td colspan='5'><br><br>Este beneficiário ainda não possui relatorios.<br><br><br></td></td>";
        }
        
        return $lista;
    }
    
    public function deletarArquivo($id_relatorio){
        $this->Query = new Select("relatorio");
        $this->Query->setWhere(array("id_relatorio" => $id_relatorio), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        if(!empty($result["arquivo_relatorio"])){
            
            $this->Query = new Update("relatorio", array("arquivo_relatorio" => ''));
            $this->Query->setWhere(array("id_relatorio" => $id_relatorio), true);
            $this->Connect->ExecuteBy($this->Query);
            
            $arquivo = explode("/", $result["arquivo_relatorio"]);
            $size_arquivo = sizeof($arquivo);
            $limit = $size_arquivo - 3;
            for ($i = 0; $i < $size_arquivo; $i++) {
                if($i >= $limit){
                    $new_arquivo[] = $arquivo[$i];
                }
            }
            $new_arquivo = implode("/", $new_arquivo);
            unlink($new_arquivo);
        }
    }
    
    private function setUpload() {
        
        $pasta = "upload/" . $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"] . "_" . $this->resetWords($_SESSION[SITE_NAME]["beneficiario"]["nome_beneficiario"]);
        
        $upload = new Upload();
        $arquivos = array();

        if(isset($_FILES["arquivo"]) && $_FILES["arquivo"]["name"] == ''){
            unset($_FILES["arquivo"]);
        }else{
            $upload->createPaste($pasta);
            $upload->setUpload("arquivo");
            $this->Dados["arquivo_relatorio"] = $upload->getPath();
        }
    }
    
    public function Criar($relatorio) {

        $this->Dados = $relatorio;
        $this->setUpload();
        $this->Query = new Insert("relatorio", $this->Dados);
        $this->Connect->InputBy($this->Query);

    }

    public function Editar($id) {
        $this->Query = new Select("relatorio");
        $this->Query->setWhere(array("id_relatorio" => $id), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        
        $this->Dados = $result;
        
        $files = array('jpeg' => 'FOTOGRAFIA', 'jpg' => 'FOTOGRAFIA', 'png' => 'FOTOGRAFIA', 'gif' => 'FOTOGRAFIA', 'pdf' => 'DOCUMENTO PDF', 'doc' => 'DOCUMENTO WORD', 'docx' => 'DOCUMENTO WORD', 'xlsx' => 'DOCUMENTO EXCEL');

        if(empty($this->Dados["arquivo_relatorio"])){
            $this->Dados["arquivo_relatorio"] = "<input type='file' name='arquivo' class='no-p bdr-none'>";
        }else{

            $ext = explode(".", $this->Dados["arquivo_relatorio"]);
            $ext = end($ext);

            $name = '';
            foreach ($files as $key => $value) {
                if($key == $ext){$name = $value;}
            }

            $this->Dados["arquivo_relatorio"] = "<a class='w-16' target='_blank'  href='" . SRC . $this->Dados["arquivo_relatorio"] . "'> <img src='".IMG."files.png'> {$name}</a><a class='tx-red tx-1' href='".SRC."relatorios&deletar_arquivo={$this->Dados["id_relatorio"]}'>&#10006;</a><input type='file' name='arquivo' class='dp-none'>";
        }
            $this->Dados["id_relatorio"] = "<input type='hidden' name='relatorio[id_relatorio]' value='{$this->Dados["id_relatorio"]}'>";
        
        
    }
    
    public function Alterar($relatorio) {
        $this->Dados = $relatorio;
        $this->setUpload();
        $this->Query = new Update("relatorio", $this->Dados);
        $this->Query->setWhere(array("id_relatorio" => $this->Dados["id_relatorio"]), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
    public function Deletar($id) {
        $this->deletarArquivo($id);
        $this->Query = new Delete("relatorio", array("id_relatorio" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }

}