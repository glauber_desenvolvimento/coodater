<?php
class Semovente {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;

    public function __construct($connect) {
        $this->Connect = $connect;
    }
    
    public function getDados() {
        $this->setSelect("tipo_semovente");
        return $this->Dados;
    }
    
    private function setSelect($select) {
        if(empty($this->Query) || (!empty($this->Query) && get_class($this->Query) <> "select")){
            $this->Query = new Select();
        }
        $this->Query->Reset($select);
        $GLOBALS['D']->OutputBy($this->Query, $result, $rows);

        $this->Dados[$select] = '';
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["id_{$select}"] == $result[$i]["id_{$select}"]) ? "selected" : '';
            $this->Dados[$select] .= 
                "<option {$selected} value='{$result[$i]["id_{$select}"]}'>
                    {$result[$i][$select]}
                </option>";
        }
    }
    
    
    public function setEmpty() {
        
        $this->Dados["id_beneficiario"] = $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"];
        $this->Dados["raca_semovente"] = "";
        $this->Dados["idade_semovente"] = "";
        $this->Dados["qtd_semovente"] = "1";
        $this->Dados["valor_semovente"] = "";
        $this->Dados["id_tipo_semovente"] = "";
        
    }
    
    public function Criar($composicao) {
        $this->Dados = $composicao;
        $this->Query = new Insert("semovente", $this->Dados);
        $this->Connect->InputBy($this->Query);

    }
    
    public function Ver($beneficiario) {
        
        $this->Query = new Select("semovente",
                array(
                    "id_semovente", "tipo_semovente", "raca_semovente", "idade_semovente", "qtd_semovente", "valor_semovente"
                )
        );
        $this->Query->setTables(array("tipo_semovente"));
        $this->Query->setWhere(array("id_beneficiario" => $beneficiario));
        $this->Query->setOrder("id_semovente DESC", true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $lista = '';
        if($rows > 0){
            for($i=0; $i<$rows; $i++){
                if($i%2 == 1){
                    $zebra = "class='bg-light-blue'";
                }else{
                    $zebra = '';
                }
                
                $result[$i]["valor_semovente"] = number_format($result[$i]["valor_semovente"] , 2, ",", ".");
                
                $lista .= "<tr {$zebra}>
                                <td>{$result[$i]["id_semovente"]}</td>
                                <td>{$result[$i]["tipo_semovente"]}</td>
                                <td>{$result[$i]["qtd_semovente"]}</td>
                                <td>{$result[$i]["idade_semovente"]}</td>
                                <td>{$result[$i]["raca_semovente"]}</td>
                                <td>{$result[$i]["valor_semovente"]}</td>
                                <td>
                                    <a class='w-3' href='" . SRC . "semoventes&editar={$result[$i]["id_semovente"]}'>
                                        <img src='" . IMG . "select.png'>
                                    </a>
                                </td>
                                <td>
                                    <a class='w-3 abre_confirma' href='javascript:;' data-href='" . SRC . "semoventes&deletar={$result[$i]["id_semovente"]}'>
                                        <img src='" . IMG . "delete.png'>
                                    </a>
                                </td>
                          </tr>";
            }
        }else{
            $lista = "<tr><td colspan='8'><br><br>Este beneficiário ainda não possui semoventes.<br><br><br></td></td>";
        }
        
        return $lista;
    }
    
    public function Editar($id) {
        $this->Query = new Select(
            "semovente",
            array(
                "id_semovente", "id_beneficiario", "id_tipo_semovente", "tipo_semovente", "raca_semovente", "idade_semovente", "qtd_semovente", "valor_semovente"
            )
        );
        $this->Query->setTables(array("tipo_semovente"));
        $this->Query->setWhere(array("id_semovente" => $id), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        $result["valor_semovente"] = str_replace(".", ",", $result["valor_semovente"]);
        $this->Dados = $result;
    }
    
    public function Alterar($semovente, $id) {
        $this->Query = new Update("semovente", $semovente);
        $this->Query->setWhere(array("id_semovente" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    public function Deletar($id) {
        $this->Query = new Delete("semovente", array("id_semovente" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
}