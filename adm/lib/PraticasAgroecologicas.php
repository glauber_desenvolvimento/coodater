<?php
class PraticasAgroecologicas {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;


    public function __construct($connect) {
        $this->Connect = $connect;
    }

    public function setEmpty($id_beneficiario){
        
        $this->Dados["mensagem"] = '<br>';
        $this->Dados["id_beneficiario"] = $id_beneficiario;
        
        $this->Query = new Select("praticas_agroecologicas_aux");
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $this->Dados['tipo_praticas_agroecologicas'] = '';
        for($i=0; $i<$rows; $i++){
            $this->Dados['tipo_praticas_agroecologicas'] .= 
                "<span class='m-bottom-s'>
                    <label class='w-23'>
                        <input class='flex' name='praticas_agroecologicas[id_praticas_agroecologicas_aux][]' value='{$result[$i]["id_praticas_agroecologicas_aux"]}' type='checkbox'> {$result[$i]["praticas_agroecologicas_aux"]}
                    </label>
                </span>";
        }
    }
    
    public function getDados($praticas_agroecologicas = NULL) {
        
        if(!empty($praticas_agroecologicas)){
            
            $this->Dados = $praticas_agroecologicas;

            $this->Query = new Select("praticas_agroecologicas_aux");
            $this->Connect->OutputBy($this->Query, $result, $rows);
            $this->Dados['tipo_praticas_agroecologicas'] = '';
            
            for($i=0; $i<$rows; $i++){
                
                $this->Query->Reset("praticas_agroecologicas", "id_praticas_agroecologicas_aux");
                $this->Query->setWhere(array("id_beneficiario" => $this->Dados["id_beneficiario"], "id_praticas_agroecologicas_aux" => $result[$i]["id_praticas_agroecologicas_aux"]), true, "AND");
                $this->Connect->OutputBy($this->Query, $tipos, $r_tipos, true);
                
                $checked = ($r_tipos == 1) ? "checked" : '';

                
                $this->Dados['tipo_praticas_agroecologicas'] .= 
                    "<span class='m-bottom-s'>
                        <label class='w-23'>
                            <input {$checked} class='flex' name='praticas_agroecologicas[id_praticas_agroecologicas_aux][]' value='{$result[$i]["id_praticas_agroecologicas_aux"]}' type='checkbox'> {$result[$i]["praticas_agroecologicas_aux"]}
                        </label>
                    </span>";
            }
        }
        
        return $this->Dados;
        
    }
    
    public function Alterar($praticas_agroecologicas) {
        
        $this->Dados = $praticas_agroecologicas;

        if(!empty($praticas_agroecologicas["id_praticas_agroecologicas_aux"])){
            $r_atual = sizeof($praticas_agroecologicas["id_praticas_agroecologicas_aux"]);
        }else{
            $r_atual = 0;
        }

        $this->Connect->Output("SELECT * FROM praticas_agroecologicas WHERE id_beneficiario = {$this->Dados["id_beneficiario"]}", $result, $r_anterior);
        
        if($r_atual == 0 && $r_anterior > 0){//RETIROU TODOS OS ITENS
            
            for ($i = 0; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM praticas_agroecologicas WHERE id_praticas_agroecologicas = {$result[$i]["id_praticas_agroecologicas"]}");
            }
            
        }elseif($r_anterior == $r_atual){//SÓ ATUALIZOU OS ITENS
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $this->Connect->Execute(
                    "UPDATE praticas_agroecologicas SET 
                    id_praticas_agroecologicas_aux = {$praticas_agroecologicas["id_praticas_agroecologicas_aux"][$i]}
                     WHERE id_praticas_agroecologicas = {$result[$i]["id_praticas_agroecologicas"]}"
                );
                    
            }

        }elseif($r_anterior > $r_atual){//FOI RETIRADO UM ITEM
            
            for ($i = 0; $i < $r_atual; $i++) {

                $this->Connect->Execute(
                    "UPDATE praticas_agroecologicas SET 
                    id_praticas_agroecologicas_aux = {$praticas_agroecologicas["id_praticas_agroecologicas_aux"][$i]}
                     WHERE id_praticas_agroecologicas = {$result[$i]["id_praticas_agroecologicas"]}"
                );
                    
            }
            
            for ($i = $r_atual; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM praticas_agroecologicas WHERE id_praticas_agroecologicas = {$result[$i]["id_praticas_agroecologicas"]}");
            }
            
        }elseif($r_anterior < $r_atual){//FOI ADICIONADO UM ITEM
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $this->Connect->Execute(
                    "UPDATE praticas_agroecologicas SET 
                    id_praticas_agroecologicas_aux = {$praticas_agroecologicas["id_praticas_agroecologicas_aux"][$i]}
                     WHERE id_praticas_agroecologicas = {$result[$i]["id_praticas_agroecologicas"]}"
                );
                    
            }
            
            for ($i = $r_anterior; $i < $r_atual; $i++) {
                $this->Connect->Input("INSERT INTO praticas_agroecologicas (id_beneficiario, id_praticas_agroecologicas_aux) VALUE ({$this->Dados["id_beneficiario"]}, {$praticas_agroecologicas["id_praticas_agroecologicas_aux"][$i]})");

            }
            
        }

       $this->Dados["Mensagem"] = "<div class='bx-radius p-b bg-white m-both-b tx-green'>Alterado com sucesso</div>";
        
    }
    
    public function Criar($praticas_agroecologicas) {
        
        $this->Dados = $praticas_agroecologicas;

        if(!empty($praticas_agroecologicas["id_praticas_agroecologicas_aux"])){
            $size = sizeof($praticas_agroecologicas["id_praticas_agroecologicas_aux"]);
        }else{
            $size = 0;
        }
        
        for ($i = 0; $i < $size; $i++) {

            $this->Connect->Input("INSERT INTO praticas_agroecologicas (id_beneficiario, id_praticas_agroecologicas_aux) VALUE ({$this->Dados["id_beneficiario"]}, {$this->Dados["id_praticas_agroecologicas_aux"][$i]})");
            
        }
        
    }
}