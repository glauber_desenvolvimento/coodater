<?php
class ProtecaoAmbiental {
    /** @var DAO */
    private $Connect;
    private $Query;
    /** @var View */
    private $View;
    private $Dados;


    public function __construct($connect, $view) {
        $this->Connect = $connect;
        $this->View = $view;
    }
    
    public function getDados(array $protecao = NULL) {
        
        if(!empty($protecao)){
            $this->Dados = $protecao;
            $this->Dados["medida_reserva"] = str_replace(".", ",", $this->Dados["medida_reserva"]);
            $this->Dados["medida_apps"] = str_replace(".", ",", $this->Dados["medida_apps"]);
            $this->Dados["Sutilizacao_apps"] = '';
            $this->Dados["Nutilizacao_apps"] = '';
            $this->Dados["Sutilizacao_reserva"] = '';
            $this->Dados["Nutilizacao_reserva"] = '';
            $this->Dados[$this->Dados["utilizacao_apps"] . "utilizacao_apps"] = 'checked';
            $this->Dados[$this->Dados["utilizacao_reserva"] . "utilizacao_reserva"] = 'checked';
            $this->Dados["mensagem"] = '<br>';
        }
        
        return $this->Dados;
    }

    public function setEmpty($id_beneficiario){
        $this->Connect->Output("SELECT MAX(id_protecao_ambiental) as id_protecao_ambiental FROM protecao_ambiental", $max, $row, true);
        $this->Query = new Select("protecao_ambiental");
        $this->Query->setWhere(array("id_protecao_ambiental" => $max["id_protecao_ambiental"]), true);
        $this->Connect->OutputBy( $this->Query, $result, $rows, true);
        $this->Dados = $result;
        $this->Dados["mensagem"] = '<br>';
        $this->Dados["id_beneficiario"] = $id_beneficiario;

    }

    public function Alterar($protecao_ambiental) {
        
        $protecao_ambiental["medida_reserva"] = str_replace(",", ".", $protecao_ambiental["medida_reserva"]);
        $protecao_ambiental["medida_apps"] = str_replace(",", ".", $protecao_ambiental["medida_apps"]);
        
        $this->Dados = $protecao_ambiental;
        $this->Query = new Update("protecao_ambiental", $this->Dados);
        $this->Query->setWhere(array("id_protecao_ambiental" => $this->Dados["id_protecao_ambiental"]), true);
        $this->Connect->ExecuteBy($this->Query);
        
        $this->setMensagem("Área de proteção ambiental alterada com sucesso", SRC . "protecao_ambiental");
        
        $this->Dados["medida_reserva"] = str_replace(".", ",", $this->Dados["medida_reserva"]);
        $this->Dados["medida_apps"] = str_replace(".", ",", $this->Dados["medida_apps"]);
        
    }
    public function Criar($protecao_ambiental) {
        
        $protecao_ambiental["medida_reserva"] = str_replace(",", ".", $protecao_ambiental["medida_reserva"]);
        $protecao_ambiental["medida_apps"] = str_replace(",", ".", $protecao_ambiental["medida_apps"]);
        
        $this->Dados = $protecao_ambiental;
        $this->Query = new Update("protecao_ambiental", $this->Dados);
        $this->Query->setWhere(array("id_protecao_ambiental" => $this->Dados["id_protecao_ambiental"]), true);
        $this->Connect->ExecuteBy($this->Query);
        $this->Connect->Input("INSERT INTO protecao_ambiental (medida_reserva) VALUE (NULL)");
        
        $this->setMensagem("Área de proteção ambiental criada com sucesso", SRC . "protecao_ambiental");
        
        $this->Dados["medida_reserva"] = str_replace(".", ",", $this->Dados["medida_reserva"]);
        $this->Dados["medida_apps"] = str_replace(".", ",", $this->Dados["medida_apps"]);
        
        
    }

    
    public function setMensagem($mensagem = NULL, $caminho = NULL) {
        if(func_num_args() == 2){
            $this->Dados["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     $mensagem
                 </div>";
            $this->Dados["mensagem"] .= 
            "<script>
                 $(function(){
                     window.setTimeout(
                         function(){
                             window.open('" . $caminho . "', '_self');
                         }, 1000
                     );
                 });
             </script>";
        }else{
            $this->Dados["mensagem"] = '';
        }
    }

}