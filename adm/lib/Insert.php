<?php
/**
 * <b>Classe Select</b> - Cria Objeto de consultas
 *    Classe que cria
 *    
 * @copyright 2014 Glauber Gonçalves dos Santos
 */
class Insert {
    private $Values;
    private $Query = "INSERT INTO ";

    public function __construct($Table = NULL, array $Data = NULL) {
        if(func_num_args() > 1):
            $columns = implode(", ", array_keys($Data));
            $values = ':' . implode(", :", array_keys($Data));
            $this->Values = $Data;
            $this->Query .= "{$Table} ({$columns}) VALUES ({$values})";
        endif;
    }

    public function Reset($Table, array $Data) {
        
        $this->Query = "INSERT INTO ";
        
        $columns = implode(', ', array_keys($Data));
        $values = ":" . implode(', :', array_keys($Data));
        $this->Values = $Data;
        $this->Query .= "{$Table} ({$columns}) VALUES ({$values})";
    }
    
    public function Multiples($Table, array $Values) {
        if($this->isMatrix($Values)):
            $columns = implode(", ", array_keys($Values[0]));
            $values = '';
            
            for($i=0; $i<sizeof($Values); $i++):
                $values .= "(";
                $j = 0;
                do{
                    $this->Values[key($Values[$i]) . "_" . $i] = $Values[$i][key($Values[$i])];
                    $values .= ":" . key($Values[$i]) . "_" . $i;
                    if($j++<(sizeof($Values[$i])-1)):
                        $values .= ", ";
                    endif;
                }while(next($Values[$i]));
                $values .= ")";
                if($i<(sizeof($Values)-1)):
                    $values .= ", ";
                endif;
            endfor;
            $this->Query .= "{$Table} ({$columns}) VALUES {$values}";
        endif;
    }
    
    private function isMatrix(array $array) {
        $aux = 0;
        for($i=0; $i<sizeof($array); $i++):
            if(is_array($array[$i])):
                $aux++;
            endif;
        endfor;
        if($aux == sizeof($array)):
            return true;
        else:
            return false;
        endif;
    }
    
    public function getValues() {
        return $this->Values;
    }

    public function getQuery() {
        return $this->Query;
    }

    public function setValues(array $Values) {
        $this->Values = $Values;
    }

    public function setQuery($Query) {
        $this->Query = $Query;
    }
    
    public function __toString(){
        return $this->Query;
    }
}
