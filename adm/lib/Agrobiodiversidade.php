<?php
class Agrobiodiversidade {
    /** @var DAO */
    private $Connect;
    private $Query;
    /** @var View */
    private $View;
    private $Dados;


    public function __construct($connect, $view) {
        $this->Connect = $connect;
        $this->View = $view;
    }
    
    public function getDados(array $agrobiodiversidade = NULL) {
        
        if(!empty($agrobiodiversidade)){
            $this->Dados = $agrobiodiversidade;
            $this->Dados["mensagem"] = '<br>';
        }
        
        return $this->Dados;
    }

    public function setEmpty($id_beneficiario){
        $this->Connect->Output("SELECT MAX(id_agrobiodiversidade) as id_agrobiodiversidade FROM agrobiodiversidade", $max, $row, true);
        $this->Query = new Select("agrobiodiversidade");
        $this->Query->setWhere(array("id_agrobiodiversidade" => $max["id_agrobiodiversidade"]), true);
        $this->Connect->OutputBy( $this->Query, $result, $rows, true);
        $this->Dados = $result;
        $this->Dados["mensagem"] = '<br>';
        $this->Dados["id_beneficiario"] = $id_beneficiario;

    }

    public function Alterar($agrobiodiversidade) {
        
        $this->Dados = $agrobiodiversidade;
        $this->Query = new Update("agrobiodiversidade", $this->Dados);
        $this->Query->setWhere(array("id_agrobiodiversidade" => $this->Dados["id_agrobiodiversidade"]), true);
        $this->Connect->ExecuteBy($this->Query);
        
        $this->setMensagem("Espécies cultivadas alteradas com sucesso", SRC . "agrobiodiversidade");
        
        
    }
    public function Criar($agrobiodiversidade) {
        
        $this->Dados = $agrobiodiversidade;
        $this->Query = new Update("agrobiodiversidade", $this->Dados);
        $this->Query->setWhere(array("id_agrobiodiversidade" => $this->Dados["id_agrobiodiversidade"]), true);
        $this->Connect->ExecuteBy($this->Query);
        $this->Connect->Input("INSERT INTO agrobiodiversidade (id_beneficiario) VALUE (NULL)");
        
        $this->setMensagem("Espécies cultivadas criadas com sucesso", SRC . "agrobiodiversidade");
        
    }

    
    public function setMensagem($mensagem = NULL, $caminho = NULL) {
        if(func_num_args() == 2){
            $this->Dados["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     $mensagem
                 </div>";
            $this->Dados["mensagem"] .= 
            "<script>
                 $(function(){
                     window.setTimeout(
                         function(){
                             window.open('" . $caminho . "', '_self');
                         }, 1000
                     );
                 });
             </script>";
        }else{
            $this->Dados["mensagem"] = '';
        }
    }

}