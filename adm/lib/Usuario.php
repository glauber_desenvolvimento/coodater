<?php

class Usuario {
    /** @var DAO */
    private $Connect;
    /** @var View */
    private $View;
    private $Query;
    private $Dados;
    const Limit = 120;
    const ByPage = 12;
    
    
    function __construct(DAO $Connect, View $View) {
        $this->Connect = $Connect;
        $this->View = $View;
    }
    
    public function Ver($order, $desc = false, $index = NULL, $pesquisa = NULL, $criterio = NULL) {
        $this->Connect->Output("SELECT MAX(id_usuario) AS max FROM usuario", $id_max, $row, true);
        $max = $id_max["max"];
        $this->Query = new Select(
                "usuario", 
                array(
                    "id_usuario",
                    "nome_usuario",
                    "login_usuario",
                    "funcao_usuario",
                    "status_usuario"
                ),
                self::Limit
            );
        $clause = " WHERE id_usuario <> {$max} AND id_usuario <> 1";
        $p = false;
        if(!empty($pesquisa) && !empty($criterio)){
            $p = true;
            $clause .= " AND " . $criterio . " LIKE '%{$pesquisa}%'";
        }
        if(is_numeric($index)){
            $ids = implode(", ", $_SESSION["nav"]->getIds((int) $index));
            $clause .= " AND id_usuario IN(" . $ids . ")";
        }
        $this->Query->setClause($clause);
        $this->Query->setOrder($order . (($desc) ? " DESC" : ''));
        $this->Query->End();
        $this->Connect->OutputBy($this->Query, $tabelas, $r_tabelas);
        if(is_numeric($index)){
            $this->setPaginator();
        }else{
            $this->navCreate($r_tabelas, $r_tabelas, $tabelas, "id_usuario");
        }
        $this->setLinhas($tabelas, $r_tabelas, $p);
    }
    
    private function setLinhas(array $dados, $rows, $pesquisa = false) {
        $this->Dados["linhas"] = '';
        if(!empty($dados)){
            for($i=0; $i<$rows; $i++){
                if(!array_key_exists($i, $dados)){break;}
                $dados[$i]["class"] = ($i%2 <> 0) ? "class='bg-light-blue'" : '';   
                $dados[$i]["bloqueio"] = ($dados[$i]["status_usuario"]) ? "Ativo" : "Inativo";
                $dados[$i]["funcao"]   = ($dados[$i]["funcao_usuario"]) ? "Administrador" : "Técnico";
                $this->Dados["linhas"] .= $this->View->prepareView("usuario/linha", $dados[$i]);
            }
        }else{
            if($pesquisa){
                $this->Dados["linhas"] = "<tr><td colspan='7'>Não foi encontrado nenhum resultado.</td></td>";
            }else{
                $this->Dados["linhas"] = "<tr><td colspan='7'>Não foi cadastrado nenhum usuario.</td></td>";
            }
        }
    }
    
    private function setPaginator() {
        $this->Dados["paginator"] = 
            $_SESSION['nav']->Paginator(
                "usuario/selecionar", 
                "a-box-2 w-3 al-center bx-radius"
            );
    }
    
    private function navCreate($rows, $bypage, array $result, $key){
        if($rows > 0){
            if(isset($_SESSION['nav'])) unset($_SESSION['nav']);
            $_SESSION['nav'] = new Navigator($rows, $bypage);
            $_SESSION['nav']->setIds($result, $key);
            $this->setPaginator();
        }else{
            $this->Dados["paginator"] = '';
        }
    }
    
    public function Criar(array $usuario) {
        $this->Dados = $usuario;
        $this->Dados["senha_usuario"] = md5($this->Dados["senha_usuario"]);
        $this->Connect->Output("SELECT * FROM usuario WHERE login_usuario = '{$this->Dados["login_usuario"]}'", $result, $rows, true);
        
        if($rows == 0){
            $this->Query = new Update("usuario", $this->Dados);
            $this->Query->setWhere(array("id_usuario" => $this->Dados["id_usuario"]), true);
            $this->Connect->ExecuteBy($this->Query);
            $this->Connect->Input("INSERT INTO usuario (status_usuario) VALUE (0)");
            $this->Dados["mensagem"] = "<div class='bx-radius p-b bg-white m-both-b tx-green'>Usuário criado com sucesso.</div>";
            $this->Dados["mensagem"] .= "<script>
                 $(function(){
                     window.setTimeout(
                         function(){
                             window.open('" . SRC . "usuario', '_self');
                         }, 1000
                     );
                 });
             </script>";
        }else{
            $this->Dados["mensagem"] = "<div class='bx-radius p-b bg-white m-both-b tx-red'>Atenção, Já existe um usuário com o login '{$this->Dados["login_usuario"]}'. Redefina o login e digite novamente a senha.</div>";
        }
        $this->Dados['action'] = "criar";
        $this->Dados['value']  = "Criar";
    }
    
    public function Alterar(array $usuario, $id) {
        $this->Dados = $usuario;
        $this->Dados["senha_usuario"] = md5($this->Dados["senha_usuario"]);
        $this->Query = new Update("usuario", $this->Dados);
        $this->Query->setWhere(array("id_usuario" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
        
        $this->Dados["mensagem"] = "<div id='message' class='bx-radius p-b bg-white m-both-b tx-green'>Usuário alterado com sucesso.</div>";
        $this->Dados["mensagem"] .= "<script>
                 $(function(){
                     window.setTimeout(
                         function(){
                             $('#message').fadeOut();
                         }, 1000
                     );
                 });
             </script>";
        $this->Dados['action'] = "alterar";
        $this->Dados['value']  = "Alterar";
        
    }
    
    public function Deletar($id) {
        $this->Query = new Delete("usuario", array("id_usuario" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
    public function getDados($act = false) {
        if($act){
            $this->Dados["0funcao_usuario"] = '';
            $this->Dados["1funcao_usuario"] = '';
            $this->Dados[$this->Dados["funcao_usuario"] . "funcao_usuario"] = "selected";
            $this->Dados["0status_usuario"] = '';
            $this->Dados["1status_usuario"] = '';
            $this->Dados[$this->Dados["status_usuario"] . "status_usuario"] = "checked";
            $this->Dados["readonly"]        = "class='bg-gray' readonly";
        }
        return $this->Dados;
        
    }
    
    public function GetById($id) {
        $this->Connect->Output("SELECT * FROM usuario WHERE id_usuario = {$id}", $result, $rows, true);
        if($rows == 1){
            $this->Dados = $result;
            $this->Dados["mensagem"] = '';
            $this->Dados["0funcao_usuario"] = '';
            $this->Dados["1funcao_usuario"] = '';
            $this->Dados["0status_usuario"] = '';
            $this->Dados["1status_usuario"] = '';
            $this->Dados["readonly"]        = "class='bg-gray' readonly";
            $this->Dados['action'] = "alterar";
            $this->Dados['value']  = "Alterar";
            $this->Dados[$this->Dados["funcao_usuario"] . "funcao_usuario"] = "selected";
            $this->Dados[$this->Dados["status_usuario"] . "status_usuario"] = "checked";
        }else{
            $this->setEmpty();
            $this->Dados['action'] = "criar";
            $this->Dados['value']  = "Criar";
        }
        
    }
    
    public function setEmpty(){
        $this->Connect->Output("SELECT MAX(id_usuario) as id_usuario FROM usuario", $max, $row, true);
        $this->Query = new Select("usuario");
        $this->Query->setWhere(array("id_usuario" => $max["id_usuario"]), true);
        $this->Connect->OutputBy( $this->Query, $result, $rows, true);
        $this->Dados = $result;
        $this->Dados['action']          = "criar";
        $this->Dados['value']           = "Criar";
        $this->Dados["0funcao_usuario"] = '';
        $this->Dados["1funcao_usuario"] = '';
        $this->Dados["0status_usuario"] = '';
        $this->Dados["1status_usuario"] = '';
        $this->Dados["readonly"]        = '';
        $this->Dados["mensagem"]        = '';
    }
}
