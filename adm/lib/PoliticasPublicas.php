<?php
class PoliticasPublicas {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;


    public function __construct($connect) {
        $this->Connect = $connect;
    }

    public function setEmpty($id_beneficiario){
        $this->Connect->Output("SELECT MAX(id_politicas_publicas) as id_politicas_publicas FROM politicas_publicas", $max, $row, true);
        $this->Query = new Select("politicas_publicas");
        $this->Query->setWhere(array("id_politicas_publicas" => $max["id_politicas_publicas"]), true);
        $this->Connect->OutputBy( $this->Query, $result, $rows, true);
        $this->Dados = $result;
        $this->Dados["mensagem"] = '<br>';
        $this->Dados["id_beneficiario"] = $id_beneficiario;
        
        $this->Query->Reset("tipo_politicas_publicas_aux");
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $this->Dados['tipo_politicas_publicas'] = '';
        for($i=0; $i<$rows; $i++){
            $this->Dados['tipo_politicas_publicas'] .= 
                "<span class='m-bottom-s'>
                    <label class='w-15'>
                        <input class='flex' name='tipo_politicas_publicas[id_tipo_politicas_publicas_aux][]' value='{$result[$i]["id_tipo_politicas_publicas_aux"]}' type='checkbox' data-check='{$result[$i]["id_tipo_politicas_publicas_aux"]}'> {$result[$i]["tipo_politicas_publicas_aux"]}
                    </label>
                    
                    <label>Valor/Mês: <input class='flex' name='tipo_politicas_publicas[valor_politicas_publicas][]' type='text' pattern='[0-9]{1,7},{0,1}[0-9]{1,2}$' placeholder='999.999,99' data-value='{$result[$i]["id_tipo_politicas_publicas_aux"]}'></label>
                </span>
                <br>";
        }
        
        $this->Query->Reset("politicas_publicas_aux");
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $this->Dados['pronaf']        = '';
        $this->Dados['financiamento'] = '';
        for($i=0; $i<$rows; $i++){
            $this->Dados['pronaf'] .= 
                "<label class='flex'><input required class='flex' name='politicas_publicas[id_pronaf]' value='{$result[$i]["id_politicas_publicas_aux"]}' type='radio'> {$result[$i]["politicas_publicas_aux"]}</label><br>";
        }
        for($i=0; $i<$rows; $i++){
            $this->Dados['financiamento'] .= 
                "<label class='flex'><input required class='flex' name='politicas_publicas[id_financiamento]' value='{$result[$i]["id_politicas_publicas_aux"]}' type='radio'> {$result[$i]["politicas_publicas_aux"]}</label><br>";
        }
    }
    
    public function getDados($politicas_publicas = NULL) {
        
        if(!empty($politicas_publicas)){
            
            $this->Dados = $politicas_publicas;
            
            $this->Query = new Select("politicas_publicas_aux");
            $this->Connect->OutputBy($this->Query, $result, $rows);
            $this->Dados['pronaf']        = '';
            $this->Dados['financiamento'] = '';
            for($i=0; $i<$rows; $i++){
                $checked = ($this->Dados["id_pronaf"] == $result[$i]["id_politicas_publicas_aux"]) ? "checked" : '';
                $this->Dados['pronaf'] .= 
                    "<label class='flex'><input {$checked} required class='flex' name='politicas_publicas[id_pronaf]' value='{$result[$i]["id_politicas_publicas_aux"]}' type='radio'> {$result[$i]["politicas_publicas_aux"]}</label><br>";
            }
            for($i=0; $i<$rows; $i++){
                $checked = ($this->Dados["id_financiamento"] == $result[$i]["id_politicas_publicas_aux"]) ? "checked" : '';
                $this->Dados['financiamento'] .= 
                    "<label class='flex'><input {$checked} required class='flex' name='politicas_publicas[id_financiamento]' value='{$result[$i]["id_politicas_publicas_aux"]}' type='radio'> {$result[$i]["politicas_publicas_aux"]}</label><br>";
            }
            
            $this->Query->Reset("tipo_politicas_publicas_aux");
            $this->Connect->OutputBy($this->Query, $result, $rows);
            $this->Dados['tipo_politicas_publicas'] = '';
            
            for($i=0; $i<$rows; $i++){
                
                $this->Query->Reset("tipo_politicas_publicas", array("id_tipo_politicas_publicas_aux", "valor_politicas_publicas"));
                $this->Query->setWhere(array("id_politicas_publicas" => $this->Dados['id_politicas_publicas'], "id_tipo_politicas_publicas_aux" => $result[$i]["id_tipo_politicas_publicas_aux"]), true, "AND");
                $this->Connect->OutputBy($this->Query, $tipos, $r_tipos, true);
                
                if($r_tipos == 1){
                    $checked = "checked";
                    $valor   = str_replace(".", ",", $tipos["valor_politicas_publicas"]);
                }else{
                    $checked = '';
                    $valor   = '';
                }
                
                $this->Dados['tipo_politicas_publicas'] .= 
                    "<span class='m-bottom-s'>
                        <label class='w-15'>
                            <input {$checked} class='flex' name='tipo_politicas_publicas[id_tipo_politicas_publicas_aux][]' value='{$result[$i]["id_tipo_politicas_publicas_aux"]}' type='checkbox' data-check='{$result[$i]["id_tipo_politicas_publicas_aux"]}'> {$result[$i]["tipo_politicas_publicas_aux"]}
                        </label>

                        <label>Valor/Mês: <input class='flex' name='tipo_politicas_publicas[valor_politicas_publicas][]' type='text' pattern='[0-9]{1,7},{0,1}[0-9]{1,2}$' placeholder='999.999,99' data-value='{$result[$i]["id_tipo_politicas_publicas_aux"]}' value='{$valor}'></label>
                    </span>
                    <br>";
            }
        }
        
        return $this->Dados;
        
    }
    
    public function Alterar($politicas_publicas, $tipo_politicas_publicas) {
        
        $new_array = array();
        foreach ($tipo_politicas_publicas["valor_politicas_publicas"] as $value) {
            if(!empty($value)){
                $new_array[] = $value;
            }
        }
        $tipo_politicas_publicas["valor_politicas_publicas"] = $new_array;
        
        $this->Dados = $politicas_publicas;
        
        $this->Query = new Update("politicas_publicas", $this->Dados);
        $this->Query->setWhere(array("id_politicas_publicas" => $this->Dados["id_politicas_publicas"]), true);
        $this->Connect->ExecuteBy($this->Query);
        
        if(!empty($tipo_politicas_publicas["id_tipo_politicas_publicas_aux"])){
            $r_atual = sizeof($tipo_politicas_publicas["id_tipo_politicas_publicas_aux"]);
        }else{
            $r_atual = 0;
        }
        

        $this->Connect->Output("SELECT * FROM tipo_politicas_publicas WHERE id_politicas_publicas = {$this->Dados["id_politicas_publicas"]}", $result, $r_anterior);
        
        if($r_atual == 0 && $r_anterior > 0){//RETIROU TODOS OS ITENS
            
            for ($i = 0; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM tipo_politicas_publicas WHERE id_tipo_politicas_publicas = {$result[$i]["id_tipo_politicas_publicas"]}");
            }
            
        }elseif($r_anterior == $r_atual){//SÓ ATUALIZOU OS ITENS
            for ($i = 0; $i < $r_anterior; $i++) {

                $tipo_politicas_publicas["valor_politicas_publicas"][$i] = str_replace(",", ".", $tipo_politicas_publicas["valor_politicas_publicas"][$i]);
                
                $this->Connect->Execute(
                    "UPDATE tipo_politicas_publicas SET 
                    id_tipo_politicas_publicas_aux = {$tipo_politicas_publicas["id_tipo_politicas_publicas_aux"][$i]}, 
                    valor_politicas_publicas = {$tipo_politicas_publicas["valor_politicas_publicas"][$i]}, 
                    id_politicas_publicas = {$this->Dados["id_politicas_publicas"]} 
                    WHERE id_tipo_politicas_publicas = {$result[$i]["id_tipo_politicas_publicas"]}"
                );
                    
            }
        
        }elseif($r_anterior > $r_atual){//FOI RETIRADO UM ITEM
            
            for ($i = 0; $i < $r_atual; $i++) {

                $tipo_politicas_publicas["valor_politicas_publicas"][$i] = str_replace(",", ".", $tipo_politicas_publicas["valor_politicas_publicas"][$i]);

                $this->Connect->Execute(
                    "UPDATE tipo_politicas_publicas SET 
                    id_tipo_politicas_publicas_aux = {$tipo_politicas_publicas["id_tipo_politicas_publicas_aux"][$i]}, 
                    valor_politicas_publicas = {$tipo_politicas_publicas["valor_politicas_publicas"][$i]}, 
                    id_politicas_publicas = {$this->Dados["id_politicas_publicas"]} 
                    WHERE id_tipo_politicas_publicas = {$result[$i]["id_tipo_politicas_publicas"]}"
                );
                    
            }
            
            for ($i = $r_atual; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM tipo_politicas_publicas WHERE id_tipo_politicas_publicas = {$result[$i]["id_tipo_politicas_publicas"]}");
            }
            
        }elseif($r_anterior < $r_atual){//FOI ADICIONADO UM ITEM
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $tipo_politicas_publicas["valor_politicas_publicas"][$i] = str_replace(",", ".", $tipo_politicas_publicas["valor_politicas_publicas"][$i]);

                $this->Connect->Execute(
                    "UPDATE tipo_politicas_publicas SET 
                    id_tipo_politicas_publicas_aux = {$tipo_politicas_publicas["id_tipo_politicas_publicas_aux"][$i]}, 
                    valor_politicas_publicas = {$tipo_politicas_publicas["valor_politicas_publicas"][$i]}, 
                    id_politicas_publicas = {$this->Dados["id_politicas_publicas"]} 
                    WHERE id_tipo_politicas_publicas = {$result[$i]["id_tipo_politicas_publicas"]}"
                );
                    
            }
            
            for ($i = $r_anterior; $i < $r_atual; $i++) {
                $this->Connect->Input("INSERT INTO tipo_politicas_publicas (id_politicas_publicas, id_tipo_politicas_publicas_aux, valor_politicas_publicas) VALUE ({$this->Dados["id_politicas_publicas"]}, {$tipo_politicas_publicas["id_tipo_politicas_publicas_aux"][$i]}, {$tipo_politicas_publicas["valor_politicas_publicas"][$i]})");

            }
            
        }

       $this->Dados["Mensagem"] = "<div class='bx-radius p-b bg-white m-both-b tx-green'>Alterado com sucesso</div>";
        
    }
    
    public function Criar($politicas_publicas, $tipo_politicas_publicas) {
        
        $new_array = array();
        foreach ($tipo_politicas_publicas["valor_politicas_publicas"] as $value) {
            if(!empty($value)){
                $new_array[] = $value;
            }
        }
        $tipo_politicas_publicas["valor_politicas_publicas"] = $new_array;
        
        $this->Dados = $politicas_publicas;
        
        $this->Query = new Update("politicas_publicas", $this->Dados);
        $this->Query->setWhere(array("id_politicas_publicas" => $this->Dados["id_politicas_publicas"]), true);
        $this->Connect->ExecuteBy($this->Query);
        $this->Connect->Input("INSERT INTO politicas_publicas (id_beneficiario) VALUE (NULL)");
        
        if(!empty($tipo_politicas_publicas["id_tipo_politicas_publicas_aux"])){
            $size = sizeof($tipo_politicas_publicas["id_tipo_politicas_publicas_aux"]);
        }else{
            $size = 0;
        }
        
        for ($i = 0; $i < $size; $i++) {
            
            $tipo_politicas_publicas["valor_politicas_publicas"][$i] = str_replace(",", ".", $tipo_politicas_publicas["valor_politicas_publicas"][$i]);

            $this->Connect->Input("INSERT INTO tipo_politicas_publicas (id_politicas_publicas, id_tipo_politicas_publicas_aux, valor_politicas_publicas) VALUE ({$this->Dados["id_politicas_publicas"]}, {$tipo_politicas_publicas["id_tipo_politicas_publicas_aux"][$i]}, {$tipo_politicas_publicas["valor_politicas_publicas"][$i]})");
            
        }
        $this->Dados["Mensagem"] = "<div class='bx-radius p-b bg-white m-both-b tx-green'>Alterado com sucesso</div>            <script>
                 $(function(){
                     window.setTimeout(
                         function(){
                             window.open('" . SRC . "acesso_a_politicas_publicas', '_self');
                         }, 1000
                     );
            });</script>";
    }
}