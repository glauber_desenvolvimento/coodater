<?php
/**
 * <b>Classe Select</b> - Cria Objeto de consultas
 *    Classe que cria
 *    
 * @copyright 2014 Glauber Gonçalves dos Santos
 */
class Delete {
    private $Values;
    private $Query = "DELETE FROM ";
    private $Clause = '';

    public function __construct($Tables = NULL, array $Where = NULL, $end = false, $Operator = '=', $rule = 'OR') {
        $this->Query .= (!empty($Tables)) ? $Tables : NULL;
        if(func_num_args() > 1):
            if(sizeof($Where) == 1 && is_array($Where)):
                $this->Clause = " WHERE " . key($Where) . " {$Operator} :" . key($Where);
                $this->Values = $Where;
            elseif(sizeof($Where) > 1 && is_array($Where)):
                $this->Clause = " WHERE ";
                $i = 0;
                do{
                    if($i++ <> (sizeof($Where)-1)):
                        $this->Clause .= key($Where) . " {$Operator} :" . key($Where) . " {$rule} ";
                    else:
                        $this->Clause .= key($Where) . " {$Operator} :" . key($Where);
                    endif;
                }while(next($Where));
                $this->Values = $Where;
            endif;
            if($end):
                $this->End();
            endif;
        endif;
    }
    
    public function Reset($Tables, array $Where = NULL, $end = false, $Operator = '=', $rule = 'OR') {
        
        $this->Values = NULL;
        $this->Query = "DELETE FROM ";
        $this->Clause = '';
        
        $this->Query .= $Tables;
        if(func_num_args() > 1):
            if(sizeof($Where) == 1 && is_array($Where)):
                $this->Clause = " WHERE " . key($Where) . " {$Operator} :" . key($Where);
                $this->Values = $Where;
            elseif(sizeof($Where) > 1 && is_array($Where)):
                $this->Clause = " WHERE ";
                $i = 0;
                do{
                    if($i++ <> (sizeof($Where)-1)):
                        $this->Clause .= key($Where) . " {$Operator} :" . key($Where) . " {$rule} ";
                    else:
                        $this->Clause .= key($Where) . " {$Operator} :" . key($Where);
                    endif;
                }while(next($Where));
                $this->Values = $Where;
            endif;
            if($end):
                $this->End();
            endif;
        endif;
    }
    
    public function setWhere(array $Where, $end = false, $rule = 'OR', $Operator = '='){
        if(sizeof($Where) == 1):
            $this->Clause = " WHERE " . key($Where) . " {$Operator} :" . key($Where);
        else:
            $this->Clause = " WHERE ";
            $i = 0;
            do{
                if($i++ <> (sizeof($Where)-1)):
                    $this->Clause .= key($Where) . " {$Operator} :" . key($Where) . " {$rule} ";
                else:
                    $this->Clause .= key($Where) . " {$Operator} :" . key($Where);
                endif;
            }while(next($Where));
        endif;
        $this->Values = $Where;
        if($end):
            $this->End();
        endif;
    }
    
    public function getClause() {
        return $this->Clause;
    }
    
    public function setClause($Clause, array $Values = NULL, $end = false) {
        $this->Clause = $Clause;
        $this->Values = (!empty($Values)) ? $Values : $this->Values;
        if($end){
            $this->End();
        }
    }
    
    public function End(){
        $this->Query .= $this->Clause;
        $this->Clause = NULL;
    }
    
    public function getValues() {
        return $this->Values;
    }

    public function getQuery() {
        return $this->Query;
    }
    
    public function setValues(array $Values) {
        $this->Values = $Values;
    }

    public function setQuery($Query) {
        $this->Query = $Query;
    }
    
    public function __toString(){
        return $this->Query;
    }
}
