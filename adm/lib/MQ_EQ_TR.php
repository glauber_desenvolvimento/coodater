<?php
class MQ_EQ_TR {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;

    public function __construct($connect) {
        $this->Connect = $connect;
    }
    
    public function getDados() {
        $this->Query = new Select("medida");
        $this->Query->setIn("id_medida", array(6,7,8), true);
        $GLOBALS['D']->OutputBy($this->Query, $result, $rows);
        $this->Dados["medida"] = '';
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["id_medida"] == $result[$i]["id_medida"]) ? "selected" : '';
            $this->Dados["medida"] .= 
                "<option {$selected} value='{$result[$i]["id_medida"]}'>
                    {$result[$i]["medida"]}
                </option>";
        }
        $this->Query->Reset("potencia");
        $this->Query->setOrder("potencia", true);
        $GLOBALS['D']->OutputBy($this->Query, $result, $rows);
        $this->Dados["potencia"] = '';
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["id_potencia"] == $result[$i]["id_potencia"]) ? "selected" : '';
            $this->Dados["potencia"] .= 
                "<option {$selected} value='{$result[$i]["id_potencia"]}'>
                    {$result[$i]["potencia"]}
                </option>";
        }
        
        
        return $this->Dados;
    }
    
    public function setEmpty() {
        
        $this->Dados["id_beneficiario"] = $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"];
        $this->Dados["especificacao_mq_eq_tr"] = "";
        $this->Dados["capacidade_mq_eq_tr"] = "";
        $this->Dados["qtd_mq_eq_tr"] = "1";
        $this->Dados["id_medida"] = "";
        $this->Dados["id_potencia"] = "";
        $this->Dados["potencia_mq_eq_tr"] = "1";
        
    }
    
    public function Criar($mq_eq_tr) {
        $this->Dados = $mq_eq_tr;
        $this->Query = new Insert("mq_eq_tr", $this->Dados);
        $this->Connect->InputBy($this->Query);

    }
    
    public function Ver($mq_eq_tr) {
        
        $this->Query = new Select("mq_eq_tr",
                array(
                    "id_mq_eq_tr", "especificacao_mq_eq_tr", "medida", "potencia", "capacidade_mq_eq_tr", "qtd_mq_eq_tr", "potencia_mq_eq_tr"
                )
        );
        $this->Query->setTables(array("medida", "potencia"));
        $this->Query->setWhere(array("id_beneficiario" => $mq_eq_tr));
        $this->Query->setOrder("id_mq_eq_tr DESC", true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $lista = '';
        if($rows > 0){
            for($i=0; $i<$rows; $i++){
                if($i%2 == 1){
                    $zebra = "class='bg-light-blue'";
                }else{
                    $zebra = '';
                }
                
                $result[$i]["capacidade_mq_eq_tr"] = "{$result[$i]["capacidade_mq_eq_tr"]} {$result[$i]["medida"]}";
                $result[$i]["potencia_mq_eq_tr"] = "{$result[$i]["potencia_mq_eq_tr"]} {$result[$i]["potencia"]}";
                
                $lista .= "<tr {$zebra}>
                                <td>{$result[$i]["id_mq_eq_tr"]}</td>
                                <td>{$result[$i]["especificacao_mq_eq_tr"]}</td>
                                <td>{$result[$i]["qtd_mq_eq_tr"]}</td>
                                <td>{$result[$i]["capacidade_mq_eq_tr"]}</td>
                                <td>{$result[$i]["potencia_mq_eq_tr"]}</td>
                                <td>
                                    <a class='w-3' href='" . SRC . "maquinas_equipamentos_de_trabalho&editar={$result[$i]["id_mq_eq_tr"]}'>
                                        <img src='" . IMG . "select.png'>
                                    </a>
                                </td>
                                <td>
                                    <a class='w-3 abre_confirma' href='javascript:;' data-href='" . SRC . "maquinas_equipamentos_de_trabalho&deletar={$result[$i]["id_mq_eq_tr"]}'>
                                        <img src='" . IMG . "delete.png'>
                                    </a>
                                </td>
                          </tr>";
            }
        }else{
            $lista = "<tr><td colspan='7'><br><br>Este beneficiário ainda não possui equipamentos.<br><br><br></td></td>";
        }
        
        return $lista;
    }
    
    public function Editar($id) {
        $this->Query = new Select(
            "mq_eq_tr",
            array(
                "id_mq_eq_tr", "id_beneficiario", "id_medida", "id_potencia", "especificacao_mq_eq_tr", "medida", "potencia", "capacidade_mq_eq_tr", "qtd_mq_eq_tr", "potencia_mq_eq_tr"
            )
        );
        $this->Query->setTables(array("medida", "potencia"));
        $this->Query->setWhere(array("id_mq_eq_tr" => $id), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        $this->Dados = $result;
    }
    
    public function Alterar($mq_eq_tr, $id) {
        $this->Query = new Update("mq_eq_tr", $mq_eq_tr);
        $this->Query->setWhere(array("id_mq_eq_tr" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    public function Deletar($id) {
        $this->Query = new Delete("mq_eq_tr", array("id_mq_eq_tr" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
}