<?php
class Benfeitoria {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;

    public function __construct($connect) {
        $this->Connect = $connect;
    }
    
    public function getDados() {
        $this->setSelect("tipo_benfeitoria");
        $this->Query->Reset("medida");
        $this->Query->setIn("id_medida", array(1,9,10), true);
        $GLOBALS['D']->OutputBy($this->Query, $result, $rows);
        $this->Dados["medida"] = '';
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["id_medida"] == $result[$i]["id_medida"]) ? "selected" : '';
            $this->Dados["medida"] .= 
                "<option {$selected} value='{$result[$i]["id_medida"]}'>
                    {$result[$i]["medida"]}
                </option>";
        }
        
        
        return $this->Dados;
    }
    
    private function setSelect($select) {
        if(empty($this->Query) || (!empty($this->Query) && get_class($this->Query) <> "select")){
            $this->Query = new Select();
        }
        $this->Query->Reset($select);
        $GLOBALS['D']->OutputBy($this->Query, $result, $rows);

        $this->Dados[$select] = '';
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["id_{$select}"] == $result[$i]["id_{$select}"]) ? "selected" : '';
            $this->Dados[$select] .= 
                "<option {$selected} value='{$result[$i]["id_{$select}"]}'>
                    {$result[$i][$select]}
                </option>";
        }
    }
    
    
    public function setEmpty() {
        
        $this->Dados["id_beneficiario"] = $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"];
        $this->Dados["tipo_benfeitoria"] = "";
        $this->Dados["obs_benfeitoria"] = "";
        $this->Dados["qtd_benfeitoria"] = "1";
        $this->Dados["valor_benfeitoria"] = "";
        $this->Dados["id_medida"] = "";
        $this->Dados["id_tipo_benfeitoria"] = "";
        
    }
    
    public function Criar($composicao) {
        $composicao["obs_benfeitoria"] = (empty($composicao["obs_benfeitoria"])) ? "Sem observação." : $composicao["obs_benfeitoria"];
        $this->Dados = $composicao;
        $this->Query = new Insert("benfeitoria", $this->Dados);
        $this->Connect->InputBy($this->Query);

    }
    
    public function Ver($beneficiario) {
        
        $this->Query = new Select("benfeitoria",
                array(
                    "id_benfeitoria", "tipo_benfeitoria", "medida", "obs_benfeitoria", "qtd_benfeitoria", "valor_benfeitoria"
                )
        );
        $this->Query->setTables(array("tipo_benfeitoria", "medida"));
        $this->Query->setWhere(array("id_beneficiario" => $beneficiario));
        $this->Query->setOrder("id_benfeitoria DESC", true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $lista = '';
        if($rows > 0){
            for($i=0; $i<$rows; $i++){
                if($i%2 == 1){
                    $zebra = "class='bg-light-blue'";
                }else{
                    $zebra = '';
                }
                
                $result[$i]["valor_benfeitoria"] = number_format($result[$i]["valor_benfeitoria"] , 2, ",", ".");
                $result[$i]["qtd_benfeitoria"]   = "{$result[$i]["qtd_benfeitoria"]} {$result[$i]["medida"]}";
                
                $lista .= "<tr {$zebra}>
                                <td>{$result[$i]["id_benfeitoria"]}</td>
                                <td>{$result[$i]["tipo_benfeitoria"]}</td>
                                <td>{$result[$i]["qtd_benfeitoria"]}</td>
                                <td>{$result[$i]["valor_benfeitoria"]}</td>
                                <td>{$result[$i]["obs_benfeitoria"]}</td>
                                <td>
                                    <a class='w-3' href='" . SRC . "benfeitorias&editar={$result[$i]["id_benfeitoria"]}'>
                                        <img src='" . IMG . "select.png'>
                                    </a>
                                </td>
                                <td>
                                    <a class='w-3 abre_confirma' href='javascript:;' data-href='" . SRC . "benfeitorias&deletar={$result[$i]["id_benfeitoria"]}'>
                                        <img src='" . IMG . "delete.png'>
                                    </a>
                                </td>
                          </tr>";
            }
        }else{
            $lista = "<tr><td colspan='7'><br><br>Este beneficiário ainda não possui benfeitorias.<br><br><br></td></td>";
        }
        
        return $lista;
    }
    
    public function Editar($id) {
        $this->Query = new Select(
            "benfeitoria",
            array(
                "id_benfeitoria", "id_beneficiario", "id_tipo_benfeitoria", "tipo_benfeitoria", "medida", "id_medida", "obs_benfeitoria", "qtd_benfeitoria", "valor_benfeitoria"
            )
        );
        $this->Query->setTables(array("tipo_benfeitoria", "medida"));
        $this->Query->setWhere(array("id_benfeitoria" => $id), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        $result["valor_benfeitoria"] = str_replace(".", ",", $result["valor_benfeitoria"]);
        $this->Dados = $result;
    }
    
    public function Alterar($benfeitoria, $id) {
        $this->Query = new Update("benfeitoria", $benfeitoria);
        $this->Query->setWhere(array("id_benfeitoria" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    public function Deletar($id) {
        $this->Query = new Delete("benfeitoria", array("id_benfeitoria" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
}