<?php
class CultFamiliar {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;

    public function __construct($connect) {
        $this->Connect = $connect;
    }
    
    public function getDados() {
        return $this->Dados;
    }
    
    public function getId($id){
        $this->Connect->Output("SELECT id_beneficiario FROM beneficiario WHERE id_beneficiario = {$id}", $result, $rows, true);
        $this->Dados = $result;
        return $result["id_beneficiario"];
    }
    
    public function setEmpty() {
        $this->Dados["valor_unit_cult_agricola"] = '';
        $this->Dados["area_cult_agricola"] = '';
        $this->Dados["produto_cult_agricola"] = '';
        $this->Dados["producao_cult_agricola"] = '';
        
        $this->Connect->Output("SELECT * FROM atividade", $result, $rows);
        $this->Dados["atividade"] = '';
        for($i=0; $i<$rows; $i++){

            $this->Dados["atividade"] .= 
                "<option value='{$result[$i]["id_atividade"]}'>{$result[$i]["atividade"]}</option>";
        }
        
        $this->Connect->Output("SELECT * FROM medida WHERE id_medida IN(11, 8)", $result, $rows);
        $this->Dados["und_medidas"] = '';
        for($i=0; $i<$rows; $i++){

            $this->Dados["und_medidas"] .= 
                "<option value='{$result[$i]["id_medida"]}'>{$result[$i]["medida"]}</option>";
        }
    }
    
    public function Criar($cultivo) {
        
        $cultivo["area_cult_agricola"] = str_replace(",", ".", $cultivo["area_cult_agricola"]);
        
        $this->Dados = $cultivo;
        $this->Query = new Insert("cult_agricola", $this->Dados);
        $this->Connect->InputBy($this->Query);

    }
    
    public function Ver($beneficiario) {
        
        $this->Query = new Select("cult_agricola");
        $this->Query->setTables(array("atividade", "medida"), "LEFT");
        $this->Query->setWhere(array("id_beneficiario" => $beneficiario));
        $this->Query->setOrder("id_cult_agricola DESC", true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $lista = '';
        if($rows > 0){
            for($i=0; $i<$rows; $i++){
                if($i%2 == 1){
                    $zebra = "class='bg-light-blue'";
                }else{
                    $zebra = '';
                }
                
                $result[$i]["area_cult_agricola"] = number_format($result[$i]["area_cult_agricola"], 2, ",", ".");
                
                $lista .= "<tr {$zebra}>
                                <td>{$result[$i]["id_cult_agricola"]}</td>
                                <td>{$result[$i]["atividade"]}</td>
                                <td>{$result[$i]["produto_cult_agricola"]}</td>
                                <td>{$result[$i]["producao_cult_agricola"]} {$result[$i]["medida"]}</td>
                                <td>{$result[$i]["area_cult_agricola"]}</td>
                                <td>{$result[$i]["valor_unit_cult_agricola"]}</td>
                                <td>
                                    <a class='w-3' href='#SRC#cultivo_agricola&editar={$result[$i]["id_cult_agricola"]}'>
                                        <img src='" . IMG . "select.png'>
                                    </a>
                                </td>
                                <td>
                                    <a class='w-3 abre_confirma' href='javascript:;' data-href='#SRC#cultivo_agricola&deletar={$result[$i]["id_cult_agricola"]}'>
                                        <img src='" . IMG . "delete.png'>
                                    </a>
                                </td>
                          </tr>";
            }
        }else{
            $lista = "<tr><td colspan='9'><br><br>Este beneficiário ainda não possui cultivos.<br><br><br></td></td>";
        }
        
        return $lista;
    }
    
    public function Editar($id) {
        
        $this->Query = new Select("cult_agricola");
        $this->Query->setTables(array("atividade", "medida"), "LEFT");
        $this->Query->setWhere(array("id_cult_agricola" => $id), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        $this->Dados = $result;
        
        $this->Dados["area_cult_agricola"] = str_replace(".", ",", $this->Dados["area_cult_agricola"]);
        
        $this->Connect->Output("SELECT * FROM atividade", $result, $rows);
        $this->Dados["atividade"] = '';
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["id_atividade"] == $result[$i]["id_atividade"]) ? "selected" : '';
            $this->Dados["atividade"] .= 
                "<option {$selected} value='{$result[$i]["id_atividade"]}'>{$result[$i]["atividade"]}</option>";
        }
        $this->Connect->Output("SELECT * FROM medida WHERE id_medida IN(11, 8)", $result, $rows);
        $this->Dados["und_medidas"] = '';
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["id_medida"] == $result[$i]["id_medida"]) ? "selected" : '';
            $this->Dados["und_medidas"] .= 
                "<option {$selected} value='{$result[$i]["id_medida"]}'>{$result[$i]["medida"]}</option>";
        }

    }
    
    public function Alterar($cult_agricola, $id) {
        $cult_agricola["area_cult_agricola"] = str_replace(",", ".", $cult_agricola["area_cult_agricola"]);
        $this->Query = new Update("cult_agricola", $cult_agricola);
        $this->Query->setWhere(array("id_cult_agricola" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    public function Deletar($id) {
        $this->Query = new Delete("cult_agricola", array("id_cult_agricola" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
}