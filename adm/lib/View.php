<?php
class View {
    private $Name;
    private $Url;
    private $Path;
    private $Keys;
    private $Values;
    private $View;
    private static $_404;
    
    public function __construct($name = NULL, $path = '', $view = true) {
        self::$_404 = file_get_contents("tpl/404-no-found.html");
        if(func_num_args()>0){
            $this->Name = $name;
            $this->Path = $path;
            if($view){
                $this->Url  = "tpl/" . $this->Path . $this->Name . ".html";
                $this->View = (!empty($this->Url) && file_exists($this->Url)) ? file_get_contents($this->Url) : self::$_404;
            }else{
                $this->Url = "inc/" . $this->Path . $this->Name . ".php";
                $this->Url = (file_exists($this->Url)) ? $this->Url : "inc/content/home.php";
            }
        }
    }
    
    public function Reset($name, $view = true) {
        $this->Name = $name;
        if($view){
            $this->Url  = "tpl/" . $this->Path . $this->Name . ".html";
            $this->View = (!empty($this->Url) && file_exists($this->Url)) ? file_get_contents($this->Url) : self::$_404;
        }else{
            $this->Url = "inc/" . $this->Path . $this->Name . ".php";
            $this->Url = (file_exists($this->Url)) ? $this->Url : "inc/content/home.php";
        }
    }
  
    public function setView($name, array $data = NULL, $s = '#'){
        $this->Reset($name);
        if(!empty($data) && $this->View <> self::$_404){
            $this->Keys   = explode('&', $s . implode($s . '&' . $s, array_keys($data)) . $s);
            $this->Values = array_values($data);
            $this->View = str_replace($this->Keys, $this->Values, $this->View);
        }
    }
    
    public function prepareView($name, array $data = NULL, $s = '#') {
        $this->Reset($name);
        if(!empty($data) && $this->View <> self::$_404){
            $this->Keys   = explode('&', $s . implode($s . '&' . $s, array_keys($data)) . $s);
            $this->Values = array_values($data);
            $this->View = str_replace($this->Keys, $this->Values, $this->View);
        }
        return $this->View;
    }
    
    public function ShowView($name, array $data = NULL, $resetPath = false, $s = '#'){
        if($resetPath){
            $this->setPath();
        }
        $this->setView($name, $data, $s);
        echo $this->View;
    }
    
    public function Request(){
        $size = func_num_args();
        if($size > 1):
            for($x=0; $x<$size; $x++):
                $this->Reset(func_get_arg($x), false);
                include($this->Url);
            endfor;
        else:
            $this->Reset(func_get_arg(0), false);
            include($this->Url);
        endif;
    }
    
    public function Import($path){
        $this->setPath($path);
        $size = func_num_args();
        if($size > 2):
            for($x=1; $x<$size; $x++):
                $this->Reset(func_get_arg($x), false);
                include($this->Url);
            endfor;
        else:
            $this->Reset(func_get_arg(1), false);
            include($this->Url);
        endif;
    }
    
    public function OpenTag($tag, array $attr = NULL, &$close){
        $OpenTag = "<{$tag}";
        do{
            $OpenTag .= " " . key($attr) . "='" . current($attr) . "'";
        }while(next($attr));
        $OpenTag .= ">";
        
        $close = "</{$tag}>";
        
        echo $OpenTag;
    }
  
    public function define404($name, array $data, $path = '') {
        $this->setPath($path);
        self::$_404 = $this->prepareView($name, $data);
    }
    
    public function set404($name, $path = '') {
        $this->setPath($path);
        $this->Reset($name);
        self::$_404 = $this->getView();
    }
    
    public function setUrl($url) {
        $this->Url = (file_exists($url)) ? $url : self::$_404;
    }
    
    public function setPath($path = '') {
        $this->Path = (!empty($path)) ? $path . '/' : $path;
    }
    
    public function concatPath($path = '') {
        $this->Path .= $path . '/';
    }
    
    public function is404() {
        if($this->Url == "tpl/404-no-found.html"){
            return true;
        }else{
            return false;
        }
    }
    
    function getUrl() {
        return $this->Url;
    }

}
