<?php
class RendaFDA {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;

    public function __construct($connect) {
        $this->Connect = $connect;
    }
    
    public function getDados() {
        return $this->Dados;
    }
    
    public function getId($id){
        $this->Connect->Output("SELECT id_beneficiario FROM beneficiario WHERE id_beneficiario = {$id}", $result, $rows, true);
        $this->Dados = $result;
        return $result["id_beneficiario"];
    }
    
    public function setEmpty() {
        $this->Dados["Mãe"] = '';
        $this->Dados["Pai"] = '';
        $this->Dados["Filho(a)"] = '';
        $this->Dados["Tio(a)"] = '';
        $this->Dados["Primo(a)"] = '';
        $this->Dados["Sobrinho(a)"] = '';
        $this->Dados["Avô"] = '';
        $this->Dados["Avó"] = '';
        $this->Dados["Neto(a)"] = '';
        $this->Dados["Cunhado(a)"] = '';
        $this->Dados["Genro"] = '';
        $this->Dados["Nora"] = '';
        $this->Dados["Outros"] = '';
        $this->Dados["nome_renda_fda"] = '';
        $this->Dados["atividade_renda_fda"] = '';
        $this->Dados["receita_renda_fda"] = '';
        $this->Dados["custo_renda_fda"] = '';
        $this->Dados["1"] = '';
        $this->Dados["0"] = '';
    }
    
    public function Criar($composicao) {
        
        $this->Dados = $composicao;
        $this->Dados["receita_renda_fda"] = str_replace(",", ".", $this->Dados["receita_renda_fda"]);
        $this->Dados["custo_renda_fda"] = str_replace(",", ".", $this->Dados["custo_renda_fda"]);
        $this->Query = new Insert("renda_fda", $this->Dados);
        $this->Connect->InputBy($this->Query);

    }
    
    public function Ver($beneficiario) {
        
        $this->Query = new Select("renda_fda");
        $this->Query->setWhere(array("id_beneficiario" => $beneficiario));
        $this->Query->setOrder("id_renda_fda DESC", true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $lista = '';
        if($rows > 0){
            for($i=0; $i<$rows; $i++){
                if($i%2 == 1){
                    $zebra = "class='bg-light-blue'";
                }else{
                    $zebra = '';
                }
                
                $lista .= "<tr {$zebra}>
                                <td>{$result[$i]["id_renda_fda"]}</td>
                                <td>{$result[$i]["nome_renda_fda"]}</td>
                                <td>{$result[$i]["parentesco_renda_fda"]}</td>
                                <td>{$result[$i]["atividade_renda_fda"]}</td>
                                <td>" . number_format($result[$i]["receita_renda_fda"], 2, ",", ".") . "</td>
                                <td>" . number_format($result[$i]["custo_renda_fda"], 2, ",", ".") . "</td>
                                <td>
                                    <a class='w-3' href='#SRC#fora_da_agricultura&editar={$result[$i]["id_renda_fda"]}'>
                                        <img src='" . IMG . "select.png'>
                                    </a>
                                </td>
                                <td>
                                    <a class='w-3 abre_confirma' href='javascript:;' data-href='#SRC#fora_da_agricultura&deletar={$result[$i]["id_renda_fda"]}'>
                                        <img src='" . IMG . "delete.png'>
                                    </a>
                                </td>
                          </tr>";
            }
        }else{
            $lista = "<tr><td colspan='9'><br><br>Este beneficiário não possui rendas fora da agricultura.<br><br><br></td></td>";
        }
        
        return $lista;
    }
    
    public function Editar($id) {
        $this->Query = new Select("renda_fda");
        $this->Query->setWhere(array("id_renda_fda" => $id), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        $this->Dados = $result;
        $this->Dados["receita_renda_fda"] = str_replace(".", ",", $this->Dados["receita_renda_fda"]);
        $this->Dados["custo_renda_fda"] = str_replace(".", ",", $this->Dados["custo_renda_fda"]);
        $this->Dados["Mãe"] = '';
        $this->Dados["Pai"] = '';
        $this->Dados["Filho(a)"] = '';
        $this->Dados["Tio(a)"] = '';
        $this->Dados["Primo(a)"] = '';
        $this->Dados["Sobrinho(a)"] = '';
        $this->Dados["Avô"] = '';
        $this->Dados["Avó"] = '';
        $this->Dados["Neto(a)"] = '';
        $this->Dados["Cunhado(a)"] = '';
        $this->Dados["Genro"] = '';
        $this->Dados["Nora"] = '';
        $this->Dados["Outros"] = '';
        $this->Dados[$this->Dados["parentesco_renda_fda"]] = "selected";
    }
    
    public function Alterar($renda_fda, $id) {
        $renda_fda["receita_renda_fda"] = str_replace(",", ".", $renda_fda["receita_renda_fda"]);
        $renda_fda["custo_renda_fda"] = str_replace(",", ".", $renda_fda["custo_renda_fda"]);
        $this->Query = new Update("renda_fda", $renda_fda);
        $this->Query->setWhere(array("id_renda_fda" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    public function Deletar($id) {
        $this->Query = new Delete("renda_fda", array("id_renda_fda" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
}