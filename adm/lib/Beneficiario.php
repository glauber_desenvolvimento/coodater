<?php
class Beneficiario {
    /** @var DAO */
    private $Connect;
    private $Query;
    /** @var View */
    private $View;
    private $Dados;
    const Limit = 1500;
    const ByPage = 72;


    public function __construct($connect, $view) {
        $this->Connect = $connect;
        $this->View = $view;
    }
    
    public function getDados() {
        if(empty($this->Dados["id_beneficiario"])){
            $this->Dados["id_beneficiario"] = 0;
        }
        $this->Connect->Output("SELECT * FROM bxf WHERE id_beneficiario = {$this->Dados["id_beneficiario"]}", $result, $rows);
        if($rows > 0){
            foreach ($result as $array) {
                $testa_check[] = $array["id_fnc_exe_parc"];
            }
        }else{
            $testa_check = array();
        }
        
        $this->Dados["funcoes"] = '';
        $this->Connect->Output("SELECT * FROM fnc_exe_parc", $result, $rows);
        for ($i = 0; $i < $rows; $i++) {
            $checked = (in_array($result[$i]["id_fnc_exe_parc"], $testa_check)) ? "checked" : "";
            if($i == 5){
                $this->Dados["funcoes"] .= "<br>";
            }
            $this->Dados["funcoes"] .=
            "<label class='flex w-9 tx-7'>
                <input {$checked} class='flex' type='checkbox' value='{$result[$i]["id_fnc_exe_parc"]}' name='bxf[id_fnc_exe_parc][]'>
                {$result[$i]["fnc_exe_parc"]}
            </label>";
        }
        
        return $this->Dados;
    }

    public function setDados(array $Dados) {
        $this->Dados = $Dados;
    }
    public function setEmpty(){
        $this->Connect->Output("SELECT MAX(id_beneficiario) as id_beneficiario FROM beneficiario", $max, $row, true);
        $this->Query = new Select("beneficiario");
        $this->Query->setWhere(array("id_beneficiario" => $max["id_beneficiario"]), true);
        $this->Connect->OutputBy( $this->Query, $result, $rows, true);
        $this->Dados = $result;
        $this->Dados["mensagem"] = '';
        $this->Dados["situacao_beneficiario1"] = '';
        $this->Dados["situacao_beneficiario0"] = '';
        $this->Dados["sexo_beneficiario1"] = '';
        $this->Dados["sexo_beneficiario0"] = '';
        $this->Dados["presente_beneficiario1"] = '';
        $this->Dados["presente_beneficiario0"] = '';
        $this->Dados["perid_ausencia_beneficiario1"] = '';
        $this->Dados["perid_ausencia_beneficiario0"] = '';
        $this->Dados["insc_rel_beneficiario1"] = '';
        $this->Dados["insc_rel_beneficiario0"] = '';
        $this->Dados["troca_permuta"] = '';
        $this->Dados["anos"] = '0';
        $this->Dados["meses"] = '';
    }
    public function getId($id){
        $this->Connect->Output("SELECT id_beneficiario FROM beneficiario WHERE id_beneficiario = {$id}", $result, $rows, true);
        $this->Dados = $result;
        return $result["id_beneficiario"];
    }
    
    public function setSelect($select) {
        if(empty($this->Query) || (!empty($this->Query) && get_class($this->Query) <> "Select")){
            $this->Query = new Select();
        }
        $this->Query->Reset($select);
        $GLOBALS['D']->OutputBy($this->Query, $result, $rows);

        $this->Dados[$select] = '';
        for($i=0; $i<$rows; $i++){
            $selected = (!empty($this->Dados["id_{$select}"]) && $this->Dados["id_{$select}"] == $result[$i]["id_{$select}"]) ? "selected" : '';
            $this->Dados[$select] .= 
                "<option {$selected} value='{$result[$i]["id_{$select}"]}'>
                    {$result[$i][$select]}
                </option>";
        }
    }
    public function setOption($option, $required = "required", $table = "beneficiario") {
        if(empty($this->Query) || (!empty($this->Query) && get_class($this->Query) <> "Select")){
            $this->Query = new Select();
        }
        $this->Query->Reset($option);
        $GLOBALS['D']->OutputBy($this->Query, $result, $rows);
        $this->Dados[$option] = '';
        for($i=0; $i<$rows; $i++){
            $checked = (!empty($this->Dados["id_{$option}"]) && $this->Dados["id_{$option}"] == $result[$i]["id_{$option}"]) ? "checked" : '';
            $this->Dados[$option] .= 
                "<label class='w-9 tx-7'>
                    <input {$checked} class='flex' type='radio' {$required} name='{$table}[id_{$option}]' value='{$result[$i]["id_{$option}"]}'>
                    &nbsp;{$result[$i][$option]}
                </label>";
        }
    }
    public function __set($name, $value) {
        $this->Dados[$name] = $value;
    }
    public function Alterar($beneficiario, $funcoes = NULL) {
        
        $anos = $beneficiario["anos"];
        $meses = $beneficiario["meses"];
        unset($beneficiario["anos"]);
        unset($beneficiario["meses"]);
        $nascimento = $beneficiario["dt_nascimento_beneficiario"];
        $beneficiario["dt_nascimento_beneficiario"] = 
            str_replace('/', '-', $beneficiario["dt_nascimento_beneficiario"]);
        $beneficiario["dt_nascimento_beneficiario"] = 
            date(
                'Y-m-d', strtotime($beneficiario["dt_nascimento_beneficiario"])
            );
        
        $this->Dados = $beneficiario;
        $this->Query = new Update("beneficiario", $this->Dados);
        $this->Query->setWhere(array("id_beneficiario" => $this->Dados["id_beneficiario"]), true);
        $this->Connect->ExecuteBy($this->Query);
        $this->setExtraDados($anos, $meses, $nascimento);
        
        if(!empty($funcoes)){
            $r_atual = sizeof($funcoes["id_fnc_exe_parc"]);
        }else{
            $r_atual = 0;
        }
        
        $this->Connect->Output("SELECT * FROM bxf WHERE id_beneficiario = {$this->Dados["id_beneficiario"]}", $result, $r_anterior);

        
        if($r_atual == 0 && $r_anterior > 0){//RETIROU TODOS OS ITENS
            
            $this->Connect->Execute("DELETE FROM bxf WHERE id_beneficiario = {$this->Dados["id_beneficiario"]}");
            
        }elseif($r_anterior == $r_atual){//SÓ ATUALIZOU OS ITENS
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $this->Connect->Execute(
                    "UPDATE bxf SET 
                    id_fnc_exe_parc = {$funcoes["id_fnc_exe_parc"][$i]}
                    WHERE id_bxf = {$result[$i]["id_bxf"]}"
                );
                    
            }

        }elseif($r_anterior > $r_atual){//FOI RETIRADO UM ITEM
            
            for ($i = 0; $i < $r_atual; $i++) {

                $this->Connect->Execute(
                    "UPDATE bxf SET 
                    id_fnc_exe_parc = {$funcoes["id_fnc_exe_parc"][$i]}
                     WHERE id_bxf = {$result[$i]["id_bxf"]}"
                );
                    
            }
            
            for ($i = $r_atual; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM bxf WHERE id_bxf = {$result[$i]["id_bxf"]}");
            }
            
        }elseif($r_anterior < $r_atual){//FOI ADICIONADO UM ITEM
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $this->Connect->Execute(
                    "UPDATE bxf SET 
                    id_fnc_exe_parc = {$funcoes["id_fnc_exe_parc"][$i]}
                     WHERE id_bxf = {$result[$i]["id_bxf"]}"
                );
                    
            }
            
            for ($i = $r_anterior; $i < $r_atual; $i++) {
                $this->Connect->Input("INSERT INTO bxf (id_beneficiario, id_fnc_exe_parc) VALUE ({$this->Dados["id_beneficiario"]}, {$funcoes["id_fnc_exe_parc"][$i]})");

            }
            
        }

    }
    public function Criar($beneficiario, $funcoes = NULL) {
        
        $anos = $beneficiario["anos"];
        $meses = $beneficiario["meses"];
        unset($beneficiario["anos"]);
        unset($beneficiario["meses"]);
        $nascimento = $beneficiario["dt_nascimento_beneficiario"];
        $beneficiario["dt_nascimento_beneficiario"] = 
            str_replace('/', '-', $beneficiario["dt_nascimento_beneficiario"]);
        $beneficiario["dt_nascimento_beneficiario"] = 
            date(
                'Y-m-d', strtotime($beneficiario["dt_nascimento_beneficiario"])
            );
        
        $this->Dados = $beneficiario;
        $this->Query = new Update("beneficiario", $this->Dados);
        $this->Query->setWhere(array("id_beneficiario" => $this->Dados["id_beneficiario"]), true);
        $this->Connect->ExecuteBy($this->Query);
        $this->Connect->Input("INSERT INTO beneficiario (nome_beneficiario) VALUE (NULL)");
        $this->setExtraDados($anos, $meses, $nascimento);
        
        if(!empty($funcoes)){
            $this->Query = new Insert();
            foreach ($funcoes as $key => $array) {
                foreach ($array as $value) {
                    $bxf["id_beneficiario"] = $this->Dados["id_beneficiario"];
                    $bxf[$key] = $value;
                    $this->Query->Reset("bxf", $bxf);
                    $this->Connect->InputBy($this->Query);
                }
            }
        }
        
    }

    private function setExtraDados($anos, $meses, $nascimento) {
        $this->Dados["troca_permuta"] = (!empty($this->Dados["vlr_troca_perm_beneficiario"])) ? "checked" : '';
        $this->Dados["situacao_beneficiario1"] = '';
        $this->Dados["situacao_beneficiario0"] = '';
        $this->Dados["sexo_beneficiario1"] = '';
        $this->Dados["sexo_beneficiario0"] = '';
        $this->Dados["presente_beneficiario1"] = '';
        $this->Dados["presente_beneficiario0"] = '';
        $this->Dados["perid_ausencia_beneficiarioMeses"] = '';
        $this->Dados["perid_ausencia_beneficiarioDias"] = '';
        $this->Dados["insc_rel_beneficiario1"] = '';
        $this->Dados["insc_rel_beneficiario0"] = '';
        $this->Dados["anos"] = $anos;
        $this->Dados["meses"] = $meses;
        $this->Dados["dt_nascimento_beneficiario"] = $nascimento;
        $this->Dados["situacao_beneficiario{$this->Dados["situacao_beneficiario"]}"] = "checked";
        $this->Dados["sexo_beneficiario{$this->Dados["sexo_beneficiario"]}"] = "checked";
        $this->Dados["presente_beneficiario{$this->Dados["presente_beneficiario"]}"] = "checked";
        $this->Dados["perid_ausencia_beneficiario{$this->Dados["perid_ausencia_beneficiario"]}"] = "checked";
        $this->Dados["insc_rel_beneficiario{$this->Dados["insc_rel_beneficiario"]}"] = "checked";
    }
    
    public function setMensagem($mensagem = NULL, $caminho = NULL) {
        if(func_num_args() == 2){
            $this->Dados["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     $mensagem
                 </div>";
            $this->Dados["mensagem"] .= 
            "<script>
                 $(function(){
                     window.setTimeout(
                         function(){
                             window.open('" . $caminho . "', '_self');
                         }, 1000
                     );
                 });
             </script>";
        }else{
            $this->Dados["mensagem"] = '';
        }
    }
    
    private function navCreate($rows, $bypage, array $result, $key){
        if($rows > 0){
            if(isset($_SESSION['nav'])) unset($_SESSION['nav']);
            $_SESSION['nav'] = new Navigator($rows, $bypage);
            $_SESSION['nav']->setIds($result, $key);
            $this->setPaginator();
        }else{
            $this->Dados["paginator"] = '';
        }
    }
    
    private function setPaginator() {
        $this->Dados["paginator"] = 
            $_SESSION['nav']->Paginator(
                SRC."beneficiario/selecionar", 
                "a-box-2 w-3 al-center bx-radius"
            );
    }
    
    private function MAX() {
        $this->Connect->Output("SELECT MAX(id_beneficiario) AS max FROM beneficiario", $max, $row, true);
        return $max["max"];
    }
    
    public function Ver($order, $desc = false, $index = NULL, $pesquisa = NULL, $criterio = NULL) {
        $max = $this->MAX();

        if(!empty($pesquisa) && !empty($criterio) && $criterio == "comercializacao"){

            $this->Query = new Select(
                    "beneficiario", 
                    array(
                        "id_beneficiario",
                        "nome_beneficiario",
                        "n_da_dap_beneficiario",
                        "rb_beneficiario",
                        "cpf_beneficiario",
                        "atividade",
                        "tipo_semovente"
                    ),
                    self::Limit
                );
            $this->Query->setTables(array("comercializacao#id_beneficiario", "atividade", "tipo_semovente"), "LEFT");
            $clause = " WHERE (atividade LIKE '%{$pesquisa}%' || tipo_semovente LIKE '%{$pesquisa}%') AND id_beneficiario <> {$max}";
            
            $p = true;
            
        }else if(!empty($pesquisa) && !empty($criterio) && $criterio == "fnc_exe_parc"){
            
            $this->Query = new Select(
                    "bxf", 
                    array(
                        "id_beneficiario",
                        "nome_beneficiario",
                        "n_da_dap_beneficiario",
                        "rb_beneficiario",
                        "cpf_beneficiario"
                    ),
                    self::Limit
                );
            $this->Query->setTables(array("beneficiario", "fnc_exe_parc"), "LEFT");
            $this->Query->setGroup("id_beneficiario");
            $clause = " WHERE {$criterio} LIKE '%{$pesquisa}%' AND id_beneficiario <> {$max}";
            
            $p = true;
            
        }else if(!empty($pesquisa) && !empty($criterio) && $criterio == "nucleo"){
            
            $this->Query = new Select(
                    "beneficiario", 
                    array(
                        "id_beneficiario",
                        "nome_beneficiario",
                        "n_da_dap_beneficiario",
                        "rb_beneficiario",
                        "cpf_beneficiario"
                    ),
                    self::Limit
                );
            $this->Query->setTables("nucleo", "LEFT");
            $clause = " WHERE {$criterio} LIKE '%{$pesquisa}%' AND id_beneficiario <> {$max}";
            
            $p = true;
            
        }else if(!empty($pesquisa) && !empty($criterio)){
            
            $this->Query = new Select(
                    "beneficiario", 
                    array(
                        "id_beneficiario",
                        "nome_beneficiario",
                        "n_da_dap_beneficiario",
                        "rb_beneficiario",
                        "cpf_beneficiario"
                    ),
                    self::Limit
                );
            $clause = " WHERE {$criterio} LIKE '%{$pesquisa}%' AND id_beneficiario <> {$max}";
            
            $p = true;
            
        }else{
            $this->Query = new Select(
                    "beneficiario", 
                    array(
                        "id_beneficiario",
                        "nome_beneficiario",
                        "n_da_dap_beneficiario",
                        "rb_beneficiario",
                        "cpf_beneficiario"
                    ),
                    self::Limit
                );
            $clause = " WHERE id_beneficiario <> {$max}";
            
            $p = false;
            
        }

        if(is_numeric($index)){
            $ids = implode(", ", $_SESSION["nav"]->getIds((int) $index));
            $clause .= " AND id_beneficiario IN(" . $ids . ")";
        }
        $this->Query->setClause($clause);
        $this->Query->setOrder($order . (($desc) ? " DESC" : ''));
        $this->Query->End();
        $this->Connect->OutputBy($this->Query, $tabelas, $r_tabelas);
        
        if($p){
            $criterio = str_replace(
                    array("nome_beneficiario", "cpf_beneficiario", "rb_beneficiario", "fnc_exe_parc", "comercializacao"), 
                    array("Nome", "CPF", "RB", "Função que exerce na parcela", "Comercialização"), $criterio);
            $this->Dados["resultados"] = "<span class='m-bottom-s'>Foram encontrados {$r_tabelas} resultado(s) por {$criterio}: {$pesquisa}</span>";
        }else{
            $this->Dados["resultados"] = '';
        }
        
        if(is_numeric($index)){
            $this->setPaginator();
        }else{
            $this->navCreate($r_tabelas, self::ByPage, $tabelas, "id_beneficiario");
        }
        $this->setLinhas($tabelas, self::ByPage, $p);
    }
    
    private function setLinhas(array $Dados, $rows, $pesquisa = false) {
        $this->Dados["linhas"] = '';
        if(!empty($Dados)){
            for($i=0; $i<$rows; $i++){
                if(!array_key_exists($i, $Dados)){break;}
                $Dados[$i]["class"] = ($i%2 <> 0) ? "class='bg-light-blue'" : '';
                $this->Dados["linhas"] .= $this->View->prepareView("beneficiario/linha", $Dados[$i]);
            }
        }else{
            if($pesquisa){
                
                $this->Dados["linhas"] = "<tr><td colspan='7'>Não foi encontrado nenhum resultado.</td></td>";
            }else{
                $this->Dados["linhas"] = "<tr><td colspan='7'>Não foi cadastrado nenhum beneficiario.</td></td>";
            }
        }
    }
    
    public function Selecionar($id) {
        $this->Query = new Select("beneficiario");
        $this->Query->setWhere(array("id_beneficiario" => $id), true);
        $this->Connect->OutputBy( $this->Query, $result, $rows, true);
        $result["dt_nascimento_beneficiario"] = 
            date(
                'd/m/Y', strtotime($result["dt_nascimento_beneficiario"])
            );
        $this->Dados = $result;
        $datas = explode(' ' ,$result["temp_res_beneficiario"]);
        if(sizeof($datas) > 2){
            $anos = $datas[0];
            $meses = $datas[3];
        }else{
            if($datas[1] == "meses" || "mês"){
                $anos = 0;
                $meses = $datas[0];
            }else{
                $anos = $datas[0];
                $meses = 0;
            }
        }
        
        $this->setExtraDados($anos, $meses, $result["dt_nascimento_beneficiario"]);
        return $this->Dados;
    }
    public function Deletar($id) {
        
        if($_SESSION[SITE_NAME]["funcao_usuario"]){
            $this->Connect->Output("SELECT arquivo_1, arquivo_2, arquivo_3 FROM relatorios WHERE id_beneficiario = {$id}", $result, $rows, true);

            for ($x = 1; $x <= 3; $x++) {
                if(!empty($result["arquivo_" . $x])){
                    $new_arquivo = '';
                    $arquivo = explode("/", $result["arquivo_" . $x]);
                    $size_arquivo = sizeof($arquivo);
                    $limit = $size_arquivo - 3;
                    for ($i = 0; $i < $size_arquivo; $i++) {
                        if($i >= $limit){
                            $new_arquivo[] = $arquivo[$i];
                        }
                    }
                    $new_arquivo = implode("/", $new_arquivo);
                    unlink($new_arquivo);
                }
            }

            $this->Query = new Delete("beneficiario", array("id_beneficiario" => $id), true);
            $this->Connect->ExecuteBy($this->Query);
            $_SESSION[SITE_NAME]["beneficiario"] = NULL;
            
            return SRC . "&selecionar";
        }else{
            return SRC . "bloqueio_delete";
        }

    }
}