<?php
class RendaFDP {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;

    public function __construct($connect) {
        $this->Connect = $connect;
    }
    
    public function getDados() {
        return $this->Dados;
    }
    
    public function getId($id){
        $this->Connect->Output("SELECT id_beneficiario FROM beneficiario WHERE id_beneficiario = {$id}", $result, $rows, true);
        $this->Dados = $result;
        return $result["id_beneficiario"];
    }
    
    public function setEmpty() {
        
        $this->Connect->Output("SELECT * FROM freq_renda_fdp", $result, $rows);
        $this->Dados["freq_renda_fdp"] = '';
        for($i=0; $i<$rows; $i++){

            $this->Dados["freq_renda_fdp"] .= 
                "<option value='{$result[$i]["id_freq_renda_fdp"]}'>{$result[$i]["freq_renda_fdp"]}</option>";
        }
        
        $this->Dados["Mãe"] = '';
        $this->Dados["Pai"] = '';
        $this->Dados["Filho(a)"] = '';
        $this->Dados["Tio(a)"] = '';
        $this->Dados["Primo(a)"] = '';
        $this->Dados["Sobrinho(a)"] = '';
        $this->Dados["Avô"] = '';
        $this->Dados["Avó"] = '';
        $this->Dados["Neto(a)"] = '';
        $this->Dados["Cunhado(a)"] = '';
        $this->Dados["Genro"] = '';
        $this->Dados["Nora"] = '';
        $this->Dados["Outros"] = '';
        $this->Dados["nome_renda_fdp"] = '';
        $this->Dados["atividade_renda_fdp"] = '';
        $this->Dados["valor_renda_fdp"] = '';
        $this->Dados["frequencia_renda_fdp"] = '';
        $this->Dados["1"] = '';
        $this->Dados["0"] = '';
    }
    
    public function Criar($composicao) {
        
        $this->Dados = $composicao;
        $this->Dados["valor_renda_fdp"] = str_replace(",", ".", $this->Dados["valor_renda_fdp"]);
        $this->Query = new Insert("renda_fdp", $this->Dados);
        $this->Connect->InputBy($this->Query);

    }
    
    public function Ver($beneficiario) {
        
        $this->Query = new Select("renda_fdp");
        $this->Query->setTables("freq_renda_fdp");
        $this->Query->setWhere(array("id_beneficiario" => $beneficiario));
        $this->Query->setOrder("id_renda_fdp DESC", true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $lista = '';
        if($rows > 0){
            for($i=0; $i<$rows; $i++){
                if($i%2 == 1){
                    $zebra = "class='bg-light-blue'";
                }else{
                    $zebra = '';
                }
                
                $lista .= "<tr {$zebra}>
                                <td>{$result[$i]["id_renda_fdp"]}</td>
                                <td>{$result[$i]["nome_renda_fdp"]}</td>
                                <td>{$result[$i]["parentesco_renda_fdp"]}</td>
                                <td>{$result[$i]["atividade_renda_fdp"]}</td>
                                <td>" . number_format($result[$i]["valor_renda_fdp"], 2, ",", ".") . "</td>
                                <td>{$result[$i]["frequencia_renda_fdp"]}x {$result[$i]["freq_renda_fdp"]}</td>
                                <td>
                                    <a class='w-3' href='#SRC#fora_da_propriedade&editar={$result[$i]["id_renda_fdp"]}'>
                                        <img src='" . IMG . "select.png'>
                                    </a>
                                </td>
                                <td>
                                    <a class='w-3 abre_confirma' href='javascript:;' data-href='#SRC#fora_da_propriedade&deletar={$result[$i]["id_renda_fdp"]}'>
                                        <img src='" . IMG . "delete.png'>
                                    </a>
                                </td>
                          </tr>";
            }
        }else{
            $lista = "<tr><td colspan='9'><br><br>Este beneficiário não possui rendas fora da propriedade.<br><br><br></td></td>";
        }
        
        return $lista;
    }
    
    public function Editar($id) {
        $this->Query = new Select("renda_fdp");
        $this->Query->setWhere(array("id_renda_fdp" => $id), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        $this->Dados = $result;
        
        $this->Connect->Output("SELECT * FROM freq_renda_fdp", $result, $rows);
        $this->Dados["freq_renda_fdp"] = '';
        for($i=0; $i<$rows; $i++){

            $selected = ($this->Dados["id_freq_renda_fdp"] == $result[$i]["id_freq_renda_fdp"]) ? 'selected' : '';
            $this->Dados["freq_renda_fdp"] .= 
                "<option {$selected} value='{$result[$i]["id_freq_renda_fdp"]}'>{$result[$i]["freq_renda_fdp"]}</option>";
        }
        
        $this->Dados["valor_renda_fdp"] = str_replace(".", ",", $this->Dados["valor_renda_fdp"]);
        $this->Dados["Mãe"] = '';
        $this->Dados["Pai"] = '';
        $this->Dados["Filho(a)"] = '';
        $this->Dados["Tio(a)"] = '';
        $this->Dados["Primo(a)"] = '';
        $this->Dados["Sobrinho(a)"] = '';
        $this->Dados["Avô"] = '';
        $this->Dados["Avó"] = '';
        $this->Dados["Neto(a)"] = '';
        $this->Dados["Cunhado(a)"] = '';
        $this->Dados["Genro"] = '';
        $this->Dados["Nora"] = '';
        $this->Dados["Outros"] = '';
        $this->Dados[$this->Dados["parentesco_renda_fdp"]] = "selected";
    }
    
    public function Alterar($renda_fdp, $id) {
        $renda_fdp["valor_renda_fdp"] = str_replace(",", ".", $renda_fdp["valor_renda_fdp"]);
        $this->Query = new Update("renda_fdp", $renda_fdp);
        $this->Query->setWhere(array("id_renda_fdp" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    public function Deletar($id) {
        $this->Query = new Delete("renda_fdp", array("id_renda_fdp" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
}