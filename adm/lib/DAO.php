<?php
/**
 * <b>Classe DAO</b> - Data Access Object
 *    Classe que gerencia objetos de acesso aos dados
 *    
 * @copyright 2014 Glauber Gonçalves dos Santos
 */


class DAO {
    private static $Host = "localhost"; 
    private static $User = "intranet_coodate"; 
    private static $Pass = '=Wv(&#b;5AD8'; 
    private static $Db   = "intranet_coodater"; 
    /** @var PDO Connect = Objeto de conexão*/
    private static $Connect;
    private $Result;
    private $Rows;
    /** @var PDOStatement = Objeto de Instrução SQL*/
    private $Query;
    /** 
    * @param String $host = Endereço de Host
    * @param String $db = Nome do Banco
    * @param String $user = Nome do Usuário
    * @param String $pass = Senha
    * @param String $dsn = Qual a plataforma
    */
    public function __construct($host = NULL, $db = NULL, $user = NULL, $pass = NULL, $dsn = NULL){
        if(func_num_args() > 3):
            self::$Host = $host;
            self::$Db   = $db;
            self::$User = $user;
            self::$Pass = $pass;
        endif;
        self::$Connect  = NULL;
        self::Factory($dsn);
    }
    public function Reset($host, $db, $user, $pass, $dsn = NULL){
        self::$Host    = $host;
        self::$Db      = $db;
        self::$User    = $user;
        self::$Pass    = $pass;
        self::$Connect = NULL;
        self::Factory($dsn);
    }
    private static function Factory($dsn = NULL){
        switch ($dsn):
            case "sqlite":   self::Sqlite();   break;
            case "firebird": self::Firebird(); break;
            case "postgree": self::Postgree(); break;
            default:         self::Mysql();    break;
        endswitch;
        self::$Connect->exec("set names utf8");
        self::$Connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    private static function Sqlite(){
        try{
            $dsn = 'sqlite:' . self::$Db . ';';
            self::$Connect = new PDO($dsn); 
        }
        catch (PDOException $e){
            echo "ERRO AO CONECTAR AO BANCO SQLITE:<br> ";
            echo "{$e->getCode()}, {$e->getMessage()}, {$e->getLine()}"; die;
        }
    }
    private static function Firebird() {
        try{
            $dsn = 'firebird:dbname=' . self::$Db . ';';
            self::$Connect = new PDO($dsn, self::$User, self::$Pass); 
        }
        catch (PDOException $e){
            echo "ERRO AO CONECTAR AO BANCO FIREBIRD:<br> ";
            echo "{$e->getCode()}, {$e->getMessage()}, {$e->getLine()}"; die;
        }
    }
    private static function Postgree() {
        $dsn   = 'pgsql:dbname=' . self::$Db    . ';' . 'user=' . self::$User  . ';';
        $dsn  .= 'password='     . self::$Pass  . ';' . 'host=' . self::$Host  . ';';
        try{ 
            self::$Connect = new PDO($dsn); 
        }
        catch (PDOException $e){
            echo "ERRO AO CONECTAR AO BANCO POSTGREE:<br> ";
            echo "{$e->getCode()}, {$e->getMessage()}, {$e->getLine()}"; die;
        }
    }
    private static function Mysql() {
        $dsn = 'mysql:host=' . self::$Host . ';dbname=' . self::$Db;
        try{
            self::$Connect = new PDO($dsn, self::$User, self::$Pass); 
        }
        catch (PDOException $e){
            echo "ERRO AO CONECTAR AO BANCO MYSQL:<br> ";
            echo "{$e->getCode()}, {$e->getMessage()}, {$e->getLine()}"; die;
        }
    }
    public function getConnection() {
        return self::$Connect;
    }
    
    /** 
    * @param String $query = Recebe uma String com a instrução
    * @param $result = Cria em tempo de execução um array dos resultados
    * @param bool $oneResult = Se true retorna apenas um resultado
    */
    public function Output($query, &$result, &$rows, $oneResult = false) {
        if(is_object($query) && 
          (get_class($query) == "Select" || get_class($query) == "Generic")):
            $this->Query = $query->getQuery();
        else:
            $this->Query = $query;
        endif;
        try{
            $this->Query = self::$Connect->query($this->Query);
            $this->Query->setFetchMode(PDO::FETCH_ASSOC);
            if(!$oneResult):
                $result = $this->Result = $this->Query->fetchAll();
            else:
                $result = $this->Result = $this->Query->fetch();
            endif;
            $rows = $this->Rows = $this->Query->rowCount();
        }
        catch (PDOException $e){
            echo "ERRO NA INSTRUÇÃO SQL:<br><br> ";
            echo $e->getMessage();
        }
    }
    /** 
    * @param object $QueryObject = Recebe um objeto da classe Select com a instrução e os valores
    * @param $result = Cria em tempo de execução um array dos resultados
    * @param bool $oneResult = Se true retorna apenas um resultado
    * @param bool $byValue = Se o array não for associativo este parâmetro deve ser ativado. (true)  
    */
    public function OutputBy($QueryObject, &$result, &$rows, $oneResult = false, $byValue = false){
        if(is_object($QueryObject) && 
          (get_class($QueryObject) == "Select" || get_class($QueryObject) == "Generic")):
            $this->Query = $QueryObject->getQuery();
            $values = $QueryObject->getValues();
            try{
                $this->Query = self::$Connect->prepare($this->Query);
                $this->Query->setFetchMode(PDO::FETCH_ASSOC);
                if(sizeof($values) == 1 && !$byValue):
                    $this->Query->bindValue(':' . key($values), current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                    goto execute;
                elseif(sizeof($values) == 1 && $byValue):
                    $this->Query->bindValue(1, current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                    goto execute;
                endif;
                if(!$byValue):
                    for($i=0; $i<sizeof($values); $i++){
                        $this->Query->bindValue(':' . key($values), current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                        next($values);
                    }
                else:
                    for($i=1; $i<=sizeof($values); $i++){
                        $this->Query->bindValue($i, current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                        next($values);
                    }
                endif;
                execute: $this->Query->execute();
                if(!$oneResult):
                    $result = $this->Result = $this->Query->fetchAll();
                else:
                    $result = $this->Result = $this->Query->fetch();
                endif;
                $rows = $this->Rows = $this->Query->rowCount();
                }
            catch (PDOException $e){
                echo "ERRO NA INSTRUÇÃO SQL:<br><br> ";
                echo $e->getMessage();
            }
        else:
            echo "Parâmetro inválido";
        endif;
    }
    /** 
    * @param String $query = recebe uma string de instrução INSERT
    */
    public function Input($query) {
        if(is_object($query) && 
          (get_class($query) == "Insert" || get_class($query) == "Generic")):
            $this->Query = $query->getQuery();
        else:
            $this->Query = $query;
        endif;
        try{
            $this->Query = self::$Connect->query($this->Query);
            $this->Result = self::$Connect->lastInsertId();
        }
        catch (PDOException $e){
            echo "ERRO NA INSTRUÇÃO SQL:<br><br> ";
            echo $e->getMessage();
        }
    }
    /** 
    * @param Insert $QueryObject = Recebe o objeto da Classe Insert e pega a instrução INSERT e os valores
    * @param bool $byValue = Se o array não for associativo este parâmetro deve ser ativado. (true)  
    */
    public function InputBy($QueryObject, $byValue = false){
        if(is_object($QueryObject) && 
          (get_class($QueryObject) == "Insert" || get_class($QueryObject) == "Generic")):
            $this->Query = $QueryObject->getQuery();
            $values = $QueryObject->getValues();
            try{
                $this->Query = self::$Connect->prepare($this->Query);
                if(sizeof($values) == 1 && !$byValue):
                    $this->Query->bindValue(':' . key($values), current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                    goto execute;
                elseif(sizeof($values) == 1 && $byValue):
                    $this->Query->bindValue(1, current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                    goto execute;
                endif;
                if(!$byValue):
                    for($i=0; $i<sizeof($values); $i++){
                        $this->Query->bindValue(':' . key($values), current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                        next($values);
                    }
                else:
                    for($i=1; $i<=sizeof($values); $i++){
                        $this->Query->bindValue($i, current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                        next($values);
                    }
                endif;
                execute: $this->Query->execute();
                $this->Result = self::$Connect->lastInsertId();
            }
            catch (PDOException $e){
                echo "ERRO NA INSTRUÇÃO SQL:<br><br> ";
                echo $e->getMessage();
            }
        else:
            echo "Parâmetro Inválido";
        endif;
    }
    /** 
    * @param String $query = Recebe a instrução UPDATE, DELETE, ETC e o executa
    */
    public function Execute($query) {
        if(is_object($query) && 
          (get_class($query) == "Update" || get_class($query) == "Delete" || get_class($query) == "Generic")):
            $this->Query = $query->getQuery();
        else:
            $this->Query = $query;
        endif;
        try{
            $this->Query = self::$Connect->query($this->Query);
            $this->Result = true;
        }
        catch (PDOException $e){
            echo "ERRO NA INSTRUÇÃO SQL:<br><br> ";
            echo $e->getMessage();
            $this->Result = false;
        }
    }
    /** 
    * @param Object $QueryObject = Recebe um objeto Updade, Delete ou Generic
    * @param bool $byValue = Se o array não for associativo este parâmetro deve ser ativado. (true)  
    * @return bool true em caso de sucesso ou false
    */
    public function ExecuteBy($QueryObject, $byValue = false){
        if(is_object($QueryObject) && 
          (get_class($QueryObject) == "Update" || get_class($QueryObject) == "Delete" || get_class($QueryObject) == "Generic")):
            $this->Query = $QueryObject->getQuery();
            $values = $QueryObject->getValues();
            try{
                $this->Query = self::$Connect->prepare($this->Query);
                if(sizeof($values) == 1 && !$byValue):
                    $this->Query->bindValue(':' . key($values), current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                    goto execute;
                elseif(sizeof($values) == 1 && $byValue):
                    $this->Query->bindValue(1, current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                    goto execute;
                endif;
                if(!$byValue):
                    for($i=0; $i<sizeof($values); $i++){
                        $this->Query->bindValue(':' . key($values), current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                        next($values);
                    }
                else:
                    for($i=1; $i<=sizeof($values); $i++){
                        $this->Query->bindValue($i, current($values), (is_int(current($values)) ? PDO::PARAM_INT : PDO::PARAM_STR) );
                        next($values);
                    }
                endif;
                execute: $this->Query->execute();
                $this->Result = true;
            }
            catch (PDOException $e){
                echo "ERRO NA INSTRUÇÃO SQL:<br><br> ";
                echo $e->getMessage();
                $this->Result = false;
            }
        else:
            echo "Parâmetro Inválido.";
        endif;
    }
    
    public function getResult() {
        return $this->Result;
    }

    public function getRows() {
        return $this->Rows;
    }
    
    public function __destruct() {}
}