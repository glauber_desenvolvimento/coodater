<?php

class Upload {

    private $Path;
    private $Caminho;
    private $Paste;
    private $Back;
    private $Quantidade = 0;
    private $FotosPermitidas = array('jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xlsx');

    function __construct($name = NULL, $paste = NULL, $back = false) {
        if (!empty($name) && isset($_FILES[$name]['name'])):
            if (!$back):
                $this->Back = "";
            else:
                $this->Back = "../";
            endif;
            $arquivo = explode(".", $_FILES[$name]['name']);
            $extensao = end($arquivo);
            if (in_array($extensao, $this->FotosPermitidas)):
                if (!empty($paste)):
                    $this->Paste = $paste;
                    $this->Caminho = $this->Back . $this->Paste . "/";
                    if(!file_exists($this->Caminho)):
                        mkdir($this->Caminho);
                    endif;
                else:
                    $this->Caminho = $this->Back;
                endif;
                echo $this->Path;
                $this->Path = $this->Caminho . uniqid() . "_" . $this->Quantidade . ".{$extensao}";
                move_uploaded_file($_FILES[$name]['tmp_name'], $this->Path);
            else:
                echo "Arquivo fora dos padrões<br><br>";
            endif;
            $this->Quantidade ++;
        endif;
    }

    public function createPaste($paste) {
        $this->Paste = $paste;
        $this->Caminho = $this->Back . $this->Paste . "/";
        if(!file_exists($this->Caminho)){
            mkdir($this->Caminho);
        }
        
    }
    
    public function setUpload($name) {
        if (isset($_FILES[$name]['name'])):
            $arquivo = explode(".", $_FILES[$name]['name']);
            $extensao = end($arquivo);
            if (in_array($extensao, $this->FotosPermitidas)):
                $this->Path = $this->Caminho . uniqid() . "_" . $this->Quantidade . ".{$extensao}";
                move_uploaded_file($_FILES[$name]['tmp_name'], $this->Path);
            else:
                echo "Arquivo fora dos padrões<br><br>";
            endif;
        endif;
        $this->Quantidade ++;
    }

    public function getPath() {
        return $this->Path;
    }

}
