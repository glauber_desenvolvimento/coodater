<?php
class ComposicaoFamiliar {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;

    public function __construct($connect) {
        $this->Connect = $connect;
    }
    
    public function getDados() {
        return $this->Dados;
    }
    
    public function getId($id){
        $this->Connect->Output("SELECT id_beneficiario FROM beneficiario WHERE id_beneficiario = {$id}", $result, $rows, true);
        $this->Dados = $result;
        return $result["id_beneficiario"];
    }
    
    public function setEmpty() {
        $this->Dados["Mãe"] = '';
        $this->Dados["Pai"] = '';
        $this->Dados["Esposo(a)"] = '';
        $this->Dados["Filho(a)"] = '';
        $this->Dados["Tio(a)"] = '';
        $this->Dados["Primo(a)"] = '';
        $this->Dados["Sobrinho(a)"] = '';
        $this->Dados["Avô"] = '';
        $this->Dados["Avó"] = '';
        $this->Dados["Neto(a)"] = '';
        $this->Dados["Cunhado(a)"] = '';
        $this->Dados["Genro"] = '';
        $this->Dados["Nora"] = '';
        $this->Dados["Outros"] = '';
        $this->Dados["nome_comp_familiar"] = '';
        $this->Dados["idade_comp_familiar"] = '';
        $this->Dados["cpf_comp_familiar"] = '';
        $this->Dados["parentesco_comp_familiar"] = '';
        $this->Dados["1"] = '';
        $this->Dados["0"] = '';
        
        $this->Connect->Output("SELECT * FROM escolaridade", $result, $rows);
        $esc = '';
        for($i=0; $i<$rows; $i++){

            $esc .= 
                "<label class='w-9 tx-7'>
                    <input class='flex' type='radio' required name='comp_familiar[id_escolaridade]' value='{$result[$i]["id_escolaridade"]}'>
                    &nbsp;{$result[$i]["escolaridade"]}
                </label>";
        }
        $this->Dados["escolaridade"] = $esc;
    }
    
    public function Criar($composicao) {
        
        $this->Dados = $composicao;
        $this->Query = new Insert("comp_familiar", $this->Dados);
        $this->Connect->InputBy($this->Query);

    }
    
    public function Ver($beneficiario) {
        
        $this->Query = new Select("comp_familiar");
        $this->Query->setTables("escolaridade");
        $this->Query->setWhere(array("id_beneficiario" => $beneficiario));
        $this->Query->setOrder("id_comp_familiar DESC", true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $lista = '';
        if($rows > 0){
            for($i=0; $i<$rows; $i++){
                $result[$i]["sexo_comp_familiar"] = ($result[$i]["sexo_comp_familiar"] == 1) ? "M" : "F";
                if($i%2 == 1){
                    $zebra = "class='bg-light-blue'";
                }else{
                    $zebra = '';
                }
                $lista .= "<tr {$zebra}>
                                <td>{$result[$i]["id_comp_familiar"]}</td>
                                <td>{$result[$i]["nome_comp_familiar"]}</td>
                                <td>{$result[$i]["sexo_comp_familiar"]}</td>
                                <td>{$result[$i]["idade_comp_familiar"]}</td>
                                <td>{$result[$i]["parentesco_comp_familiar"]}</td>
                                <td>{$result[$i]["cpf_comp_familiar"]}</td>
                                <td>{$result[$i]["escolaridade"]}</td>
                                <td>
                                    <a class='w-3' href='#SRC#composicao_familiar&editar={$result[$i]["id_comp_familiar"]}'>
                                        <img src='" . IMG . "select.png'>
                                    </a>
                                </td>
                                <td>
                                    <a class='w-3 abre_confirma' href='javascript:;' data-href='#SRC#composicao_familiar&deletar={$result[$i]["id_comp_familiar"]}'>
                                        <img src='" . IMG . "delete.png'>
                                    </a>
                                </td>
                          </tr>";
            }
        }else{
            $lista = "<tr><td colspan='9'><br><br>Este beneficiário ainda não possui parentes.<br><br><br></td></td>";
        }
        
        return $lista;
    }
    
    public function Editar($id) {
        $this->Query = new Select("comp_familiar");
        $this->Query->setTables("escolaridade");
        $this->Query->setWhere(array("id_comp_familiar" => $id), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        $this->Dados = $result;
        $this->Connect->Output("SELECT * FROM escolaridade", $escolaridade, $esc_rows);
        $esc = '';
        for($i=0; $i<$esc_rows; $i++){
            $check = ($this->Dados["escolaridade"] == $escolaridade[$i]["escolaridade"]) ? "checked" : '';
            $esc .= 
                "<label class='w-9 tx-7'>
                    <input {$check} class='flex' type='radio' required name='comp_familiar[id_escolaridade]' value='{$escolaridade[$i]["id_escolaridade"]}'>
                    &nbsp;{$escolaridade[$i]["escolaridade"]}
                </label>";
        }
        $this->Dados["escolaridade"] = $esc;
        $this->Dados["Mãe"] = '';
        $this->Dados["Pai"] = '';
        $this->Dados["Esposo(a)"] = '';
        $this->Dados["Filho(a)"] = '';
        $this->Dados["Tio(a)"] = '';
        $this->Dados["Primo(a)"] = '';
        $this->Dados["Sobrinho(a)"] = '';
        $this->Dados["Avô"] = '';
        $this->Dados["Avó"] = '';
        $this->Dados["Neto(a)"] = '';
        $this->Dados["Cunhado(a)"] = '';
        $this->Dados["Genro"] = '';
        $this->Dados["Nora"] = '';
        $this->Dados["Outros"] = '';
        $this->Dados["0"] = '';
        $this->Dados["1"] = '';
        $this->Dados[$this->Dados["sexo_comp_familiar"]] = "checked";
        $this->Dados[$this->Dados["parentesco_comp_familiar"]] = "selected";
    }
    
    public function Alterar($comp_familiar, $id) {
        $this->Query = new Update("comp_familiar", $comp_familiar);
        $this->Query->setWhere(array("id_comp_familiar" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    public function Deletar($id) {
        $this->Query = new Delete("comp_familiar", array("id_comp_familiar" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
}