<?php
class OrganizacaoSocial {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;


    public function __construct($connect) {
        $this->Connect = $connect;
    }

    public function setEmpty($id_beneficiario){
        
        $this->Dados["mensagem"] = '<br>';
        $this->Dados["id_beneficiario"] = $id_beneficiario;
        
        $this->Query = new Select("organizacao_social_aux");
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $this->Dados['tipo_organizacao_social'] = '';
        for($i=0; $i<$rows; $i++){
            $this->Dados['tipo_organizacao_social'] .= 
                "<span class='m-bottom-s'>
                    <label class='w-15'>
                        <input class='flex' name='organizacao_social[id_organizacao_social_aux][]' value='{$result[$i]["id_organizacao_social_aux"]}' type='checkbox'> {$result[$i]["organizacao_social_aux"]}
                    </label>
                </span>
                <br>";
        }
    }
    
    public function getDados($organizacao_social = NULL) {
        
        if(!empty($organizacao_social)){
            
            $this->Dados = $organizacao_social;

            $this->Query = new Select("organizacao_social_aux");
            $this->Connect->OutputBy($this->Query, $result, $rows);
            $this->Dados['tipo_organizacao_social'] = '';
            
            for($i=0; $i<$rows; $i++){
                
                $this->Query->Reset("organizacao_social", "id_organizacao_social_aux");
                $this->Query->setWhere(array("id_beneficiario" => $this->Dados["id_beneficiario"], "id_organizacao_social_aux" => $result[$i]["id_organizacao_social_aux"]), true, "AND");
                $this->Connect->OutputBy($this->Query, $tipos, $r_tipos, true);
                
                $checked = ($r_tipos == 1) ? "checked" : '';

                
                $this->Dados['tipo_organizacao_social'] .= 
                    "<span class='m-bottom-s'>
                        <label class='w-15'>
                            <input {$checked} class='flex' name='organizacao_social[id_organizacao_social_aux][]' value='{$result[$i]["id_organizacao_social_aux"]}' type='checkbox'> {$result[$i]["organizacao_social_aux"]}
                        </label>
                    </span>
                    <br>";
            }
        }
        
        return $this->Dados;
        
    }
    
    public function Alterar($organizacao_social) {
        
        $this->Dados = $organizacao_social;

        if(!empty($organizacao_social["id_organizacao_social_aux"])){
            $r_atual = sizeof($organizacao_social["id_organizacao_social_aux"]);
        }else{
            $r_atual = 0;
        }

        $this->Connect->Output("SELECT * FROM organizacao_social WHERE id_beneficiario = {$this->Dados["id_beneficiario"]}", $result, $r_anterior);
        
        if($r_atual == 0 && $r_anterior > 0){//RETIROU TODOS OS ITENS
            
            for ($i = 0; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM organizacao_social WHERE id_organizacao_social = {$result[$i]["id_organizacao_social"]}");
            }
            
        }elseif($r_anterior == $r_atual){//SÓ ATUALIZOU OS ITENS
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $this->Connect->Execute(
                    "UPDATE organizacao_social SET 
                    id_organizacao_social_aux = {$organizacao_social["id_organizacao_social_aux"][$i]}
                     WHERE id_organizacao_social = {$result[$i]["id_organizacao_social"]}"
                );
                    
            }
        
        }elseif($r_anterior > $r_atual){//FOI RETIRADO UM ITEM
            
            for ($i = 0; $i < $r_atual; $i++) {

                $this->Connect->Execute(
                    "UPDATE organizacao_social SET 
                    id_organizacao_social_aux = {$organizacao_social["id_organizacao_social_aux"][$i]}
                     WHERE id_organizacao_social = {$result[$i]["id_organizacao_social"]}"
                );
                    
            }
            
            for ($i = $r_atual; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM organizacao_social WHERE id_organizacao_social = {$result[$i]["id_organizacao_social"]}");
            }
            
        }elseif($r_anterior < $r_atual){//FOI ADICIONADO UM ITEM
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $this->Connect->Execute(
                    "UPDATE organizacao_social SET 
                    id_organizacao_social_aux = {$organizacao_social["id_organizacao_social_aux"][$i]}
                     WHERE id_organizacao_social = {$result[$i]["id_organizacao_social"]}"
                );
                    
            }
            
            for ($i = $r_anterior; $i < $r_atual; $i++) {
                $this->Connect->Input("INSERT INTO organizacao_social (id_beneficiario, id_organizacao_social_aux) VALUE ({$this->Dados["id_beneficiario"]}, {$organizacao_social["id_organizacao_social_aux"][$i]})");

            }
            
        }

       $this->Dados["Mensagem"] = "<div class='bx-radius p-b bg-white m-both-b tx-green'>Alterado com sucesso</div>";
        
    }
    
    public function Criar($organizacao_social) {
        
        $this->Dados = $organizacao_social;

        if(!empty($organizacao_social["id_organizacao_social_aux"])){
            $size = sizeof($organizacao_social["id_organizacao_social_aux"]);
        }else{
            $size = 0;
        }
        
        for ($i = 0; $i < $size; $i++) {

            $this->Connect->Input("INSERT INTO organizacao_social (id_beneficiario, id_organizacao_social_aux) VALUE ({$this->Dados["id_beneficiario"]}, {$this->Dados["id_organizacao_social_aux"][$i]})");
            
        }
        
    }
}