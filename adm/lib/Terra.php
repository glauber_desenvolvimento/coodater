<?php
class Terra {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;

    public function __construct($connect) {
        $this->Connect = $connect;
    }
    
    public function getDados() {
        return $this->Dados;
    }
    
    public function getId($id){
        $this->Connect->Output("SELECT id_beneficiario FROM beneficiario WHERE id_beneficiario = {$id}", $result, $rows, true);
        $this->Dados = $result;
        return $result["id_beneficiario"];
    }
    
    public function setEmpty() {
        
        $this->Dados["valor_und_terra"] = '';
        $this->Dados["area_terra"] = '';
        $this->Query = new Select("tipo_terra");
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $this->Dados["tipo_terra"] = '';
        for($i=0; $i<$rows; $i++){
            $this->Dados["tipo_terra"] .= 
                "<option value='{$result[$i]["id_tipo_terra"]}'>
                    {$result[$i]["tipo_terra"]}
                </option>";
        }
        
        $this->Query->Reset("medida");
        $this->Query->setIn("id_medida" ,array(1,9), true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $this->Dados["und_medidas"] = '';
        for($i=0; $i<$rows; $i++){
            $this->Dados["und_medidas"] .= 
                "<option value='{$result[$i]["id_medida"]}'>
                    {$result[$i]["medida"]}
                </option>";
        }
        
    }
    
    public function Criar($terra) {
        
        $terra["area_terra"] = str_replace(",", ".", $terra["area_terra"]);
        $terra["valor_und_terra"] = str_replace(",", ".", $terra["valor_und_terra"]);
        
        $this->Dados = $terra;
        $this->Query = new Insert("terra", $this->Dados);
        $this->Connect->InputBy($this->Query);
        
    }
    
    public function Ver($beneficiario) {
        
        $this->Query = new Select("terra");
        $this->Query->setTables(array("tipo_terra", "medida"), "LEFT");
        $this->Query->setWhere(array("id_beneficiario" => $beneficiario));
        $this->Query->setOrder("id_terra DESC", true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $lista = '';
        if($rows > 0){
            for($i=0; $i<$rows; $i++){
                if($i%2 == 1){
                    $zebra = "class='bg-light-blue'";
                }else{
                    $zebra = '';
                }

                $result[$i]["area_terra"] = $result[$i]["area_terra"] . ' ' . $result[$i]["medida"];
                $result[$i]["valor_und_terra"] = number_format($result[$i]["valor_und_terra"], 2, ",", ".");
                
                $lista .= "<tr {$zebra}>
                                <td>{$result[$i]["id_terra"]}</td>
                                <td>{$result[$i]["tipo_terra"]}</td>
                                <td>{$result[$i]["area_terra"]}</td>
                                <td>{$result[$i]["valor_und_terra"]}</td>
                                <td>
                                    <a class='w-3' href='#SRC#terras&editar={$result[$i]["id_terra"]}'>
                                        <img src='" . IMG . "select.png'>
                                    </a>
                                </td>
                                <td>
                                    <a class='w-3 abre_confirma' href='javascript:;' data-href='#SRC#terras&deletar={$result[$i]["id_terra"]}'>
                                        <img src='" . IMG . "delete.png'>
                                    </a>
                                </td>
                          </tr>";
            }
        }else{
            $lista = "<tr><td colspan='7'><br><br>Este beneficiário ainda não possui terras.<br><br><br></td></td>";
        }
        
        return $lista;
    }
    
    public function Editar($id) {
        $this->Query = new Select("terra");
        $this->Query->setTables(array("tipo_terra", "medida"), "LEFT");
        $this->Query->setWhere(array("id_terra" => $id), true);
        $this->Connect->OutputBy($this->Query, $result, $rows, true);
        $this->Dados = $result;

        $this->Query->Reset("tipo_terra");
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $this->Dados["tipo_terra"] = '';
        
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["id_tipo_terra"] == $result[$i]["id_tipo_terra"]) ? "selected" : '';
            $this->Dados["tipo_terra"] .= 
                "<option {$selected} value='{$result[$i]["id_tipo_terra"]}'>
                    {$result[$i]["tipo_terra"]}
                </option>";
        }
        
        $this->Query->Reset("medida");
        $this->Query->setIn("id_medida" ,array(1,9), true);
        $this->Connect->OutputBy($this->Query, $result, $rows);
        
        $this->Dados["und_medidas"] = '';
        for($i=0; $i<$rows; $i++){
            $selected = ($this->Dados["id_medida"] == $result[$i]["id_medida"]) ? "selected" : '';
            $this->Dados["und_medidas"] .= 
                "<option {$selected} value='{$result[$i]["id_medida"]}'>
                    {$result[$i]["medida"]}
                </option>";
        }
        
        $this->Dados["area_terra"] = str_replace(".", ",", $this->Dados["area_terra"]);
        $this->Dados["valor_und_terra"] = str_replace(".", ",", $this->Dados["valor_und_terra"]);
        
    }
    
    public function Alterar($terra, $id) {
        
        $terra["valor_und_terra"] = str_replace(",", ".", $terra["valor_und_terra"]);
        $terra["area_terra"] = str_replace(",", ".", $terra["area_terra"]);

        $this->Query = new Update("terra", $terra);
        $this->Query->setWhere(array("id_terra" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    public function Deletar($id) {
        $this->Query = new Delete("terra", array("id_terra" => $id), true);
        $this->Connect->ExecuteBy($this->Query);
    }
    
}