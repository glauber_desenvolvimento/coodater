<?php
class ProducaoFamiliar {
    /** @var DAO */
    private $Connect;
    private $Query;
    /** @var View */
    private $View;
    private $Dados;


    public function __construct($connect, $view) {
        $this->Connect = $connect;
        $this->View = $view;
    }
    
    public function getDados(array $producao = NULL) {
        
        if(!empty($producao)){
            $this->Dados = $producao;
            
            $this->Query = new Select("estado_local");
            $this->Connect->OutputBy($this->Query, $result, $rows);
            $this->Dados['estados'] = '';
            for($i=0; $i<$rows; $i++){
                
                $selected = ($this->Dados['id_estado_local'] == $result[$i]["id_estado_local"])
                          ? "selected"
                          : '';
                $this->Dados['estados'] .= 
                    "<option {$selected} value='{$result[$i]["id_estado_local"]}'>
                        {$result[$i]["estado_local"]}/{$result[$i]["sigla_estado_local"]}
                    </option>";
            }
            
            $this->Dados["mensagem"] = '<br>';
        }
        
        return $this->Dados;
    }

    public function setEmpty($id_beneficiario){
        $this->Connect->Output("SELECT MAX(id_producao_familiar) as id_producao_familiar FROM producao_familiar", $max, $row, true);
        $this->Query = new Select("producao_familiar");
        $this->Query->setWhere(array("id_producao_familiar" => $max["id_producao_familiar"]), true);
        $this->Connect->OutputBy( $this->Query, $result, $rows, true);
        $this->Dados = $result;
        $this->Dados["mensagem"] = '<br>';
        $this->Dados["id_beneficiario"] = $id_beneficiario;
        $this->Query->Reset("estado_local");
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $this->Dados['estados'] = '';
        for($i=0; $i<$rows; $i++){
            $this->Dados['estados'] .= 
                "<option value='{$result[$i]["id_estado_local"]}'>
                    {$result[$i]["estado_local"]}/{$result[$i]["sigla_estado_local"]}
                </option>";
        }
        
    }

    public function Alterar($producao_familiar) {
        
        $this->Dados = $producao_familiar;
        $this->Query = new Update("producao_familiar", $this->Dados);
        $this->Query->setWhere(array("id_producao_familiar" => $this->Dados["id_producao_familiar"]), true);
        $this->Connect->ExecuteBy($this->Query);
        
        $this->setMensagem("Dados da unidade alterada com sucesso", SRC . "dados_da_unidade_de_producao_familiar");
    }
    public function Criar($producao_familiar) {
        
        $this->Dados = $producao_familiar;
        $this->Query = new Update("producao_familiar", $this->Dados);
        $this->Query->setWhere(array("id_producao_familiar" => $this->Dados["id_producao_familiar"]), true);
        $this->Connect->ExecuteBy($this->Query);
        $this->Connect->Input("INSERT INTO producao_familiar (denominacao_producao_familiar) VALUE (NULL)");
        
        $this->setMensagem("Dados da unidade criada com sucesso", SRC . "dados_da_unidade_de_producao_familiar");
        
    }

    
    public function setMensagem($mensagem = NULL, $caminho = NULL) {
        if(func_num_args() == 2){
            $this->Dados["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     $mensagem
                 </div>";
            $this->Dados["mensagem"] .= 
            "<script>
                 $(function(){
                     window.setTimeout(
                         function(){
                             window.open('" . $caminho . "', '_self');
                         }, 1000
                     );
                 });
             </script>";
        }else{
            $this->Dados["mensagem"] = '';
        }
    }

}