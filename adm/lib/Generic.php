<?php
/**
 * <b>Classe Select</b> - Cria Objeto de consultas
 *    Classe que cria
 *    
 * @copyright 2014 Glauber Gonçalves dos Santos
 */
class Generic {
    private $Values;
    private $Query;
    private $Clause;

    public function __construct($Query = NULL) {
        if(func_num_args() > 0):
            $this->Query = $Query;
        endif;
    }

    public function Reset($Query) {
        
        $this->Values = NULL;
        $this->Clause = NULL;
        $this->Query = $Query;
    }
    
    public function setWhere($Key, array $Values, $end = false, $rule = 'OR', $Operator = '='){
        if(sizeof($Values) == 1):
            $this->Clause = " WHERE {$Key} {$Operator} ?";
        else:
            $this->Clause = " WHERE ";
            for($i=0; $i<sizeof($Values); $i++):
                if($i <> (sizeof($Values)-1)):
                    $this->Clause .= "{$Key} {$Operator} ? {$rule} ";
                else:
                    $this->Clause .= "{$Key} {$Operator} ?";
                endif;
            endfor;
        endif;
        $this->Values = $Values;
        if($end):
            $this->End();
        endif;
    }
    
    public function getClause() {
        return $this->Clause;
    }
    
    public function setClause($Clause, array $Values = NULL, $end = false) {
        $this->Clause = $Clause;
        $this->Values = (!empty($Values)) ? $Values : $this->Values;
        if($end){
            $this->End();
        }
    }
    
    public function End(){
        $this->Query .= $this->Clause;
        $this->Clause = NULL;
    }
    
    public function getValues() {
        return $this->Values;
    }

    public function getQuery() {
        return $this->Query;
    }

    public function setValues(array $Values) {
        $this->Values = $Values;
    }

    public function setQuery($Query) {
        $this->Query = $Query;
    }
    
    public function __toString(){
        return $this->Query;
    }
}
