<?php
class CaptacaoDaAgua {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;


    public function __construct($connect) {
        $this->Connect = $connect;
    }

    public function setEmpty($id_beneficiario){
        
        $this->Dados["mensagem"] = '<br>';
        $this->Dados["id_beneficiario"] = $id_beneficiario;
        
        $this->Query = new Select("captacao_da_agua_aux");
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $this->Dados['tipo_captacao_da_agua'] = '';
        for($i=0; $i<$rows; $i++){
            $this->Dados['tipo_captacao_da_agua'] .= 
                "<span class='m-bottom-s'>
                    <label class='w-20'>
                        <input class='flex' name='captacao_da_agua[id_captacao_da_agua_aux][]' value='{$result[$i]["id_captacao_da_agua_aux"]}' type='checkbox'> {$result[$i]["captacao_da_agua_aux"]}
                    </label>
                </span><br>";
        }
    }
    
    public function getDados($captacao_da_agua = NULL) {
        
        if(!empty($captacao_da_agua)){
            
            $this->Dados = $captacao_da_agua;

            $this->Query = new Select("captacao_da_agua_aux");
            $this->Connect->OutputBy($this->Query, $result, $rows);
            $this->Dados['tipo_captacao_da_agua'] = '';
            
            for($i=0; $i<$rows; $i++){
                
                $this->Query->Reset("captacao_da_agua", "id_captacao_da_agua_aux");
                $this->Query->setWhere(array("id_beneficiario" => $this->Dados["id_beneficiario"], "id_captacao_da_agua_aux" => $result[$i]["id_captacao_da_agua_aux"]), true, "AND");
                $this->Connect->OutputBy($this->Query, $tipos, $r_tipos, true);
                
                $checked = ($r_tipos == 1) ? "checked" : '';

                
                $this->Dados['tipo_captacao_da_agua'] .= 
                    "<span class='m-bottom-s'>
                        <label class='w-20'>
                            <input {$checked} class='flex' name='captacao_da_agua[id_captacao_da_agua_aux][]' value='{$result[$i]["id_captacao_da_agua_aux"]}' type='checkbox'> {$result[$i]["captacao_da_agua_aux"]}
                        </label>
                    </span><br>";
            }
            
            if(is_array($captacao_da_agua["id_captacao_da_agua_aux"])){
                $this->Dados["id_captacao_da_agua_aux"] = '';
            }
            
        }
        
        return $this->Dados;
        
    }
    
    public function Alterar($captacao_da_agua) {
        
        $this->Dados = $captacao_da_agua;

        if(!empty($captacao_da_agua["id_captacao_da_agua_aux"])){
            $r_atual = sizeof($captacao_da_agua["id_captacao_da_agua_aux"]);
        }else{
            $r_atual = 0;
        }

        $this->Connect->Output("SELECT * FROM captacao_da_agua WHERE id_beneficiario = {$this->Dados["id_beneficiario"]}", $result, $r_anterior);
        
        if($r_atual == 0 && $r_anterior > 0){//RETIROU TODOS OS ITENS
            
            for ($i = 0; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM captacao_da_agua WHERE id_captacao_da_agua = {$result[$i]["id_captacao_da_agua"]}");
            }
            
        }elseif($r_anterior == $r_atual){//SÓ ATUALIZOU OS ITENS
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $this->Connect->Execute(
                    "UPDATE captacao_da_agua SET 
                    id_captacao_da_agua_aux = {$captacao_da_agua["id_captacao_da_agua_aux"][$i]}
                    WHERE id_captacao_da_agua = {$result[$i]["id_captacao_da_agua"]}"
                );
                    
            }

        }elseif($r_anterior > $r_atual){//FOI RETIRADO UM ITEM
            
            for ($i = 0; $i < $r_atual; $i++) {

                $this->Connect->Execute(
                    "UPDATE captacao_da_agua SET 
                    id_captacao_da_agua_aux = {$captacao_da_agua["id_captacao_da_agua_aux"][$i]}
                     WHERE id_captacao_da_agua = {$result[$i]["id_captacao_da_agua"]}"
                );
                    
            }
            
            for ($i = $r_atual; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM captacao_da_agua WHERE id_captacao_da_agua = {$result[$i]["id_captacao_da_agua"]}");
            }
            
        }elseif($r_anterior < $r_atual){//FOI ADICIONADO UM ITEM
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $this->Connect->Execute(
                    "UPDATE captacao_da_agua SET 
                    id_captacao_da_agua_aux = {$captacao_da_agua["id_captacao_da_agua_aux"][$i]}
                     WHERE id_captacao_da_agua = {$result[$i]["id_captacao_da_agua"]}"
                );
                    
            }
            
            for ($i = $r_anterior; $i < $r_atual; $i++) {
                $this->Connect->Input("INSERT INTO captacao_da_agua (id_beneficiario, id_captacao_da_agua_aux) VALUE ({$this->Dados["id_beneficiario"]}, {$captacao_da_agua["id_captacao_da_agua_aux"][$i]})");

            }
            
        }

       $this->Dados["Mensagem"] = "<div class='bx-radius p-b bg-white m-both-b tx-green'>Alterado com sucesso</div>";
        
    }
    
    public function Criar($captacao_da_agua) {
        
        $this->Dados = $captacao_da_agua;

        if(!empty($captacao_da_agua["id_captacao_da_agua_aux"])){
            $size = sizeof($captacao_da_agua["id_captacao_da_agua_aux"]);
        }else{
            $size = 0;
        }
        
        for ($i = 0; $i < $size; $i++) {

            $this->Connect->Input("INSERT INTO captacao_da_agua (id_beneficiario, id_captacao_da_agua_aux) VALUE ({$this->Dados["id_beneficiario"]}, {$this->Dados["id_captacao_da_agua_aux"][$i]})");
            
        }
        
    }
}