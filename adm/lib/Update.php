<?php
/**
 * <b>Classe Select</b> - Cria Objeto de consultas
 *    Classe que cria
 *    
 * @copyright 2014 Glauber Gonçalves dos Santos
 */
class Update {
    private $Values;
    private $Query = "UPDATE ";
    private $Clause = '';

    public function __construct($Table = NULL, array $Columns = NULL, $end = false) {
        if(func_num_args() > 1):
            $this->Query .= "{$Table} SET ";
            for($i=0; $i<sizeof($Columns); $i++):
                $this->Query .= key($Columns) . " = :" . key($Columns);
                if($i<(sizeof($Columns)-1)):
                    $this->Query .= ", ";
                endif;
                next($Columns);
            endfor;
            $this->Values = $Columns;
            if($end):
                $this->End();
            endif;
        endif;
    }
    
    public function Reset($Table, array $Columns, $end = false) {
        $this->Query .= "{$Table} SET ";
        for($i=0; $i<sizeof($Columns); $i++):
            $this->Query .= key($Columns) . " = :" . key($Columns);
            if($i<(sizeof($Columns)-1)):
                $this->Query .= ", ";
            endif;
            next($Columns);
        endfor;
        $this->Values = $Columns;
        if($end):
            $this->End();
        endif;
    }
    
    public function setWhere(array $Where, $end = false, $rule = 'OR', $Operator = '='){
        if(sizeof($Where) == 1):
            $this->Clause = " WHERE " . key($Where) . " {$Operator} :" . key($Where);
        else:
            $this->Clause = " WHERE ";
            $i = 0;
            do{
                if($i++ <> (sizeof($Where)-1)):
                    $this->Clause .= key($Where) . " {$Operator} :" . key($Where) . " {$rule} ";
                else:
                    $this->Clause .= key($Where) . " {$Operator} :" . key($Where);
                endif;
            }while(next($Where));
        endif;
        if(is_array($this->Values)):
            $this->Values = array_merge($this->Values, $Where);
        else:
            $this->Values = $Where;
        endif;
        if($end):
            $this->End();
        endif;
    }
    
    public function getClause() {
        return $this->Clause;
    }
    
    public function setClause($Clause, array $Values = NULL, $end = false) {
        $this->Clause = $Clause;
        $this->Values = (!empty($Values)) ? $Values : $this->Values;
        if($end){
            $this->End();
        }
    }
    
    public function End(){
        $this->Query .= $this->Clause;
        $this->Clause = NULL;
    }
    
    public function getValues() {
        return $this->Values;
    }

    public function getQuery() {
        return $this->Query;
    }

    public function setValues(array $Values) {
        $this->Values = $Values;
    }

    public function setQuery($Query) {
        $this->Query = $Query;
    }
    
    public function __toString(){
        return $this->Query;
    }
}
