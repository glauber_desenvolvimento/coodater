<?php
class DestinacaoDoEsgoto {
    /** @var DAO */
    private $Connect;
    private $Query;
    private $Dados;


    public function __construct($connect) {
        $this->Connect = $connect;
    }

    public function setEmpty($id_beneficiario){
        
        $this->Dados["mensagem"] = '<br>';
        $this->Dados["id_beneficiario"] = $id_beneficiario;
        
        $this->Query = new Select("destinacao_do_esgoto_aux");
        $this->Connect->OutputBy($this->Query, $result, $rows);
        $this->Dados['tipo_destinacao_do_esgoto'] = '';
        for($i=0; $i<$rows; $i++){
            $this->Dados['tipo_destinacao_do_esgoto'] .= 
                "<span class='m-bottom-s'>
                    <label class='w-20'>
                        <input class='flex' name='destinacao_do_esgoto[id_destinacao_do_esgoto_aux][]' value='{$result[$i]["id_destinacao_do_esgoto_aux"]}' type='checkbox'> {$result[$i]["destinacao_do_esgoto_aux"]}
                    </label>
                </span><br>";
        }
    }
    
    public function getDados($destinacao_do_esgoto = NULL) {
        
        if(!empty($destinacao_do_esgoto)){
            
            $this->Dados = $destinacao_do_esgoto;

            $this->Query = new Select("destinacao_do_esgoto_aux");
            $this->Connect->OutputBy($this->Query, $result, $rows);
            $this->Dados['tipo_destinacao_do_esgoto'] = '';
            
            for($i=0; $i<$rows; $i++){
                
                $this->Query->Reset("destinacao_do_esgoto", "id_destinacao_do_esgoto_aux");
                $this->Query->setWhere(array("id_beneficiario" => $this->Dados["id_beneficiario"], "id_destinacao_do_esgoto_aux" => $result[$i]["id_destinacao_do_esgoto_aux"]), true, "AND");
                $this->Connect->OutputBy($this->Query, $tipos, $r_tipos, true);
                
                $checked = ($r_tipos == 1) ? "checked" : '';

                
                $this->Dados['tipo_destinacao_do_esgoto'] .= 
                    "<span class='m-bottom-s'>
                        <label class='w-20'>
                            <input {$checked} class='flex' name='destinacao_do_esgoto[id_destinacao_do_esgoto_aux][]' value='{$result[$i]["id_destinacao_do_esgoto_aux"]}' type='checkbox'> {$result[$i]["destinacao_do_esgoto_aux"]}
                        </label>
                    </span><br>";
            }
        }
        
        return $this->Dados;
        
    }
    
    public function Alterar($destinacao_do_esgoto) {
        
        $this->Dados = $destinacao_do_esgoto;

        if(!empty($destinacao_do_esgoto["id_destinacao_do_esgoto_aux"])){
            $r_atual = sizeof($destinacao_do_esgoto["id_destinacao_do_esgoto_aux"]);
        }else{
            $r_atual = 0;
        }

        $this->Connect->Output("SELECT * FROM destinacao_do_esgoto WHERE id_beneficiario = {$this->Dados["id_beneficiario"]}", $result, $r_anterior);
        
        if($r_atual == 0 && $r_anterior > 0){//RETIROU TODOS OS ITENS
            
            for ($i = 0; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM destinacao_do_esgoto WHERE id_destinacao_do_esgoto = {$result[$i]["id_destinacao_do_esgoto"]}");
            }
            
        }elseif($r_anterior == $r_atual){//SÓ ATUALIZOU OS ITENS
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $this->Connect->Execute(
                    "UPDATE destinacao_do_esgoto SET 
                    id_destinacao_do_esgoto_aux = {$destinacao_do_esgoto["id_destinacao_do_esgoto_aux"][$i]}
                    WHERE id_destinacao_do_esgoto = {$result[$i]["id_destinacao_do_esgoto"]}"
                );
                    
            }

        }elseif($r_anterior > $r_atual){//FOI RETIRADO UM ITEM
            
            for ($i = 0; $i < $r_atual; $i++) {

                $this->Connect->Execute(
                    "UPDATE destinacao_do_esgoto SET 
                    id_destinacao_do_esgoto_aux = {$destinacao_do_esgoto["id_destinacao_do_esgoto_aux"][$i]}
                     WHERE id_destinacao_do_esgoto = {$result[$i]["id_destinacao_do_esgoto"]}"
                );
                    
            }
            
            for ($i = $r_atual; $i < $r_anterior; $i++) {
                $this->Connect->Execute("DELETE FROM destinacao_do_esgoto WHERE id_destinacao_do_esgoto = {$result[$i]["id_destinacao_do_esgoto"]}");
            }
            
        }elseif($r_anterior < $r_atual){//FOI ADICIONADO UM ITEM
        
            for ($i = 0; $i < $r_anterior; $i++) {

                $this->Connect->Execute(
                    "UPDATE destinacao_do_esgoto SET 
                    id_destinacao_do_esgoto_aux = {$destinacao_do_esgoto["id_destinacao_do_esgoto_aux"][$i]}
                     WHERE id_destinacao_do_esgoto = {$result[$i]["id_destinacao_do_esgoto"]}"
                );
                    
            }
            
            for ($i = $r_anterior; $i < $r_atual; $i++) {
                $this->Connect->Input("INSERT INTO destinacao_do_esgoto (id_beneficiario, id_destinacao_do_esgoto_aux) VALUE ({$this->Dados["id_beneficiario"]}, {$destinacao_do_esgoto["id_destinacao_do_esgoto_aux"][$i]})");

            }
            
        }

       $this->Dados["Mensagem"] = "<div class='bx-radius p-b bg-white m-both-b tx-green'>Alterado com sucesso</div>";
        
    }
    
    public function Criar($destinacao_do_esgoto) {
        
        $this->Dados = $destinacao_do_esgoto;

        if(!empty($destinacao_do_esgoto["id_destinacao_do_esgoto_aux"])){
            $size = sizeof($destinacao_do_esgoto["id_destinacao_do_esgoto_aux"]);
        }else{
            $size = 0;
        }
        
        for ($i = 0; $i < $size; $i++) {

            $this->Connect->Input("INSERT INTO destinacao_do_esgoto (id_beneficiario, id_destinacao_do_esgoto_aux) VALUE ({$this->Dados["id_beneficiario"]}, {$this->Dados["id_destinacao_do_esgoto_aux"][$i]})");
            
        }
        
    }
}