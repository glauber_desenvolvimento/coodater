﻿<?php
class Navigator{
	public $byPage;
	public $Links;
	private $Ids;
	public function __construct($rows = NULL, $bypage = NULL){
            if(func_num_args()==2){
                $this->byPage = $bypage;//Amostragem por página
                $this->Links = (int) ceil($rows/$bypage);
            }
	}
        
        public function reset($rows, $bypage){
            $this->byPage = $bypage;//Amostragem por página
            $this->Links = (int) ceil($rows/$bypage);
	}
        
	public function setIds($result, $id){
            $index = 0;
            $i = 0;
            $x = 0;
            do{
                $this->Ids[$index][$i] = $result[$x][$id];
                if($i == ($this->byPage-1))
                    {$i = 0; $index++;}
                else
                    {$i++;}
                $x++;
            }while(next($result));
	}
        
	public function getIds($index){
            if(array_key_exists($index, $this->Ids)):
                return $this->Ids[$index];
            else:
                return $this->Ids[0];
            endif;
	}
        
	public function Paginator($page, $class = NULL){
            $paginator = '';
            if($this->Links > 1):
                for($i=0; $i<$this->Links ; $i++):
                    $paginator .= '<a href="' . $page . '&i=' . $i . '"';
                    if(!empty($class)):
                        $paginator .= ' class="' . $class;
                        if(isset($_GET['i']) && $_GET['i'] == $i):
                            $paginator .= ' a-active';
                        elseif(!isset($_GET['i']) && !$i):
                            $paginator .= ' a-active';
                        endif;
                        $paginator .= '"';
                    endif;
                    $paginator .= ">" . ($i+1) . '</a>';
                endfor;
            endif;
            return $paginator;
	}
        
        public function searchKey($id) {
            
            $pagina = 0;
            while($pagina < $this->Links){
                $position = array_search($id, $this->Ids[$pagina]);
                if(!($position === false)){
                    break;
                }
                $pagina++;
            }
            
            if($position === false){
                $position = 0;
                $pagina   = 0;
            }
            
            return array($pagina, $position);
            
        }
        
        public function getPaginator($id, $page, $class) {
            $paginator = '';
            $offset    = $this->searchKey($id);
            $pagina    = $offset[0];
            $position  = $offset[1];
            $bypage    = sizeof($this->Ids[$pagina]);
            
            $back_position = $position - 1;
            if(array_key_exists($back_position, $this->Ids[$pagina])){

                $paginator .= "<a class='{$class}' href='" . $page . "{$this->Ids[$pagina][$back_position]}'>Anterior</a>";
                    
            }elseif($position == 0){

                $back_pagina = $pagina - 1;
                if(array_key_exists($back_pagina, $this->Ids)){
                    $paginator .= "<a class='{$class}' href='" . $page . "{$this->Ids[$back_pagina][(sizeof($this->Ids[$back_pagina])-1)]}'>Anterior</a>";
                }

            }

            $next_position = $position + 1;
            if(array_key_exists($next_position, $this->Ids[$pagina])){

                $paginator .= "<a class='{$class}' href='" . $page . "{$this->Ids[$pagina][$next_position]}'>Próximo</a>";
                    
            }elseif($position == ($bypage-1)){

                $next_pagina = $pagina + 1;
                if(array_key_exists($next_pagina, $this->Ids)){
                    $paginator .= "<a class='{$class}' href='" . $page . "{$this->Ids[$next_pagina][0]}'>Próximo</a>";
                }

            }
            return $paginator;
        }
}