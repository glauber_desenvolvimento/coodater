<?php
#### Automação de Objetos ####
function __autoload($class) {
    if (file_exists("adm/lib/{$class}.php")):
        require "adm/lib/{$class}.php";
    else:
        die("<h3>Classe não encontrada.</h3>");
    endif;
}


#### Variaveis Globais ####
$GLOBALS['D'] = new DAO;
$GLOBALS['V'] = new View;


