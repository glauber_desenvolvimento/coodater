<?php
session_start();
ini_set("display_errors", "On");
ini_set("date.timezone", "America/Sao_Paulo");
ini_set("upload_max_filesize","1M");
header("Content-Type: text/html; charset=UTF-8");

#### Função que impede ataques ####
function antiGetAtack($name) {
    $get = strip_tags(trim(filter_input(INPUT_GET, $name, FILTER_DEFAULT)));
    return $get;
}

#### Tratando a variável G como um array ####
$_GET['G'] = antiGetAtack('G');
$_GET['G'] = (empty($_GET['G'])) ? "home" : $_GET['G'];
$_GET['G'] = explode('/', $_GET['G']);

#### Constantes ####
define('RESPONSIVE',    false); ### R - Responsive
define('SITE_NAME',     "Coodater"); ### R - Responsive
define('COPY',          "Copyright " . SITE_NAME . " - " . date("Y") . ". Todos os direitos reservados."); ### R - Responsive
define("SRC",           "http://{$_SERVER["HTTP_HOST"]}/"); ### Caminho Absoluto
define("IMG",           "http://{$_SERVER["HTTP_HOST"]}/img/"); ### Caminho Absoluto de Imagens
define('LAST_G',        end($_GET['G']));
define('FIRST_G',       $_GET['G'][0]);
 

