<?php
#### Preparando o site ####
$head['sitename']           = SITE_NAME;
$head['title']              = (isset($_GET["G"]) && $_GET["G"][0] <> "index") 
                                    ? SITE_NAME . "-" . str_replace("_", " ", ucfirst($_GET["G"][0])) 
                                    : SITE_NAME;
$head['viewport']           = "device-width, initial-scale=1, maximum-scale=1";
$head['SRC']                = SRC;
$head['IMG']                = IMG;
$GLOBALS["tpl"]["head"]     = $GLOBALS['V']->prepareView("head", $head);
