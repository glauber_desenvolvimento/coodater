<?php
#### Preparando a View ####

if(isset($_SESSION[SITE_NAME])){
    $topo['SRC']              = SRC;
    $topo['IMG']              = IMG;
    $topo["usuario"]          = $_SESSION[SITE_NAME]["nome_usuario"];
    $topo["funcao"]           = ($_SESSION[SITE_NAME]["funcao_usuario"]) ? "Administrador(a)" : "Técnico";
    $GLOBALS["tpl"]["header"] = $GLOBALS['V']->prepareView("header", $topo);
}else{
    $GLOBALS["tpl"]["header"] = '';
}

