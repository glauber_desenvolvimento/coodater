<?php
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    
    $dados = $_SESSION[SITE_NAME]["beneficiario"];
    $beneficiario["nome_beneficiario"] = $dados["nome_beneficiario"];
    $b = explode(' ', $dados["nome_beneficiario"]);
    $beneficiario["titulo_beneficiario"] = $b[0];
    $beneficiario["beneficiario"] = $GLOBALS['V']->prepareView("beneficiario_dados", $dados);

    $query = new Select("comp_familiar",
                array("nome_comp_familiar", "parentesco_comp_familiar")
            );
    $query->setWhere(array("id_beneficiario" => $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]), true);
    $GLOBALS["D"]->OutputBy($query, $result, $rows);
    
    if($rows > 0){
        $familiares = '';
        for($i=0; $i<$rows; $i++){
            $familiares .= "<span class='w-15'>&bull; {$result[$i]["nome_comp_familiar"]} ({$result[$i]["parentesco_comp_familiar"]})</span>";
        }
        $beneficiario["beneficiario"] .= $GLOBALS['V']->prepareView("comp_familiar", array("familiares" => $familiares));
    }
    
    $query->Reset("terra", "tipo_terra");
    $query->setTables("tipo_terra", "LEFT");
    $query->setWhere(array("id_beneficiario" => $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]), true);
    $GLOBALS["D"]->OutputBy($query, $result, $rows);
    
    if($rows > 0){
        $terras = '';
        for($i=0; $i<$rows; $i++){
            $terras .= "<span class='w-15'>&bull; {$result[$i]["tipo_terra"]} </span>";
        }
        $beneficiario["beneficiario"] .= $GLOBALS['V']->prepareView("terras", array("terras" => $terras));
    }
    
    $query->Reset("producao_familiar",
                array("denominacao_producao_familiar", "endereco_producao_familiar", "chacara_producao_familiar")
            );
    $query->setWhere(array("id_beneficiario" => $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]), true);
    $GLOBALS["D"]->OutputBy($query, $result, $rows, true);
    if($rows > 0){
        $beneficiario["beneficiario"] .= $GLOBALS['V']->prepareView("producao_familiar", $result);
    }
    
    $query->Reset("benfeitoria", "tipo_benfeitoria");
    $query->setTables("tipo_benfeitoria");
    $query->setWhere(array("id_beneficiario" => $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]), true);
    $GLOBALS["D"]->OutputBy($query, $result, $rows);
    if($rows > 0){
        $benfeitorias = '';
        for($i=0; $i<$rows; $i++){
            $benfeitorias .= "<span class='w-15'>&bull; {$result[$i]["tipo_benfeitoria"]}</span>";
        }
        $beneficiario["beneficiario"] .= $GLOBALS['V']->prepareView("benfeitorias", array("benfeitorias" => $benfeitorias));
    }
    
    
    $query->Reset("mq_eq_tr", array("qtd_mq_eq_tr", "especificacao_mq_eq_tr"));
    $query->setWhere(array("id_beneficiario" => $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]), true);
    $GLOBALS["D"]->OutputBy($query, $result, $rows);
    if($rows > 0){
        $benfeitorias = '';
        for($i=0; $i<$rows; $i++){
            $benfeitorias .= "<span class='w-15'><b>Especificação:</b> <span>{$result[$i]["especificacao_mq_eq_tr"]}</span></span><span class='w-8 j_break_a'><b>Qtde: </b><span>{$result[$i]["qtd_mq_eq_tr"]}</span></span>";
    
        }
        $beneficiario["beneficiario"] .= $GLOBALS['V']->prepareView("mq_eq_tr", array("mq_eq_tr" => $benfeitorias));
    }
    
    $query->Reset("semovente", array("tipo_semovente", "qtd_semovente"));
    $query->setTables("tipo_semovente");
    $query->setWhere(array("id_beneficiario" => $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]), true);
    $GLOBALS["D"]->OutputBy($query, $result, $rows);
    if($rows > 0){
        $benfeitorias = '';
        for($i=0; $i<$rows; $i++){
            $benfeitorias .= "<span class='w-15'><b>Semovente:</b> <span>{$result[$i]["tipo_semovente"]}</span></span><span class='w-8 j_break_a'><b>Qtde: </b><span>{$result[$i]["qtd_semovente"]}</span></span>";
        }
        $beneficiario["beneficiario"] .= $GLOBALS['V']->prepareView("semoventes", array("semoventes" => $benfeitorias));
    }
    
    $query->Reset("cult_agricola", "atividade");
    $query->setTables("atividade");
    $query->setWhere(array("id_beneficiario" => $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]), true);
    $GLOBALS["D"]->OutputBy($query, $result, $rows);
    if($rows > 0){
        $atividades = '';
        for($i=0; $i<$rows; $i++){
            $atividades .= "<span class='w-15'>&bull; {$result[$i]["atividade"]}</span>";
        }
        $beneficiario["beneficiario"] .= $GLOBALS['V']->prepareView("atividades", array("atividades" => $atividades));
    }
    
    $query->Reset("renda_fdp", array("nome_renda_fdp", "parentesco_renda_fdp", "valor_renda_fdp"));
    $query->setWhere(array("id_beneficiario" => $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]), true);
    $GLOBALS["D"]->OutputBy($query, $result, $rows);
    if($rows > 0){
        $renda_fdp = '';
        for($i=0; $i<$rows; $i++){
            $renda_fdp .= "<span class='full'>&bull; {$result[$i]["nome_renda_fdp"]} - {$result[$i]["parentesco_renda_fdp"]}: R$" . number_format($result[$i]["valor_renda_fdp"], 2, ",", ".") . "</span>";
        }
        $beneficiario["beneficiario"] .= $GLOBALS['V']->prepareView("renda_fdp", array("fdp" => $renda_fdp));
    }
    
    $query->Reset("renda_fda", array("nome_renda_fda", "parentesco_renda_fda", "receita_renda_fda"));
    $query->setWhere(array("id_beneficiario" => $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]), true);
    $GLOBALS["D"]->OutputBy($query, $result, $rows);
    if($rows > 0){
        $renda_fdp = '';
        for($i=0; $i<$rows; $i++){
            $renda_fdp .= "<span class='full'>&bull; {$result[$i]["nome_renda_fda"]} - {$result[$i]["parentesco_renda_fda"]}: R$" . number_format($result[$i]["receita_renda_fda"], 2, ",", ".") . "</span>";
        }
        $beneficiario["beneficiario"] .= $GLOBALS['V']->prepareView("renda_fda", array("fda" => $renda_fdp));
    }
    
    $query->Reset("comercializacao", array("atividade", "tipo_semovente"));
    $query->setTables(array("atividade", "tipo_semovente"), "LEFT");
    $query->setWhere(array("id_beneficiario" => $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]), true);
    $GLOBALS["D"]->OutputBy($query, $result, $rows);
    if($rows > 0){
        $comercializacao = '';
        for($i=0; $i<$rows; $i++){
            
            $result[$i]["comercializao"] = (!empty($result[$i]["atividade"])) ? $result[$i]["atividade"] : $result[$i]["tipo_semovente"] ;
            $comercializacao .= "<span class='w-15'>&bull; {$result[$i]["comercializao"]}</span>";
            
        }
        $beneficiario["beneficiario"] .= $GLOBALS['V']->prepareView("comercializacao", array("comercializacao" => $comercializacao));
    }
    
}else{
    $beneficiario["nome_beneficiario"] = "Beneficiário inexistente";
    $beneficiario["titulo_beneficiario"] = "Beneficiário";
    $beneficiario["beneficiario"] = "Por favor, crie um novo Beneficiário";
    
}
    $beneficiario["SRC"] = SRC;
    $beneficiario["IMG"] = IMG;
    $GLOBALS["tpl"]["modal"] = $GLOBALS['V']->prepareView("beneficiario", $beneficiario);