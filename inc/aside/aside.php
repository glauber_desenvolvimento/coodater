<?php
#### Preparando a View ####
$aside["SRC"] = SRC;

$aside["act-beneficiario"] = 'a-box';
$aside["drop-beneficiario"] = '';

$aside["act-comp"]         = 'a-box';

$aside["act-und-prod"]     = 'a-box';
$aside["drop-und-prod"]    = '';

$aside["act-renda"]        = 'a-box';
$aside["drop-renda"]       = '';

$aside["act-comercial"]    = 'a-box';
$aside["act-acesso"]       = 'a-box';
$aside["act-org"]          = 'a-box';

$aside["act-amb"]          = 'a-box';
$aside["drop-amb"]         = '';

$aside["act-relatorio"]    = 'a-box';
$aside["act-usuario"]      = 'a-box';
$aside["drop-usuario"]     = '';

switch (FIRST_G){
    case "home":
                                $aside["act-beneficiario"] = 'a-box-2'; 
                                $aside["drop-beneficiario"]= "data-open"; break;
    case "beneficiario":
                                $aside["act-beneficiario"] = 'a-box-2'; 
                                $aside["drop-beneficiario"]= "data-open"; break;
    
    case "composicao_familiar": $aside["act-comp"]         = 'a-box-2'; break;
    
    case "dados_da_unidade_de_producao_familiar":
                                $aside["act-und-prod"]     = 'a-box-2'; 
                                $aside["drop-und-prod"]    = "data-open"; break;
    case "terras":
                                $aside["act-und-prod"]     = 'a-box-2'; 
                                $aside["drop-und-prod"]    = "data-open"; break;
    case "benfeitorias":
                                $aside["act-und-prod"]     = 'a-box-2'; 
                                $aside["drop-und-prod"]    = "data-open"; break;
    case "maquinas_equipamentos_de_trabalho":
                                $aside["act-und-prod"]     = 'a-box-2'; 
                                $aside["drop-und-prod"]    = "data-open"; break;
    case "semoventes":
                                $aside["act-und-prod"]     = 'a-box-2'; 
                                $aside["drop-und-prod"]    = "data-open"; break;
    
    case "cultivo_agricola":
                                $aside["act-renda"]        = 'a-box-2';
                                $aside["drop-renda"]       = "data-open";break;
    case "fora_da_propriedade":
                                $aside["act-renda"]        = 'a-box-2';
                                $aside["drop-renda"]       = "data-open";break;
    case "fora_da_agricultura":
                                $aside["act-renda"]        = 'a-box-2';
                                $aside["drop-renda"]       = "data-open";break;
                            
    case "comercializacao":     $aside["act-comercial"]    = 'a-box-2'; break;
    case "acesso_a_politicas_publicas": 
                                $aside["act-acesso"]       = 'a-box-2'; break;
    case "organizacao_social":  $aside["act-org"]          = 'a-box-2'; break;
    
    case "protecao_ambiental": 
                                $aside["act-amb"]          = 'a-box-2'; 
                                $aside["drop-amb"]         = "data-open"; break;
    case "praticas_agroecologicas": 
                                $aside["act-amb"]          = 'a-box-2'; 
                                $aside["drop-amb"]         = "data-open"; break;
    case "agrobiodiversidade": 
                                $aside["act-amb"]          = 'a-box-2'; 
                                $aside["drop-amb"]         = "data-open"; break;
    case "destinacao_do_lixo": 
                                $aside["act-amb"]          = 'a-box-2'; 
                                $aside["drop-amb"]         = "data-open"; break;
    case "destinacao_do_esgoto": 
                                $aside["act-amb"]          = 'a-box-2'; 
                                $aside["drop-amb"]         = "data-open"; break;
    case "captacao_da_agua": 
                                $aside["act-amb"]          = 'a-box-2'; 
                                $aside["drop-amb"]         = "data-open"; break;
    
    case "relatorios":          $aside["act-relatorio"]    = 'a-box-2'; break;
    case "usuario":
                                $aside["act-usuario"]      = 'a-box-2'; 
                                $aside["drop-usuario"]     = "data-open"; break;
}

$GLOBALS['tpl']['menu_bar'] = $GLOBALS['V']->prepareView("menu_bar", $aside);