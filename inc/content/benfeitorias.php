<?php

#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    $benfeitoria = new Benfeitoria($GLOBALS['D']);

    if(isset($_POST['benfeitorias'])){
        $benfeitoria->Criar($_POST["benfeitoria"]);
        header("Location: " . SRC . '/benfeitorias');
    }elseif(isset($_POST['alterar']) && !empty($_GET['editar'])){
        $benfeitoria->Alterar($_POST["benfeitoria"], $_GET['editar']);
        header("Location: " . SRC . '/benfeitorias');
    }elseif(!empty($_GET['deletar'])){
        $benfeitoria->Deletar($_GET['deletar']);
        header("Location: " . SRC . '/benfeitorias');
    }elseif(!empty($_GET['editar'])){
        $benfeitoria->Editar($_GET['editar']);
        $main = $benfeitoria->getDados();
    }else{
        $benfeitoria->setEmpty();
        $main = $benfeitoria->getDados();
    }
    $main["action"] = (!empty($_GET["editar"])) ? "alterar" : "benfeitorias";
    $main["linhas"]         = $benfeitoria->Ver($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
    $main['SRC']            = SRC;
    $main['IMG']            = IMG;
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("benfeitorias", $main);
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}