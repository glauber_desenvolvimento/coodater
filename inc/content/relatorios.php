<?php

#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    $relatorios = new Relatorios($GLOBALS['D']);

    if(isset($_POST['criar'])){
        $relatorios->Criar($_POST["relatorio"]);
        header("Location: " . SRC . '/relatorios');
    }elseif(isset($_POST['alterar']) && !empty($_GET['editar'])){
        $relatorios->Alterar($_POST["relatorio"]);
        header("Location: " . SRC . '/relatorios');
    }elseif(!empty($_GET['deletar'])){
        $relatorios->Deletar($_GET['deletar']);
        header("Location: " . SRC . '/relatorios');
    }elseif(!empty($_GET['editar']) || !empty($_GET['deletar_arquivo'])){
        
        if(!empty($_GET['deletar_arquivo'])){
            $relatorios->deletarArquivo($_GET['deletar_arquivo']);
            header("Location: " . SRC . "relatorios&editar={$_GET['deletar_arquivo']}" );
        }
        
        $relatorios->Editar($_GET['editar']);
        $main = $relatorios->getDados();
    }else{
        $main = $relatorios->getDados();
        $main["arquivo_relatorio"] = "<input type='file' name='arquivo' class='no-p bdr-none'>";
        $main["relatorio"]         = '';
        $main["id_relatorio"]      = '';
    }
    $main["linhas"]          = $relatorios->Ver($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
    $main["id_beneficiario"] = $_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"];
    $main["action"]          = (!empty($_GET['editar']) || !empty($_GET['deletar_arquivo'])) ? "alterar" : "criar";
    $main['SRC']             = SRC;
    $main['IMG']             = IMG;
    $GLOBALS["tpl"]["main"]  = $GLOBALS['V']->prepareView("relatorios", $main);
}else{
    $GLOBALS["tpl"]["main"]  = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}



