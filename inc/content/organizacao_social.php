<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    
    $organizacao_social = new OrganizacaoSocial($GLOBALS['D']);

    $GLOBALS['D']->Output("SELECT * FROM organizacao_social WHERE id_beneficiario = {$_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]}", $result, $rows, true);

    if($rows > 0){
        if(isset($_POST["alterar"])){
            
            $organizacao_social->Alterar($_POST["organizacao_social"]);
            $main = $organizacao_social->getDados($_POST["organizacao_social"]);
            $main["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     Alterado com sucesso.
                 </div>
                 <script>
                     $(function(){
                         window.setTimeout(
                             function(){
                                 window.open('" . SRC . "organizacao_social', '_self');
                             }, 1000
                         );
                     });
                </script>";
            unset($main["id_organizacao_social_aux"]);
        }else{
            
            $main = $organizacao_social->getDados($result);
            $main["mensagem"] = "<br>";
            
        }
        $main["action"] = "alterar";
        $main["value"] = "Alterar";
    }else{
        if(isset($_POST["criar"])){
            
            $organizacao_social->Criar($_POST["organizacao_social"]);
            $main = $organizacao_social->getDados($_POST["organizacao_social"]);
            unset($main["id_organizacao_social_aux"]);
            
            if(empty($_POST["organizacao_social"]["id_organizacao_social_aux"])){
                $main["mensagem"] = 
                    "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                         Selecione uma opção.
                     </div>";
            }else{
                $main["mensagem"] = 
                    "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                         Criado com sucesso.
                     </div>
                     <script>
                     $(function(){
                         window.setTimeout(
                             function(){
                                 window.open('" . SRC . "organizacao_social', '_self');
                             }, 1000
                         );
                     });
                    </script>";
            }
        }else{
            $organizacao_social->setEmpty($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
            $main = $organizacao_social->getDados();
        }
        $main["action"] = "criar";
        $main["value"] = "Criar";
    }
    
    $main['SRC']              = SRC;
    $main['IMG']              = IMG;

    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("organizacao_social", $main);
    
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}