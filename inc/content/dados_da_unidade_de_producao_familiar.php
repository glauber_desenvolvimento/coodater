<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    
    $producao_familiar = new ProducaoFamiliar($GLOBALS['D'], $GLOBALS['V']);

    $GLOBALS['D']->Output("SELECT * FROM producao_familiar WHERE id_beneficiario = {$_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]}", $result, $rows, true);

    if($rows == 1){
        if(isset($_POST["alterar"])){
            $producao_familiar->Alterar($_POST["producao_familiar"]);
            $main = $producao_familiar->getDados();
        }else{
            $main = $producao_familiar->getDados($result);
        }
        $main["action"] = "alterar";
        $main["value"] = "Alterar";
    }else{
        if(isset($_POST["criar"])){
            $producao_familiar->Criar($_POST["producao_familiar"]);
            $main = $producao_familiar->getDados();
        }else{
            $producao_familiar->setEmpty($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
            $main = $producao_familiar->getDados();
        }
        $main["action"] = "criar";
        $main["value"] = "Criar";
    }
    
    $main['SRC']              = SRC;
    $main['IMG']              = IMG;

    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("dados_da_unidade_de_producao_familiar", $main);
    
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}