<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    $comercializacao = new Comercializacao($GLOBALS['D']);

    if(isset($_POST['salvar'])){
        $comercializacao->Criar($_POST["comercializacao"]);
        header("Location: " . SRC . '/comercializacao');
    }elseif(isset($_POST['alterar']) && !empty($_GET['editar'])){
        $comercializacao->Alterar($_POST["comercializacao"], $_GET['editar']);
        header("Location: " . SRC . '/comercializacao');
    }elseif(!empty($_GET['deletar'])){
        $comercializacao->Deletar($_GET['deletar']);
        header("Location: " . SRC . '/comercializacao');
    }elseif(!empty($_GET['editar'])){
        $comercializacao->Editar($_GET['editar']);
        $main = $comercializacao->getDados();
    }else{
        $main["id_beneficiario"] = $comercializacao->getId($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
        $comercializacao->setEmpty();
        $main = $comercializacao->getDados();
    }
    $main["action"] = (!empty($_GET["editar"])) ? "alterar" : "salvar";
    $main["linhas"]         = $comercializacao->Ver($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
    $main['SRC']            = SRC;
    $main['IMG']            = IMG;
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("comercializacao", $main);
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}



