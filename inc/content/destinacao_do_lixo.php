<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    
    $destinacao_do_lixo = new DestinacaoDoLixo($GLOBALS['D']);

    $GLOBALS['D']->Output("SELECT * FROM destinacao_do_lixo WHERE id_beneficiario = {$_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]}", $result, $rows, true);

    if($rows > 0){
        if(isset($_POST["alterar"])){
            
            $destinacao_do_lixo->Alterar($_POST["destinacao_do_lixo"]);
            $main = $destinacao_do_lixo->getDados($_POST["destinacao_do_lixo"]);
            $main["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     Alterado com sucesso.
                 </div>
                 <script>
                     $(function(){
                         window.setTimeout(
                             function(){
                                 window.open('" . SRC . "destinacao_do_lixo', '_self');
                             }, 1000
                         );
                     });
                </script>";
            unset($main["id_destinacao_do_lixo_aux"]);
        }else{
            
            $main = $destinacao_do_lixo->getDados($result);
            $main["mensagem"] = "<br>";
            
        }
        $main["action"] = "alterar";
        $main["value"] = "Alterar";
    }else{
        if(isset($_POST["criar"])){
            
            $destinacao_do_lixo->Criar($_POST["destinacao_do_lixo"]);
            $main = $destinacao_do_lixo->getDados($_POST["destinacao_do_lixo"]);
            unset($main["id_destinacao_do_lixo_aux"]);
            
            if(empty($_POST["destinacao_do_lixo"]["id_destinacao_do_lixo_aux"])){
                $main["mensagem"] = 
                    "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                         Selecione uma opção.
                     </div>";
            }else{
                $main["mensagem"] = 
                    "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                         Criado com sucesso.
                     </div>
                     <script>
                     $(function(){
                         window.setTimeout(
                             function(){
                                 window.open('" . SRC . "destinacao_do_lixo', '_self');
                             }, 1000
                         );
                     });
                    </script>";
            }
        }else{
            $destinacao_do_lixo->setEmpty($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
            $main = $destinacao_do_lixo->getDados();
        }
        $main["action"] = "criar";
        $main["value"] = "Criar";
    }
    
    $main['SRC']              = SRC;
    $main['IMG']              = IMG;

    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("destinacao_do_lixo", $main);
    
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}