<?php
#### Preparando a View ####
$beneficiario = new Beneficiario($GLOBALS["D"], $GLOBALS["V"]);
$page         = LAST_G;

if(isset($_GET["novo"])){
    $_SESSION[SITE_NAME]["beneficiario"] = NULL;
}

if(end($_GET["G"]) == "selecionar"){
    
    $index = isset($_GET["i"]) ? $_GET["i"] : NULL ;
    $order = isset($_GET["order"]) ? $_GET["order"] : "id_beneficiario";
    $desc = (isset($_GET["desc"]) && isset($_GET["order"])) ? true : false;
    $pesquisa = !empty($_REQUEST["pesquisa"]) ? $_REQUEST["pesquisa"] : NULL;
    $criterio = !empty($_REQUEST["criterio"]) ? $_REQUEST["criterio"] : NULL;
    
    ### Funções ###
    if(isset($_GET["id"])){
        $_SESSION[SITE_NAME]["beneficiario"] = $beneficiario->Selecionar($_GET["id"]);
        header("Location: " . SRC);
    }elseif(isset($_GET["deletar"])){
        $tela = $beneficiario->Deletar($_GET["deletar"]);
        header("Location: " . $tela);
    }

    $beneficiario->Ver($order, $desc, $index, $pesquisa, $criterio);
    $main = $beneficiario->getDados();

    $main["index"]        = (isset($_GET["i"])) ? "&i=" . $_GET["i"] : '';
    $main["criterio"]     = (isset($_REQUEST["criterio"])) ? "&criterio=" . $_REQUEST["criterio"] : '';
    $main["pesquisa"]     = (isset($_REQUEST["pesquisa"])) ? "&pesquisa=" . $_REQUEST["pesquisa"] : '';
    $main['SRC']            = SRC;
    $main['IMG']            = IMG;
    
    $page = 'beneficiario/selecionar';
    
}else if(LAST_G == "home"){

    if(isset($_POST['criar'])){
        if(empty($_POST["bxf"])){
            $_POST["bxf"] = NULL;
        }
        $beneficiario->Criar($_POST["beneficiario"], $_POST["bxf"]);
        if(isset($_POST["troca_permuta"])){
            $beneficiario->troca_permuta = "checked";
        }
        $_SESSION[SITE_NAME]["beneficiario"] = $beneficiario->getDados();
        $beneficiario->act = "alterar";
        $beneficiario->nome_act = "Alterar";
        $beneficiario->setMensagem("Beneficiário cadastrado com sucesso!", SRC);
        
    }elseif(isset($_POST['alterar'])){
        if(empty($_POST["bxf"])){
            $_POST["bxf"] = NULL;
        }
        $beneficiario->Alterar($_POST["beneficiario"], $_POST["bxf"]);
        if(isset($_POST["troca_permuta"])){
            $beneficiario->troca_permuta = "checked";
        }
        $_SESSION[SITE_NAME]["beneficiario"] = $beneficiario->getDados();
        $beneficiario->act = "alterar";
        $beneficiario->nome_act = "Alterar";
        $beneficiario->setMensagem("Beneficiário alterado com sucesso!", SRC);
        
    }elseif(!empty($_SESSION[SITE_NAME]["beneficiario"])){
        $beneficiario->setDados($_SESSION[SITE_NAME]["beneficiario"]);
        $beneficiario->act = "alterar";
        $beneficiario->nome_act = "Alterar";
        $beneficiario->setMensagem();
        
    }else{
        $beneficiario->setEmpty();
        $beneficiario->act = "criar";
        $beneficiario->nome_act = "Criar";
        
    }
    
    $beneficiario->setSelect("estado_local");
    $beneficiario->setSelect("nucleo");
    $beneficiario->setOption("estado_civil");
    $beneficiario->setOption("escolaridade");
    $beneficiario->setOption("form_adq_parcela");
    $beneficiario->setOption("motiv_ausencia", '');
    $beneficiario->setOption("tipo_construcao");
    $main = $beneficiario->getDados();

}
$main['SRC']            = SRC;
$main['IMG']            = IMG;
$GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView($page, $main);

