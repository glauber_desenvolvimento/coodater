<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    
    $agrobiodiversidade = new Agrobiodiversidade($GLOBALS['D'], $GLOBALS['V']);

    $GLOBALS['D']->Output("SELECT * FROM agrobiodiversidade WHERE id_beneficiario = {$_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]}", $result, $rows, true);

    if($rows == 1){
        if(isset($_POST["alterar"])){
            $agrobiodiversidade->Alterar($_POST["agrobiodiversidade"]);
            $main = $agrobiodiversidade->getDados();
        }else{
            $main = $agrobiodiversidade->getDados($result);
        }
        $main["action"] = "alterar";
        $main["value"] = "Alterar";
    }else{
        if(isset($_POST["criar"])){
            $agrobiodiversidade->Criar($_POST["agrobiodiversidade"]);
            $main = $agrobiodiversidade->getDados();
        }else{
            $agrobiodiversidade->setEmpty($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
            $main = $agrobiodiversidade->getDados();
        }
        $main["action"] = "criar";
        $main["value"]  = "Criar";
    }
    
    $main['SRC']              = SRC;
    $main['IMG']              = IMG;

    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("agrobiodiversidade", $main);
    
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}