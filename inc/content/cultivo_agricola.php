<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    $composicao = new CultFamiliar($GLOBALS['D']);

    if(isset($_POST['cult'])){
        $composicao->Criar($_POST["cult_agricola"]);
        header("Location: " . SRC . '/cultivo_agricola');
    }elseif(isset($_POST['alterar']) && !empty($_GET['editar'])){
        $composicao->Alterar($_POST["cult_agricola"], $_GET['editar']);
        header("Location: " . SRC . '/cultivo_agricola');
    }elseif(!empty($_GET['deletar'])){
        $composicao->Deletar($_GET['deletar']);
        header("Location: " . SRC . '/cultivo_agricola');
    }elseif(!empty($_GET['editar'])){
        $composicao->Editar($_GET['editar']);
        $main = $composicao->getDados();
    }else{
        $main["id_beneficiario"] = $composicao->getId($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
        $composicao->setEmpty();
        $main = $composicao->getDados();
    }
    $main["action"] = (!empty($_GET["editar"])) ? "alterar" : "cult";
    $main["linhas"]         = $composicao->Ver($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
    $main['SRC']            = SRC;
    $main['IMG']            = IMG;
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("cult_agricola", $main);
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}



