<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    
    $destinacao_do_esgoto = new DestinacaoDoEsgoto($GLOBALS['D']);

    $GLOBALS['D']->Output("SELECT * FROM destinacao_do_esgoto WHERE id_beneficiario = {$_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]}", $result, $rows, true);

    if($rows > 0){
        if(isset($_POST["alterar"])){
            
            $destinacao_do_esgoto->Alterar($_POST["destinacao_do_esgoto"]);
            $main = $destinacao_do_esgoto->getDados($_POST["destinacao_do_esgoto"]);
            $main["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     Alterado com sucesso.
                 </div>
                 <script>
                     $(function(){
                         window.setTimeout(
                             function(){
                                 window.open('" . SRC . "destinacao_do_esgoto', '_self');
                             }, 1000
                         );
                     });
                </script>";
            unset($main["id_destinacao_do_esgoto_aux"]);
        }else{
            
            $main = $destinacao_do_esgoto->getDados($result);
            $main["mensagem"] = "<br>";
            
        }
        $main["action"] = "alterar";
        $main["value"] = "Alterar";
    }else{
        if(isset($_POST["criar"])){
            
            $destinacao_do_esgoto->Criar($_POST["destinacao_do_esgoto"]);
            $main = $destinacao_do_esgoto->getDados($_POST["destinacao_do_esgoto"]);
            unset($main["id_destinacao_do_esgoto_aux"]);
            
            if(empty($_POST["destinacao_do_esgoto"]["id_destinacao_do_esgoto_aux"])){
                $main["mensagem"] = 
                    "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                         Selecione uma opção.
                     </div>";
            }else{
                $main["mensagem"] = 
                    "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                         Criado com sucesso.
                     </div>
                     <script>
                     $(function(){
                         window.setTimeout(
                             function(){
                                 window.open('" . SRC . "destinacao_do_esgoto', '_self');
                             }, 1000
                         );
                     });
                    </script>";
            }
        }else{
            $destinacao_do_esgoto->setEmpty($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
            $main = $destinacao_do_esgoto->getDados();
        }
        $main["action"] = "criar";
        $main["value"] = "Criar";
    }
    
    $main['SRC']              = SRC;
    $main['IMG']              = IMG;

    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("destinacao_do_esgoto", $main);
    
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}