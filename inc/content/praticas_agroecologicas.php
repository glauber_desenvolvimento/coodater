<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    
    $praticas_agroecologicas = new PraticasAgroecologicas($GLOBALS['D']);

    $GLOBALS['D']->Output("SELECT * FROM praticas_agroecologicas WHERE id_beneficiario = {$_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]}", $result, $rows, true);

    if($rows > 0){
        if(isset($_POST["alterar"])){
            
            $praticas_agroecologicas->Alterar($_POST["praticas_agroecologicas"]);
            $main = $praticas_agroecologicas->getDados($_POST["praticas_agroecologicas"]);
            $main["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     Alterado com sucesso.
                 </div>
                 <script>
                     $(function(){
                         window.setTimeout(
                             function(){
                                 window.open('" . SRC . "praticas_agroecologicas', '_self');
                             }, 1000
                         );
                     });
                </script>";
            unset($main["id_praticas_agroecologicas_aux"]);
        }else{
            
            $main = $praticas_agroecologicas->getDados($result);
            $main["mensagem"] = "<br>";
            
        }
        $main["action"] = "alterar";
        $main["value"] = "Alterar";
    }else{
        if(isset($_POST["criar"])){
            
            $praticas_agroecologicas->Criar($_POST["praticas_agroecologicas"]);
            $main = $praticas_agroecologicas->getDados($_POST["praticas_agroecologicas"]);
            unset($main["id_praticas_agroecologicas_aux"]);
            
            if(empty($_POST["praticas_agroecologicas"]["id_praticas_agroecologicas_aux"])){
                $main["mensagem"] = 
                    "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                         Selecione uma opção.
                     </div>";
            }else{
                $main["mensagem"] = 
                    "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                         Criado com sucesso.
                     </div>
                     <script>
                     $(function(){
                         window.setTimeout(
                             function(){
                                 window.open('" . SRC . "praticas_agroecologicas', '_self');
                             }, 1000
                         );
                     });
                    </script>";
            }
        }else{
            $praticas_agroecologicas->setEmpty($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
            $main = $praticas_agroecologicas->getDados();
        }
        $main["action"] = "criar";
        $main["value"] = "Criar";
    }
    
    $main['SRC']              = SRC;
    $main['IMG']              = IMG;

    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("praticas_agroecologicas", $main);
    
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}