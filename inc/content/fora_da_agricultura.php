<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    $fda = new RendaFDA($GLOBALS['D']);

    if(isset($_POST['fda'])){
        $fda->Criar($_POST["renda_fda"]);
        header("Location: " . SRC . '/fora_da_agricultura');
    }elseif(isset($_POST['alterar']) && !empty($_GET['editar'])){
        $fda->Alterar($_POST["renda_fda"], $_GET['editar']);
        header("Location: " . SRC . '/fora_da_agricultura');
    }elseif(!empty($_GET['deletar'])){
        $fda->Deletar($_GET['deletar']);
        header("Location: " . SRC . '/fora_da_agricultura');
    }elseif(!empty($_GET['editar'])){
        $fda->Editar($_GET['editar']);
        $main = $fda->getDados();
    }else{
        $main["id_beneficiario"] = $fda->getId($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
        $fda->setEmpty();
        $main = $fda->getDados();
    }
    $main["action"] = (!empty($_GET["editar"])) ? "alterar" : "fda";
    $main["linhas"]         = $fda->Ver($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
    $main['SRC']            = SRC;
    $main['IMG']            = IMG;
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("fora_da_agricultura", $main);
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}



