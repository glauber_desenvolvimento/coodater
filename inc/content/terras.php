<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    $terra = new Terra($GLOBALS['D']);

    if(isset($_POST['salvar'])){
        $terra->Criar($_POST["terra"]);
        header("Location: " . SRC . 'terras');
    }elseif(isset($_POST['alterar']) && !empty($_GET['editar'])){
        $terra->Alterar($_POST["terra"], $_GET['editar']);
        header("Location: " . SRC . 'terras');
    }elseif(!empty($_GET['deletar'])){
        $terra->Deletar($_GET['deletar']);
        header("Location: " . SRC . 'terras');
    }elseif(!empty($_GET['editar'])){
        $terra->Editar($_GET['editar']);
        $main = $terra->getDados();
    }else{
        $main["id_beneficiario"] = $terra->getId($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
        $terra->setEmpty();
        $main = $terra->getDados();
    }
    $main["action"] = (!empty($_GET["editar"])) ? "alterar" : "salvar";
    $main["linhas"]         = $terra->Ver($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
    $main['SRC']            = SRC;
    $main['IMG']            = IMG;
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("terras", $main);
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}



