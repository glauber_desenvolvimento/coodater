<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    $fdp = new RendaFDP($GLOBALS['D']);

    if(isset($_POST['fdp'])){
        $fdp->Criar($_POST["renda_fdp"]);
        header("Location: " . SRC . '/fora_da_propriedade');
    }elseif(isset($_POST['alterar']) && !empty($_GET['editar'])){
        $fdp->Alterar($_POST["renda_fdp"], $_GET['editar']);
        header("Location: " . SRC . '/fora_da_propriedade');
    }elseif(!empty($_GET['deletar'])){
        $fdp->Deletar($_GET['deletar']);
        header("Location: " . SRC . '/fora_da_propriedade');
    }elseif(!empty($_GET['editar'])){
        $fdp->Editar($_GET['editar']);
        $main = $fdp->getDados();
    }else{
        $main["id_beneficiario"] = $fdp->getId($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
        $fdp->setEmpty();
        $main = $fdp->getDados();
    }
    $main["action"] = (!empty($_GET["editar"])) ? "alterar" : "fdp";
    $main["linhas"]         = $fdp->Ver($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
    $main['SRC']            = SRC;
    $main['IMG']            = IMG;
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("fora_da_propriedade", $main);
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}



