<?php
#### Preparando a View ####
if(isset($_POST["enviar"]) && !isset($_SESSION["bloqueio"])){
    
    $GLOBALS["D"]->Output("SELECT id_usuario, nome_usuario, login_usuario, funcao_usuario FROM usuario WHERE login_usuario = '{$_POST["login_usuario"]}' AND senha_usuario = '" . md5($_POST["senha_usuario"]) . "' AND status_usuario = 1 LIMIT 1", $result, $rows, true);

    if($rows == 1){
        $_SESSION[SITE_NAME]["id_usuario"]     = $result["id_usuario"];
        $_SESSION[SITE_NAME]["nome_usuario"]   = $result["nome_usuario"];
        $_SESSION[SITE_NAME]["login_usuario"]  = $result["login_usuario"];
        $_SESSION[SITE_NAME]["funcao_usuario"] = $result["funcao_usuario"];
        
        $insert = new Insert("log", 
                    array(
                        "data_log" => date("Y-m-d H:m:s"),
                        "id_usuario" => $result["id_usuario"] 
                    )
                );
        
        $GLOBALS["D"]->InputBy($insert);

        unset($_SESSION["contador"]);
        header("Location: " . SRC . "home");
    }elseif($rows == 0){
        if(!isset($_SESSION["contador"])){
            $_SESSION["contador"] = 1;
        }else{
            $_SESSION["contador"]++;
            if($_SESSION["contador"] == 10){
                unset($_SESSION["contador"]);
                $_SESSION["bloqueio"] = true;
                session_cache_expire(5);
            }
        }
        $login["tela"] = "Login inválido";
    }
}else{
    $login["tela"] = "";
}
$login["IMG"] = IMG;
#### Head do HTML ####
$GLOBALS['tpl']['login']     = $GLOBALS['V']->prepareView("login", $login);
