<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    
    $protecao_ambiental = new ProtecaoAmbiental($GLOBALS['D'], $GLOBALS['V']);

    $GLOBALS['D']->Output("SELECT * FROM protecao_ambiental WHERE id_beneficiario = {$_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]}", $result, $rows, true);

    if($rows == 1){
        if(isset($_POST["alterar"])){
            $protecao_ambiental->Alterar($_POST["protecao_ambiental"]);
            $main = $protecao_ambiental->getDados();
        }else{
            $main = $protecao_ambiental->getDados($result);
        }
        $main["action"] = "alterar";
        $main["value"] = "Alterar";
    }else{
        if(isset($_POST["criar"])){
            $protecao_ambiental->Criar($_POST["protecao_ambiental"]);
            $main = $protecao_ambiental->getDados();
        }else{
            $protecao_ambiental->setEmpty($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
            $main = $protecao_ambiental->getDados();
        }
        $main["action"] = "criar";
        $main["value"]  = "Criar";
    }
    
    $main['SRC']              = SRC;
    $main['IMG']              = IMG;

    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("protecao_ambiental", $main);
    
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}