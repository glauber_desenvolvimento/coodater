<?php
#### Preparando a View ####

$usuario = new Usuario($GLOBALS['D'], $GLOBALS['V']);
$page = "usuario";

if($_SESSION[SITE_NAME]["funcao_usuario"]){

    if(isset($_GET["selecionar"])){

        if(!empty($_GET["deletar"])){
            $usuario->Deletar($_GET["deletar"]);
        }

        $index = isset($_GET["i"]) ? $_GET["i"] : NULL ;
        $order = isset($_GET["order"]) ? $_GET["order"] : "id_usuario";
        $desc = (isset($_GET["desc"]) && isset($_GET["order"])) ? true : false;
        $pesquisa = !empty($_REQUEST["pesquisa"]) ? $_REQUEST["pesquisa"] : NULL;
        $criterio = !empty($_REQUEST["criterio"]) ? $_REQUEST["criterio"] : NULL;
        $usuario->Ver($order, $desc, $index, $pesquisa, $criterio);
        $main = $usuario->getDados();
        
        $main["index"]        = (isset($_GET["i"])) ? "&i=" . $_GET["i"] : '';
        $main["criterio"]     = (isset($_REQUEST["criterio"])) ? "&criterio=" . $_REQUEST["criterio"] : '';
        $main["pesquisa"]     = (isset($_REQUEST["pesquisa"])) ? "&pesquisa=" . $_REQUEST["pesquisa"] : '';

        $page = "selecionar";

    }else if(isset($_POST["criar"])){

        $usuario->Criar($_POST["usuario"]);
        $main = $usuario->getDados(true);
        $main["readonly"] = "readonly";

    }else if(isset($_POST["alterar"]) && isset($_GET["id"])){

        $usuario->Alterar($_POST["usuario"], $_GET["id"]);
        $main = $usuario->getDados(true);

    }else if(isset($_GET["id"])){

        $usuario->GetById($_GET["id"]);
        $main = $usuario->getDados();

    }else {

        $usuario->setEmpty();
        $main = $usuario->getDados();

    }

    $main['SRC']        = SRC;
    $main['IMG']        = IMG;

    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("usuario/" . $page, $main);



}else{
    
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("usuario/acesso_restrito");
    
}

