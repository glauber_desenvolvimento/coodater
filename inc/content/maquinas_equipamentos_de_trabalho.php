<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    $mq_eq_tr = new MQ_EQ_TR($GLOBALS['D']);

    if(isset($_POST['mq_eq_trs'])){
        $mq_eq_tr->Criar($_POST["mq_eq_tr"]);
        header("Location: " . SRC . '/maquinas_equipamentos_de_trabalho');
    }elseif(isset($_POST['alterar']) && !empty($_GET['editar'])){
        $mq_eq_tr->Alterar($_POST["mq_eq_tr"], $_GET['editar']);
        header("Location: " . SRC . '/maquinas_equipamentos_de_trabalho');
    }elseif(!empty($_GET['deletar'])){
        $mq_eq_tr->Deletar($_GET['deletar']);
        header("Location: " . SRC . '/maquinas_equipamentos_de_trabalho');
    }elseif(!empty($_GET['editar'])){
        $mq_eq_tr->Editar($_GET['editar']);
        $main = $mq_eq_tr->getDados();
    }else{
        $mq_eq_tr->setEmpty();
        $main = $mq_eq_tr->getDados();
    }
    $main["action"] = (!empty($_GET["editar"])) ? "alterar" : "mq_eq_trs";
    $main["linhas"]         = $mq_eq_tr->Ver($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
    $main['SRC']            = SRC;
    $main['IMG']            = IMG;
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("maquinas_equipamentos_de_trabalho", $main);
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}