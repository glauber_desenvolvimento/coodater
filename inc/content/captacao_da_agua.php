<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    
    $captacao_da_agua = new CaptacaoDaAgua($GLOBALS['D']);

    $GLOBALS['D']->Output("SELECT * FROM captacao_da_agua WHERE id_beneficiario = {$_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]}", $result, $rows, true);

    if($rows > 0){
        if(isset($_POST["alterar"])){
            
            $captacao_da_agua->Alterar($_POST["captacao_da_agua"]);
            $main = $captacao_da_agua->getDados($_POST["captacao_da_agua"]);
            $main["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     Alterado com sucesso.
                 </div>
                 <script>
                     $(function(){
                         window.setTimeout(
                             function(){
                                 window.open('" . SRC . "captacao_da_agua', '_self');
                             }, 1000
                         );
                     });
                </script>";
            unset($main["id_captacao_da_agua_aux"]);
        }else{
            
            $main = $captacao_da_agua->getDados($result);
            $main["mensagem"] = "<br>";
            
        }
        $main["action"] = "alterar";
        $main["value"] = "Alterar";
    }else{
        if(isset($_POST["criar"])){
            
            $captacao_da_agua->Criar($_POST["captacao_da_agua"]);
            $main = $captacao_da_agua->getDados($_POST["captacao_da_agua"]);
            unset($main["id_captacao_da_agua_aux"]);
            
            if(empty($_POST["captacao_da_agua"]["id_captacao_da_agua_aux"])){
                $main["mensagem"] = 
                    "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                         Selecione uma opção.
                     </div>";
            }else{
                $main["mensagem"] = 
                    "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                         Criado com sucesso.
                     </div>
                     <script>
                     $(function(){
                         window.setTimeout(
                             function(){
                                 window.open('" . SRC . "captacao_da_agua', '_self');
                             }, 1000
                         );
                     });
                    </script>";
            }
        }else{
            $captacao_da_agua->setEmpty($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
            $main = $captacao_da_agua->getDados();
        }
        $main["action"] = "criar";
        $main["value"] = "Criar";
    }
    
    $main['SRC']              = SRC;
    $main['IMG']              = IMG;

    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("captacao_da_agua", $main);
    
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}