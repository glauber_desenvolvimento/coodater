<?php
#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    
    $politicas_publicas = new PoliticasPublicas($GLOBALS['D']);

    $GLOBALS['D']->Output("SELECT * FROM politicas_publicas WHERE id_beneficiario = {$_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]}", $result, $rows, true);

    
    if($rows == 1){
        if(isset($_POST["alterar"])){
            
            $politicas_publicas->Alterar($_POST["politicas_publicas"], $_POST["tipo_politicas_publicas"]);
            $main = $politicas_publicas->getDados($_POST["politicas_publicas"]);
            $main["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     Criado com sucesso.
                 </div>";
        }else{
            
            $main = $politicas_publicas->getDados($result);
            $main["mensagem"] = "<br>";
            
        }
        $main["action"] = "alterar";
        $main["value"] = "Alterar";
    }else{
        if(isset($_POST["criar"])){
            
            $politicas_publicas->Criar($_POST["politicas_publicas"], $_POST["tipo_politicas_publicas"]);
            $main = $politicas_publicas->getDados($_POST["politicas_publicas"]);
            
            $main["mensagem"] = 
                "<div class='bx-radius p-b bg-white m-both-b tx-green'>
                     Criado com sucesso.
                 </div>
                 <script>
                 $(function(){
                     window.setTimeout(
                         function(){
                             window.open('" . SRC . "acesso_a_politicas_publicas', '_self');
                         }, 1000
                     );
                 });
             </script>";

        }else{
            $politicas_publicas->setEmpty($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
            $main = $politicas_publicas->getDados();
        }
        $main["action"] = "criar";
        $main["value"] = "Criar";
    }
    
    $main['SRC']              = SRC;
    $main['IMG']              = IMG;

    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("acesso_a_politicas_publicas", $main);
    
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}