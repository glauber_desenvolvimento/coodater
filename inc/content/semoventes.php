<?php

#### Preparando a View ####
if(!empty($_SESSION[SITE_NAME]["beneficiario"])){
    $semovente = new Semovente($GLOBALS['D']);

    if(isset($_POST['semoventes'])){
        $semovente->Criar($_POST["semovente"]);
        header("Location: " . SRC . '/semoventes');
    }elseif(isset($_POST['alterar']) && !empty($_GET['editar'])){
        $semovente->Alterar($_POST["semovente"], $_GET['editar']);
        header("Location: " . SRC . '/semoventes');
    }elseif(!empty($_GET['deletar'])){
        $semovente->Deletar($_GET['deletar']);
        header("Location: " . SRC . '/semoventes');
    }elseif(!empty($_GET['editar'])){
        $semovente->Editar($_GET['editar']);
        $main = $semovente->getDados();
    }else{
        $semovente->setEmpty();
        $main = $semovente->getDados();
    }
    $main["action"] = (!empty($_GET["editar"])) ? "alterar" : "semoventes";
    $main["linhas"]         = $semovente->Ver($_SESSION[SITE_NAME]["beneficiario"]["id_beneficiario"]);
    $main['SRC']            = SRC;
    $main['IMG']            = IMG;
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("semoventes", $main);
}else{
    $GLOBALS["tpl"]["main"] = $GLOBALS['V']->prepareView("crie_um_beneficiario");
}