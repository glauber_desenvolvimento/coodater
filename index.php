<?php
ob_start();

require("adm/fnc/autoload.php");
require("adm/fnc/FrontEndProject.php");


$GLOBALS['V']->Import("header", "head");
$GLOBALS['V']->Import("header", "header");

if(LAST_G == "exit"){
    unset($_SESSION[SITE_NAME]);
    header("Location: " . SRC);
    
}elseif(isset($_SESSION[SITE_NAME])){

    $GLOBALS['V']->Import("aside", "aside");
    $GLOBALS['V']->Import("content", LAST_G);
    
    $page = "main";
    $GLOBALS['V']->Import("modal", "modal");

}else{
    
    $GLOBALS['V']->Import("content", "login");
    $page = "login";
    

}

$GLOBALS['V']->Import("footer", "footer");
$GLOBALS['V']->ShowView(
    $page,
    $GLOBALS["tpl"], 
    true);

ob_flush();
