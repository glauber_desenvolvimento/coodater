
$(function(){
    $(".divider:not(:empty)").wrapInner("<div></div>").prepend("<hr>");
    $(".divider:empty").prepend("<hr>&nbsp;");
    $(".label").before("<br>");
    $("p + p").before("<br>");
    $("button").click(function() {
        return false;
    });

    $(".summary")
        .prepend("&#9658; ")
        .click(function () {
            var title = $(this).attr("title");
            var prop = $(this).prop("data-open");
            var alvo = ".summary[title='" + title + "']";
            if($(this).attr("data-close") == ''){
                $(".summary").prop("data-open", "");
                $(".summary")
                    .html(
                        function(){
                            var title = $(this).attr("title");
                            return "&#9658; " + title;
                        }
                );
                $(".summary + *").slideUp(); 
            }
            if(prop == "data-open"){
                $(this).prop("data-open", "");
                $(this).html("&#9658; " + title);
                $(alvo + " + *").slideUp();
                $(".details + *[title='" + title + "']").css("visibility", "visible");
            }else{
                $(this).prop("data-open", "data-open");
                $(this).html("&#9660; " + title);
                $(alvo + " + *").slideDown();
                $(".details + *[title='" + title + "']").css("visibility", "hidden");
            }
        });
    $(".summary + *").hide(); 
    
    $("*[data-open]").prop("data-open", "data-open").html(function(){
        var title = $(this).attr("title");
        return "&#9660; " + title;
    });
    $("*[data-open] + *").show();
    
    
    //Módulos de template
    $(".j_module").wrapInner("<main></main>");

    //Classe Scroll Up
    $('.j_scroll').click(function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: $(this.hash).offset().top}, 2000);
    });

    //Botão Subir
    $('#j_upper').click(function(e) {
        e.preventDefault();
        $('html, body').animate(
            {scrollTop: $(this.hash).offset().top},
            2000, 
            'swing',
            function() {
                $('#j_upper').fadeOut();
            }
        );
    }).css('z-index', '9').hide();
    $(window).resize(function(){
        if($(window).width() < 1024){
            $('#j_upper').hide();
        }else{
            $('#j_upper').show();
        }
    });
    $(window).scroll(function() {
        if($(window).width() > 1024){
            if ($(this).scrollTop() > 300) {
                $('#j_upper').fadeIn();
            } else {
                $('#j_upper').fadeOut();
            }
        }
    });
    
    //Péliculas de modal
    $("#j_overlay").hide();
    $(window).keyup(function(e){
        if(e.keyCode == 27){
            $('#j_overlay').fadeOut();
        }
    });
    $("#j_close")
        .click(function(){
            $('#j_overlay').fadeOut();
        })
        .attr("href", "javascript:;");
    $("#j_open")
        .click(function(){
            $('#j_overlay').fadeIn();
        })
        .attr("href", "javascript:;");
    
    //Links com método Post
    $(".j_post").click(function() {
        $('#j_post input').attr('value', $(this).attr('rel'));
        if($(this).attr('name')){
            $('#j_post input').attr('name', $(this).attr('name'));
        }else{
            $('#j_post input').attr('name', 'P');
        }
        $('#j_post').submit();
    }).attr("href", "javascript:;");

    //Transformar imagens em links
    $('.j_link').click(function() {
        var url = $(this).attr('title');
        window.open(url, '_self');
    }).css({cursor: 'pointer'});
    
    $('.j_link_b').click(function() {
        var url = $(this).attr('title');
        window.open(url, '_blank');
    }).css({cursor: 'pointer'});

    //Alteração de tamanho das letras
    $("#j_small_lether").click(function() {
        $('.j_module').css('font-size', '0.8em');
    });
    $("#j_medium_lether").click(function() {
        $('.j_module').css('font-size', '1em');
    });
    $("#j_big_lether").click(function() {
        $('.j_module').css('font-size', '1.2em');
    });
    
    $(".j_break_b").before("<br>");
    $(".j_break_a").after("<br>");
    $(".j_break_ab").before("<br>").after("<br>");
    $(".j_breaker_b").before("<br><br>");
    $(".j_breaker_a").after("<br><br>");
    $(".j_breaker_ab").before("<br><br>").after("<br><br>");

    $('#exit').click(function() {
        var url = $(this).attr('id');
        window.open(url, '_self');
    }).css({cursor: 'pointer'});
});

function j_loading(status) {
    if (status == 1)
        $('#j_loading').fadeIn();
    else
        $('#j_loading').hide();
}
$(window).load(function(){
    j_loading(0);
});