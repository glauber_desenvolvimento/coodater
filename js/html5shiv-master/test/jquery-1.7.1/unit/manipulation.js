module("manipulation", { teardown: moduleTeardown });

// Ensure that an extended Array prototype doesn't break jQuery
Array.prototype.arrayProtoFn = function(arg) { throw("arrayProtoFn should not be called"); };

var bareObj = function(value) { return value; };
var functionReturningObj = function(value) { return (function() { return value; }); };

test("text()", function() {
	expect(3);
	var expected = "This link has class=\"blog\": Simon Willison's Weblog";
	equal( jQuery("#sap").text(), expected, "Check for merged text of more then one element." );

	// Check serialization of text values
	equal( jQuery(document.createTextNode("foo")).text(), "foo", "Text node was retreived from .text()." );
	notEqual( jQuery(document).text(), "", "Retrieving text for the document retrieves all text (#10724).");
});

var testText = function(valueObj) {
	expect(4);
	var val = valueObj("<div><b>Hello</b> cruel world!</div>");
	equal( jQuery("#foo").text(val)[0].innerHTML.replace(/>/g, "&gt;"), "&lt;div&gt;&lt;b&gt;Hello&lt;/b&gt; cruel world!&lt;/div&gt;", "Check escaped text" );

	// using contents will get comments regular, text, and comment nodes
	var j = jQuery("#nonnodes").contents();
	j.text(valueObj("hi!"));
	equal( jQuery(j[0]).text(), "hi!", "Check node,textnode,comment with text()" );
	equal( j[1].nodeValue, " there ", "Check node,textnode,comment with text()" );

	// Blackberry 4.6 doesn't maintain comments in the DOM
	equal( jQuery("#nonnodes")[0].childNodes.length < 3 ? 8 : j[2].nodeType, 8, "Check node,textnode,comment with text()" );
};

test("text(String)", function() {
	testText(bareObj);
});

test("text(Function)", function() {
	testText(functionReturningObj);
});

test("text(Function) with incoming value", function() {
	expect(2);

	var old = "This link has class=\"blog\": Simon Willison's Weblog";

	jQuery("#sap").text(function(i, val) {
		equal( val, old, "Make sure the incoming value is correct." );
		return "foobar";
	});

	equal( jQuery("#sap").text(), "foobar", "Check for merged text of more then one element." );

	QUnit.reset();
});

var testWrap = function(val) {
	expect(19);
	var defaultText = "Try them out:";
	var result = jQuery("#first").wrap(val( "<div class='red'><span></span></div>" )).text();
	equal( defaultText, result, "Check for wrapping of on-the-fly html" );
	ok( jQuery("#first").parent().parent().is(".red"), "Check if wrapper has class 'red'" );

	QUnit.reset();
	result = jQuery("#first").wrap(val( document.getElementById("empty") )).parent();
	ok( result.is("ol"), "Check for element wrapping" );
	equal( result.text(), defaultText, "Check for element wrapping" );

	QUnit.reset();
	jQuery("#check1").click(function() {
		var checkbox = this;
		ok( checkbox.checked, "Checkbox's state is erased after wrap() action, see #769" );
		jQuery(checkbox).wrap(val( "<div id='c1' style='display:none;'></div>" ));
		ok( checkbox.checked, "Checkbox's state is erased after wrap() action, see #769" );
	}).click();

	// using contents will get comments regular, text, and comment nodes
	var j = jQuery("#nonnodes").contents();
	j.wrap(val( "<i></i>" ));

	// Blackberry 4.6 doesn't maintain comments in the DOM
	equal( jQuery("#nonnodes > i").length, jQuery("#nonnodes")[0].childNodes.length, "Check node,textnode,comment wraps ok" );
	equal( jQuery("#nonnodes > i").text(), j.text(), "Check node,textnode,comment wraps doesn't hurt text" );

	// Try wrapping a disconnected node
	var cacheLength = 0;
	for (var i in jQuery.cache) {
		cacheLength++;
	}

	j = jQuery("<label/>").wrap(val( "<li/>" ));
	equal( j[0].nodeName.toUpperCase(), "LABEL", "Element is a label" );
	equal( j[0].parentNode.nodeName.toUpperCase(), "LI", "Element has been wrapped" );

	for (i in jQuery.cache) {
		cacheLength--;
	}
	equal(cacheLength, 0, "No memory leak in jQuery.cache (bug #7165)");

	// Wrap an element containing a text node
	j = jQuery("<span/>").wrap("<div>test</div>");
	equal( j[0].previousSibling.nodeType, 3, "Make sure the previous node is a text element" );
	equal( j[0].parentNode.nodeName.toUpperCase(), "DIV", "And that we're in the div element." );

	// Try to wrap an element with multiple elements (should fail)
	j = jQuery("<div><span></span></div>").children().wrap("<p></p><div></div>");
	equal( j[0].parentNode.parentNode.childNodes.length, 1, "There should only be one element wrapping." );
	equal( j.length, 1, "There should only be one element (no cloning)." );
	equal( j[0].parentNode.nodeName.toUpperCase(), "P", "The span should be in the paragraph." );

	// Wrap an element with a jQuery set
	j = jQuery("<span/>").wrap(jQuery("<div></div>"));
	equal( j[0].parentNode.nodeName.toLowerCase(), "div", "Wrapping works." );

	// Wrap an element with a jQuery set and event
	result = jQuery("<div></div>").click(function(){
		ok(true, "Event triggered.");

		// Remove handlers on detached elements
		result.unbind();
		jQuery(this).unbind();
	});

	j = jQuery("<span/>").wrap(result);
	equal( j[0].parentNode.nodeName.toLowerCase(), "div", "Wrapping works." );

	j.parent().trigger("click");

	// clean up attached elements
	QUnit.reset();
}

test("wrap(String|Element)", function() {
	testWrap(bareObj);
});

test("wrap(Function)", function() {
	testWrap(functionReturningObj);
});

test("wrap(Function) with index (#10177)", function() {
	var expectedIndex = 0,
	    targets = jQuery("#qunit-fixture p");

	expect(targets.length);
	targets.wrap(function(i) {
		equal( i, expectedIndex, "Check if the provided index (" + i + ") is as expected (" + expectedIndex + ")" );
		expectedIndex++;

		return "<div id='wrap_index_'" + i + "'></div>";
	});
});

test("wrap(String) consecutive elements (#10177)", function() {
	var targets = jQuery("#qunit-fixture p");

	expect(targets.length * 2);
	targets.wrap("<div class='wrapper'></div>");
	
	targets.each(function() {
		var $this = jQuery(this);
		
		ok( $this.parent().is('.wrapper'), "Check each elements parent is correct (.wrapper)" );
		equal( $this.siblings().length, 0, "Each element should be wrapped individually" );
	});
});

var testWrapAll = function(val) {
	expect(8);
	var prev = jQuery("#firstp")[0].previousSibling;
	var p = jQuery("#firstp,#first")[0].parentNode;

	var result = jQuery("#firstp,#first").wrapAll(val( "<div class='red'><div class='tmp'></div></div>" ));
	equal( result.parent().length, 1, "Check for wrapping of on-the-fly html" );
	ok( jQuery("#first").parent().parent().is(".red"), "Check if wrapper has class 'red'" );
	ok( jQuery("#firstp").parent().parent().is(".red"), "Check if wrapper has class 'red'" );
	equal( jQuery("#first").parent().parent()[0].previousSibling, prev, "Correct Previous Sibling" );
	equal( jQuery("#first").parent().parent()[0].parentNode, p, "Correct Parent" );

	QUnit.reset();
	var prev = jQuery("#firstp")[0].previousSibling;
	var p = jQuery("#first")[0].parentNode;
	var result = jQuery("#firstp,#first").wrapAll(val( document.getElementById("empty") ));
	equal( jQuery("#first").parent()[0], jQuery("#firstp").parent()[0], "Same Parent" );
	equal( jQuery("#first").parent()[0].previousSibling, prev, "Correct Previous Sibling" );
	equal( jQuery("#first").parent()[0].parentNode, p, "Correct Parent" );
}

test("wrapAll(String|Element)", function() {
	testWrapAll(bareObj);
});

var testWrapInner = function(val) {
	expect(11);
	var num = jQuery("#first").children().length;
	var result = jQuery("#first").wrapInner(val("<div class='red'><div id='tmp'></div></div>"));
	equal( jQuery("#first").children().length, 1, "Only one child" );
	ok( jQuery("#first").children().is(".red"), "Verify Right Element" );
	equal( jQuery("#first").children().children().children().length, num, "Verify Elements Intact" );

	QUnit.reset();
	var num = jQuery("#first").html("foo<div>test</div><div>test2</div>").children().length;
	var result = jQuery("#first").wrapInner(val("<div class='red'><div id='tmp'></div></div>"));
	equal( jQuery("#first").children().length, 1, "Only one child" );
	ok( jQuery("#first").children().is(".red"), "Verify Right Element" );
	equal( jQuery("#first").children().children().children().length, num, "Verify Elements Intact" );

	QUnit.reset();
	var num = jQuery("#first").children().length;
	var result = jQuery("#first").wrapInner(val(document.getElementById("empty")));
	equal( jQuery("#first").children().length, 1, "Only one child" );
	ok( jQuery("#first").children().is("#empty"), "Verify Right Element" );
	equal( jQuery("#first").children().children().length, num, "Verify Elements Intact" );

	var div = jQuery("<div/>");
	div.wrapInner(val("<span></span>"));
	equal(div.children().length, 1, "The contents were wrapped.");
	equal(div.children()[0].nodeName.toLowerCase(), "span", "A span was inserted.");
}

test("wrapInner(String|Element)", function() {
	testWrapInner(bareObj);
});

test("wrapInner(Function)", function() {
	testWrapInner(functionReturningObj)
});

test("unwrap()", function() {
	expect(9);

	jQuery("body").append("  <div id='unwrap' style='display: none;'> <div id='unwrap1'> <span class='unwrap'>a</span> <span class='unwrap'>b</span> </div> <div id='unwrap2'> <span class='unwrap'>c</span> <span class='unwrap'>d</span> </div> <div id='unwrap3'> <b><span class='unwrap unwrap3'>e</span></b> <b><span class='unwrap unwrap3'>f</span></b> </div> </div>");

	var abcd = jQuery("#unwrap1 > span, #unwrap2 > span").get(),
		abcdef = jQuery("#unwrap span").get();

	equal( jQuery("#unwrap1 span").add("#unwrap2 span:first").unwrap().length, 3, "make #unwrap1 and #unwrap2 go away" );
	deepEqual( jQuery("#unwrap > span").get(), abcd, "all four spans should still exist" );

	deepEqual( jQuery("#unwrap3 span").unwrap().get(), jQuery("#unwrap3 > span").get(), "make all b in #unwrap3 go away" );

	deepEqual( jQuery("#unwrap3 span").unwrap().get(), jQuery("#unwrap > span.unwrap3").get(), "make #unwrap3 go away" );

	deepEqual( jQuery("#unwrap").children().get(), abcdef, "#unwrap only contains 6 child spans" );

	deepEqual( jQuery("#unwrap > span").unwrap().get(), jQuery("body > span.unwrap").get(), "make the 6 spans become children of body" );

	deepEqual( jQuery("body > span.unwrap").unwrap().get(), jQuery("body > span.unwrap").get(), "can't unwrap children of body" );
	deepEqual( jQuery("body > span.unwrap").unwrap().get(), abcdef, "can't unwrap children of body" );

	deepEqual( jQuery("body > span.unwrap").get(), abcdef, "body contains 6 .unwrap child spans" );

	jQuery("body > span.unwrap").remove();
});

var testAppend = function(valueObj) {
	expect(41);
	var defaultText = "Try them out:"
	var result = jQuery("#first").append(valueObj("<b>buga</b>"));
	equal( result.text(), defaultText + "buga", "Check if text appending works" );
	equal( jQuery("#select3").append(valueObj("<option value='appendTest'>Append Test</option>")).find("option:last-child").attr("value"), "appendTest", "Appending html options to select element");

	QUnit.reset();
	var expected = "This link has class=\"blog\": Simon Willison's WeblogTry them out:";
	jQuery("#sap").append(valueObj(document.getElementById("first")));
	equal( jQuery("#sap").text(), expected, "Check for appending of element" );

	QUnit.reset();
	expected = "This link has class=\"blog\": Simon Willison's WeblogTry them out:Yahoo";
	jQuery("#sap").append(valueObj([document.getElementById("first"), document.getElementById("yahoo")]));
	equal( jQuery("#sap").text(), expected, "Check for appending of array of elements" );

	QUnit.reset();
	expected = "This link has class=\"blog\": Simon Willison's WeblogYahooTry them out:";
	jQuery("#sap").append(valueObj(jQuery("#yahoo, #first")));
	equal( jQuery("#sap").text(), expected, "Check for appending of jQuery object" );

	QUnit.reset();
	jQuery("#sap").append(valueObj( 5 ));
	ok( jQuery("#sap")[0].innerHTML.match( /5$/ ), "Check for appending a number" );

	QUnit.reset();
	jQuery("#sap").append(valueObj( " text with spaces " ));
	ok( jQuery("#sap")[0].innerHTML.match(/ text with spaces $/), "Check for appending text with spaces" );

	QUnit.reset();
	ok( jQuery("#sap").append(valueObj( [] )), "Check for appending an empty array." );
	ok( jQuery("#sap").append(valueObj( "" )), "Check for appending an empty string." );
	ok( jQuery("#sap").append(valueObj( document.getElementsByTagName("foo") )), "Check for appending an empty nodelist." );

	QUnit.reset();
	jQuery("form").append(valueObj("<input name='radiotest' type='radio' checked='checked' />"));
	jQuery("form input[name=radiotest]").each(function(){
		ok( jQuery(this).is(":checked"), "Append checked radio");
	}).remove();

	QUnit.reset();
	jQuery("form").append(valueObj("<input name='radiotest' type='radio' checked    =   'checked' />"));
	jQuery("form input[name=radiotest]").each(function(){
		ok( jQuery(this).is(":checked"), "Append alternately formated checked radio");
	}).remove();

	QUnit.reset();
	jQuery("form").append(valueObj("<input name='radiotest' type='radio' checked />"));
	jQuery("form input[name=radiotest]").each(function(){
		ok( jQuery(this).is(":checked"), "Append HTML5-formated checked radio");
	}).remove();

	QUnit.reset();
	jQuery("#sap").append(valueObj( document.getElementById("form") ));
	equal( jQuery("#sap>form").size(), 1, "Check for appending a form" ); // Bug #910

	QUnit.reset();
	var pass = true;
	try {
		var body = jQuery("#iframe")[0].contentWindow.document.body;

		pass = false;
		jQuery( body ).append(valueObj( "<div>test</div>" ));
		pass = true;
	} catch(e) {}

	ok( pass, "Test for appending a DOM node to the contents of an IFrame" );

	QUnit.reset();
	jQuery("<fieldset/>").appendTo("#form").append(valueObj( "<legend id='legend'>test</legend>" ));
	t( "Append legend", "#legend", ["legend"] );

	QUnit.reset();
	jQuery("#select1").append(valueObj( "<OPTION>Test</OPTION>" ));
	equal( jQuery("#select1 option:last").text(), "Test", "Appending &lt;OPTION&gt; (all caps)" );

	jQuery("#table").append(valueObj( "<colgroup></colgroup>" ));
	ok( jQuery("#table colgroup").length, "Append colgroup" );

	jQuery("#table colgroup").append(valueObj( "<col/>" ));
	ok( jQuery("#table colgroup col").length, "Append col" );

	QUnit.reset();
	jQuery("#table").append(valueObj( "<caption></caption>" ));
	ok( jQuery("#table caption").length, "Append caption" );

	QUnit.reset();
	jQuery("form:last")
		.append(valueObj( "<select id='appendSelect1'></select>" ))
		.append(valueObj( "<select id='appendSelect2'><option>Test</option></select>" ));

	t( "Append Select", "#appendSelect1, #appendSelect2", ["appendSelect1", "appendSelect2"] );

	equal( "Two nodes", jQuery("<div />").append("Two", " nodes").text(), "Appending two text nodes (#4011)" );

	// using contents will get comments regular, text, and comment nodes
	var j = jQuery("#nonnodes").contents();
	var d = jQuery("<div/>").appendTo("#nonnodes").append(j);
	equal( jQuery("#nonnodes").length, 1, "Check node,textnode,comment append moved leaving just the div" );
	ok( d.contents().length >= 2, "Check node,textnode,comment append works" );
	d.contents().appendTo("#nonnodes");
	d.remove();
	ok( jQuery("#nonnodes").contents().length >= 2, "Check node,textnode,comment append cleanup worked" );

	QUnit.reset();
	var $input = jQuery("<input />").attr({ "type": "checkbox", "checked": true }).appendTo('#testForm');
	equal( $input[0].checked, true, "A checked checkbox that is appended stays checked" );

	QUnit.reset();
	var $radios = jQuery("input:radio[name='R1']"),
		$radioNot = jQuery("<input type='radio' name='R1' checked='checked'/>").insertAfter( $radios ),
		$radio = $radios.eq(1).click();
	$radioNot[0].checked = false;
	$radios.parent().wrap("<div></div>");
	equal( $radio[0].checked, true, "Reappending radios uphold which radio is checked" );
	equal( $radioNot[0].checked, false, "Reappending radios uphold not being checked" );
	QUnit.reset();

	var prev = jQuery("#sap").children().length;

	jQuery("#sap").append(
		"<span></span>",
		"<span></span>",
		"<span></span>"
	);

	equal( jQuery("#sap").children().length, prev + 3, "Make sure that multiple arguments works." );
	QUnit.reset();
}

test("append(String|Element|Array&lt;Element&gt;|jQuery)", function() {
	testAppend(bareObj);
});

test("append(Function)", function() {
	testAppend(functionReturningObj);
});

test("append(Function) with incoming value", function() {
	expect(12);

	var defaultText = "Try them out:", old = jQuery("#first").html();

	var result = jQuery("#first").append(function(i, val){
		equal( val, old, "Make sure the incoming value is correct." );
		return "<b>buga</b>";
	});
	equal( result.text(), defaultText + "buga", "Check if text appending works" );

	var select = jQuery("#select3");
	old = select.html();

	equal( select.append(function(i, val){
		equal( val, old, "Make sure the incoming value is correct." );
		return "<option value='appendTest'>Append Test</option>";
	}).find("option:last-child").attr("value"), "appendTest", "Appending html options to select element");

	QUnit.reset();
	var expected = "This link has class=\"blog\": Simon Willison's WeblogTry them out:";
	old = jQuery("#sap").html();

	jQuery("#sap").append(function(i, val){
		equal( val, old, "Make sure the incoming value is correct." );
		return document.getElementById("first");
	});
	equal( jQuery("#sap").text(), expected, "Check for appending of element" );

	QUnit.reset();
	expected = "This link has class=\"blog\": Simon Willison's WeblogTry them out:Yahoo";
	old = jQuery("#sap").html();

	jQuery("#sap").append(function(i, val){
		equal( val, old, "Make sure the incoming value is correct." );
		return [document.getElementById("first"), document.getElementById("yahoo")];
	});
	equal( jQuery("#sap").text(), expected, "Check for appending of array of elements" );

	QUnit.reset();
	expected = "This link has class=\"blog\": Simon Willison's WeblogYahooTry them out:";
	old = jQuery("#sap").html();

	jQuery("#sap").append(function(i, val){
		equal( val, old, "Make sure the incoming value is correct." );
		return jQuery("#yahoo, #first");
	});
	equal( jQuery("#sap").text(), expected, "Check for appending of jQuery object" );

	QUnit.reset();
	old = jQuery("#sap").html();

	jQuery("#sap").append(function(i, val){
		equal( val, old, "Make sure the incoming value is correct." );
		return 5;
	});
	ok( jQuery("#sap")[0].innerHTML.match( /5$/ ), "Check for appending a number" );

	QUnit.reset();
});

test("append the same fragment with events (Bug #6997, 5566)", function () {
	var doExtra = !jQuery.support.noCloneEvent && document.fireEvent;
	expect(2 + (doExtra ? 1 : 0));
	stop();

	var element;

	// This patch modified the way that cloning occurs in IE; we need to make sure that
	// native event handlers on the original object don't get disturbed when they are
	// modified on the clone
	if ( doExtra ) {
		element = jQuery("div:first").click(function () {
			ok(true, "Event exists on original after being unbound on clone");
			jQuery(this).unbind("click");
		});
		var clone = element.clone(true).unbind("click");
		clone[0].fireEvent("onclick");
		element[0].fireEvent("onclick");

		// manually clean up detached elements
		clone.remove();
	}

	element = jQuery("<a class='test6997'></a>").click(function () {
		ok(true, "Append second element events work");
	});

	jQuery("#listWithTabIndex li").append(element)
		.find("a.test6997").eq(1).click();

	element = jQuery("<li class='test6997'></li>").click(function () {
		ok(true, "Before second element events work");
		start();
	});

	jQuery("#listWithTabIndex li").before(element);
	jQuery("#listWithTabIndex li.test6997").eq(1).click();
});

test("append HTML5 sectioning elements (Bug #6485)", function () {
	expect(2);

	jQuery("#qunit-fixture").append("<article style='font-size:10px'><section><aside>HTML5 elements</aside></section></article>");

	var article = jQuery("article"),
	aside = jQuery("aside");

	equal( article.css("fontSize"), "10px", "HTML5 elements are styleable");
	equal( aside.length, 1, "HTML5 elements do not collapse their children")
});

test("HTML5 Elements inherit styles from style rules (Bug #10501)", function () {
	expect(1);

	jQuery("#qunit-fixture").append("<article id='article'></article>");
	jQuery("#article").append("<section>This section should have a pink background.</section>");

	// In IE, the missing background color will claim its value is "transparent"
	notEqual( jQuery("section").css("backgr