<?php
session_start();
header('Content-type: text/css; charset=UTF-8');

function ShowTemplate($name, $data = NULL){
    if(file_exists($name . ".css") && !empty($data)){
        $keys   = explode('&', '@' . implode("@&@", array_keys($data)) . '@');
        $values = array_values($data);
        echo str_replace($keys, $values, file_get_contents($name . ".css"));
    }elseif(file_exists($name . ".css")){
        echo file_get_contents($name . ".css"); 
    }
}

$colors["red"]                = "#F00";
$colors["green"]              = "#090";
$colors["blue"]               = "#667BB4";
$colors["cyan"]               = "#0BF";
$colors["yellow"]             = "#FF0";
$colors["orange"]             = "#F90";
$colors["violet"]             = "#53B";
$colors["brown"]              = "#432";
$colors["light-red"]          = "#FCC";
$colors["light-orange"]       = "#FE6";
$colors["light-blue"]         = "#EAEAFF";
$colors["light-cyan"]         = "#BFF";
$colors["light-green"]        = "#BEE7BE";
$colors["light-yellow"]       = "#FFC";
$colors["light-violet"]       = "#CBF";
$colors["light-brown"]        = "#EDD";
$colors["alpha-red"]          = "rgba(255,0,0,0.5)";
$colors["alpha-green"]        = "rgba(0,153,0,0.5)";
$colors["alpha-blue"]         = "rgba(0,0,153,0.5)";
$colors["alpha-cyan"]         = "rgba(0,204,204,0.5)";
$colors["alpha-yellow"]       = "rgba(255,204,0,0.5)";
$colors["alpha-orange"]       = "rgba(255,153,0,0.5)";
$colors["alpha-violet"]       = "rgba(85,51,187,0.5)";
$colors["alpha-brown"]        = "rgba(68,51,34,0.5)";

$template["main_color"]       = $colors["blue"];
$template["realce_color"]     = $colors["green"];

$custom["gd1x1"]              = "linear-gradient(to top, " . $colors["blue"] . ", " . $colors["light-blue"] . ")";
$custom["gd1x2"]              = "linear-gradient(to top, " . $colors["light-blue"] . ", " . $colors["blue"] . ")";
$custom["bg1x1"]              = $colors["blue"];
$custom["bg1x2"]              = $colors["light-blue"];
$custom["tx1x1"]              = "#000";
$custom["tx1x2"]              = $colors["red"];

$custom["gd2x1"]              = "linear-gradient(to top, " . $colors["green"] . ", " . $colors["light-green"] . ")";
$custom["gd2x2"]              = "linear-gradient(to top, " . $colors["light-green"] . ", " . $colors["green"] . ")";
$custom["bg2x1"]              = $colors["green"];
$custom["bg2x2"]              = $colors["light-green"];
$custom["tx2x1"]              = "#000";
$custom["tx2x2"]              = "#FFF";

$custom["active-1"]           = $colors["red"];
$custom["active-2"]           = $colors["light-red"];

ShowTemplate("reset",    $template);
ShowTemplate("helpers",  $colors);
ShowTemplate("custom",   $custom);


####  Classes de altura e largura
for ($i = 1; $i <= 20; $i++):
    echo ".h-{$i}{height: {$i}em;} .w-{$i}{width: {$i}em;}";
endfor;
    
